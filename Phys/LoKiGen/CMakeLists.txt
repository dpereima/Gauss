###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/LoKiGen
------------
#]=======================================================================]

gaudi_add_library(LoKiGenLib
    SOURCES
        src/GenAlgsDicts.cpp
        src/GenChild.cpp
        src/GenChildSelector.cpp
        src/GenDecayChain.cpp
        src/GenDecays.cpp
        src/GenDump.cpp
        src/GenExtractDicts.cpp
        src/GenHybridEngine.cpp
        src/GenHybridEngineActor.cpp
        src/GenHybridLock.cpp
        src/GenKinematics.cpp
        src/GenMoniDicts.cpp
        src/GenPIDOperators.cpp
        src/GenParticles.cpp
        src/GenParticles2.cpp
        src/GenParticles3.cpp
        src/GenParticles4.cpp
        src/GenParticles5.cpp
        src/GenSections.cpp
        src/GenSources.cpp
        src/GenStreamers.cpp
        src/GenTreesFactory.cpp
        src/GenVertices.cpp
        src/IGenDecay.cpp
        src/IGenHybridFactory.cpp
        src/IGenHybridTool.cpp
        src/LoKiGen.cpp
        src/Oscillated.cpp
        src/PrintHepMCDecay.cpp
    LINK
        PUBLIC
            LHCb::GenEvent
            LHCb::LoKiCoreLib
)

gaudi_add_module(LoKiGen
    SOURCES
        src/Components/DumpHepMC.cpp
        src/Components/GenDecay.cpp
        src/Components/GenFilter.cpp
        src/Components/GenHybridTool.cpp
    LINK
        Gauss::LoKiGenLib
)

gaudi_add_dictionary(LoKiGenDict
    HEADERFILES dict/LoKiGenDict.h
    SELECTION dict/LoKiGen.xml
    LINK
        Gauss::LoKiGenLib
)

gaudi_add_executable(GenDecayGrammarTest
    SOURCES
        src/tests/GenDecayGrammarTest.cpp
    LINK
        Gauss::LoKiGenLib
)

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)
