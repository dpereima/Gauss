/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BCMHITCHECKER_H
#define BCMHITCHECKER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Event/MCHit.h"

// from Bcm
#include "BcmDet/DeBcmSens.h"
#include "BcmDet/DeBcm.h"

/** @class BcmHitChecker BcmHitChecker.h
 *
 *
 *  @author Tomasz Szumlak & Chris Parkes
 *  @date   2005-12-13
 */

class DeBcm;
class DeBcmSens;

class BcmHitChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode getData();
  bool checkStation(LHCb::MCHit* Hit);
  StatusCode bcmHitMonitor();

private:

  Gaudi::Property<std::string> m_bcmDetLocation {this, "BcmDetLocation", DeBcmLocation::Default, "BcmDetLocation"};
  Gaudi::Property<std::string> m_bcmHitsLocation{this, "BcmHitsLocation", "MC/Bcm/Hits","BcmHitsLocation"};
  Gaudi::Property<bool> m_detailedMonitor{this, "MonitorInDetail",false,"MonitorInDetail"};
  DeBcm* m_bcmDet;
  LHCb::MCHits* m_bcmMCHits = {};

};
#endif
