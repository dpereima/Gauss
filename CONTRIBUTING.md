# Contributions to the Gauss GitLab project

When you contribute new or modified code to Gauss ensure to start by getting a copy of the package(s) you need to modify **from the branch you 
intend this code to be for**. 
Due to the fact that the majority of people contributing code will want it to be deployed in Sim09, and you should specify this branch for 
your pull and merge requests. 

For example if you want to contribute a new generator levels cut for Sim09, once you have your satellite project, you can just check out the 
package from the Sim09 branch
```
git lb-checkout Gauss/Sim09 Gen/GenCuts
```
You can then work on the changes you want to make and commit them to your local copy as much as you want.
You do not need to put entries in the release notes of the package any more but you should put meaningful message when you commit to your local 
copy as they will be propagated to the remote repository.

Once you are happy with your changes you can push them to a branch in the Gauss remote repository.
It is recommended you create a Jira task in the LHCBGAUSS Jira (LHCBGAUSS-NNNN in the example below) with a short description of the issue 
before pushing to the remote Gauss repository, and we suggest
you do so early on in the process as we can provide general suggestions and comments before hand.

It is suggested that the name of the branch you commit your code is related to what it is for e.g:
```
git lb-push Gauss WhatTheCodeIsFor
```

Once the branch has been pushed to the repository other people can look at the code and contribute to the development by using the same branch.

When you are ready for the code to be tested and reviewed you have to **create a merge request**.
Please put a meaningull tile and the Jira ticket in the description,
e.g. this refers to / closes / see / details in LHCBGAUSS-NNNN
This will introduce a direct link between the Jira task and the merge request that will later be carried on in the release notes 
and web documentation.

When creating the merge request you need to first *select the branch you want this to be applied to*. The default is master so be sure to change
it to `Sim09` if you need to. 
In case you want the code is for master but you want it to be propagated to `Sim09` or vice-versa either add a comment to this extent into the merge request 
or select the label ~ToSim09 or ~ToMaster.
The Gauss manager will take care of the rest and keep you in the loop.

# Testing and reviewing the code you submitted
In order to test the code provided for new releases we have various nightly slots and for Gauss we have additional dedicated nightly slots.   
For a MR to be picked up in the nightly the corresponding label has to be selected.   
The whole up-to-date list with an explanation can be found in lhcb/Gauss#12

Gauss in lhcb-sim09 and lhcb-sim09-cmake is built from the head of the Sim09 GitLab branch + the selected MR
while the one in the other slots is built from master.

In case the code of the merge request does not compile it will be exclude from future builds until a fix is provided.

The person asking for a merge request should verify that the code provides the expected results by running Gauss from the corresponding 
nightly build, e.g. for Sim09 by doing:
```
lb-run --nightly lhcb-sim09 Gauss/Sim09 gaudirun.py MyOptions.py
```
In parallel someone will be asked to review the code to ensure someone else an idea of what it does and it fits within the Gauss framework 
and the underlying LHCb and Gaudi software.

If changes to the code are necessary due to compilation problems, unexpected test results, review comments, they should be introduce in the 
branch in which the code has been submitted (LHCBGAUSS-NNNN.WhatTheCodeIsFor in the example above)


