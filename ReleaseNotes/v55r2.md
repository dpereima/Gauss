2021-10-27 Gauss v55r2
===

This version uses 
Geant4 [v106r2p4](../../../../Geant4/-/tags/v106r2p4),
Run2Support [v2r0](../../../../Run2Support/-/tags/v2r0),
LHCb [v53r1](../../../../LHCb/-/tags/v53r1),
Gaudi [v36r0](../../../../../gaudi/Gaudi/-/tags/v36r0) and
LCG [100_LHCB_6](http://lcginfo.cern.ch/release/100_LHCB_6/) with ROOT 6.24.00. :new:

The following generators are used from LCG [100_LHCB_6](http://lcginfo.cern.ch/release/100_LHCB_6/) with ROOT 6.24.00:
- pythia8   244.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- **madgraph  2.7.3.py3.atlas** :new:
- herwig++  7.2.1,      thepeg 2.2.1
- crmc      1.8.0.lhcb, :warning: starlight **r313** :warning: 
- rivet     **3.1.4** :warning:, yoda   **1.9.0** :warning:   

while the followings are built privately:
- EvtGen (**R02-01-01** :warning:) with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB
- Hijing (1.383bs.2)


This version is released on `master` branch.
Built relative to Gauss [v55r1](../-/tags/v55r1), with the following changes:

### New features ~"new feature"

- ~Configuration | Update ZmaxForStoring plane to include Neutron Shielding and Tilt, !738 (@mimazure)
- ~Configuration ~"Detector simulation" | External Detector, !750 (@mimazure)
- ~Configuration ~"Detector simulation" | MCCollector, !747 (@mimazure)
- ~"Detector simulation" | Parallel Geometry, !757 (@mimazure)
- ~"Detector simulation" | Export GDML with auxiliary information, !746 (@mimazure)
- ~"Detector simulation" ~Calo ~"Fast Simulations" | Fast calo simulation - from Matteo Rama, !633 (@gcorti) [LHCBGAUSS-1956]. Include earlier iteration !627 (@mrama)
- ~"Detector simulation" ~"Fast Simulations" | EcalCollector for fast simulation studies, !752 (@mimazure)
- ~Generators | MadGraph adaptor for Gauss, !749 (@gcorti) [LHCBGAUSS-1374]. Include earlier iterations !573 (@cvazquez, @philten) and !730 (@brachwal, @philten)
- ~Generators | Fix version of MadGraphData, !774 (@gcorti) [lhcb-datapkg/Gen/MadgraphData#4]
- ~Generators | BeamSpot4D, !665 (@tevans)
- ~Generators ~IFT | Adding another decay channel which is erased during EPOS initialization. The..., !767 (@chgu)
- ~Generators ~Upgrade | Update of 4D PV Markov chain sampler, !668 (@fkeizer)


### Fixes ~"bug fix" ~workaround

- ~RICH ~Upgrade | Fix background initialisation for the RICH2 Run3 simulation., !741 (@bmalecki) [LHCBGAUSS-2329]
- ~Generators | Write name of external generator to GenFSR also in cases where there are no interactions generated., !760 (@kreps) [LHCBGAUSS-2310]
- ~Build | Add empty __init__.py to packages to avoid warning, !773 (@kreps)
 

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Calo ~Geant4 ~Generators | Follow changes in LHCb!3121, !761 (@graven)
- ~Generators | Update for Rivet 3.1.4, !765 (@kreps)
- ~Generators | Use new LCG layer LHCB_6 to pick up new generators versions, !736 (@gcorti) [LHCBGAUSS-2135,LHCBGAUSS-2274]
- ~Build | Add missing links to work with new CMake configuration, !751 (@clemenci)
- ~Build | Fixes to build agains new style CMake projects, !744 (@clemenci)
- Dropped usage of (UN)LIKELY macro, !758 (@sponce)
- Drop removed properties from reference files, !740 (@clemenci)
- Update references of tests in preparation for v55r2 release, !762 (@gcorti)


### Other

- ~Monitoring ~Geant4 | Bsiddi fix lhcbgauss 876.git conddb fix, !674 (@bsiddi) [LHCBGAUSS-876]
- ~Generators ~Decays | Use EvtGen option to suppress external warnings, !729 (@tlatham) [#23]
