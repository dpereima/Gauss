#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Author  : Philip Ilten
Created : 2016-08-29

Script to generate SUPERCHIC2 code that can be used with Gauss. The
following changes are made from the vanilla SUPERCHIC2 source that is
downloaded.

1. The SUPERCHIC program is changed to a subroutine.
2. All variables not saved or stored in a common block in SUPERCHIC
   are added to the common block SCCOM.
3. Entry and exit points to the SUPERCHIC subroutine are generated.
4. The random number generator, R2455, is replaced with the Gauss
   random number generator.
"""
from __future__ import print_function

import os, shutil, urllib2, tarfile, glob, Fortran

# Determine local common variables.
def common(src, proc, com, ext = ['entryp', 'exitp', 'dum']):
    src = Fortran.Source(src, recipe = rec)
    vrs = src.procedures[proc].filter(exclude = ['DATA', 'IN-COMMON', 'SAVE'])
    com = '      common/' + com + '/'
    wdt = len(com)
    for v in vrs:
        if v.name in ext: continue
        v = v.name + ','
        if wdt + len(v) >= 72: v = '\n     &' + v; wdt = len(v) - 1
        else: wdt += len(v)
        com += v
    for v in ext:
        v = v + ','
        if wdt + len(v) >= 72: v = '\n     &' + v; wdt = len(v) - 1
        else: wdt += len(v)
        com += v
    return com[:-1]

# Generate modified source.
def generate(src, mod, dct, gol):

    # Create the go-to statements.
    pnt = []
    for k,v in dct.iteritems():
        if type(v) == int: pnt += [v]
    pnt = sorted(pnt); l = ''
    for p in pnt:
        l += ("\n      if (entryp.eq.%i) then\n      goto %i\n      endif" % 
              (1000 + abs(p), 1000 + abs(p)))
    dct[gol] = dct[gol] + l

    # Create the source.
    org = [l.rstrip('\n') for l in file(src, 'r')]
    mod = file(mod, 'w')
    mod.write('c Modified automatically with '
              'Gen/SuperChic2/python/SuperChic2.py.'
              + '\nc Do not manually modify. Generated from ' + src + '.\n')
    cnt = ''
    for i, l in enumerate(org):
        # Handle line continuations.
        l = cnt + l
        if i + 1 < len(org) and org[i + 1].startswith("     &"):
            cnt = l + "\n"; continue
        else: cnt = ""
        # Apply the modification dictionary.
        if i + 1 in dct:
            v = dct[i + 1]
            if type(v) == str: l = v
            elif type(v) == int:
                if v < 0:
                    l = (" " + str(1000 + abs(v)) + " continue\n      " +
                         ("if (exitp.eq.%i) then\n      return\n      endif\n" 
                          % (1000 + abs(v))) + l)
                else:
                    l = (l + "\n " + str(1000 + abs(v)) + " continue\n      " +
                         ("if (exitp.eq.%i) then\n      return\n      endif" % 
                          (1000 + abs(v))))
        mod.write(l + '\n')
    mod.close()

# Download the FORTRAN source if needed.
tgz = 'superchicv2.04.tar.gz'
url = 'https://superchic.hepforge.org/'
if not os.path.isfile(tgz):
    info = urllib2.urlopen(url + tgz)
    text = info.read()
    code = file(tgz, 'w')
    code.write(text)
    code.close()
    code = tarfile.open(tgz)
    code.extractall()
    code.close()
tgz = tgz[0:-7]
rec = ['gfortran', '-fdump-fortran-original', '-w', '-c',
       '$SOURCE', '-o', '$TARGET']
for inc in glob.glob(tgz + '/src/*'): rec += ['-I', inc]

# Create the src folders.
for fld in ['include', 'init', 'main', 'intf']:
    if not os.path.exists('../src/' + fld): os.makedirs('../src/' + fld)

# Load the Makefile variables.
mkf = file(tgz + '/makefile', 'r')
dct = {'PWD': tgz, 'EW': ''}
var = None
for l in mkf:
    if '=' in l: var = l.split('=')[0].strip()
    l = l.strip()
    if not var in dct: dct[var] = []
    if var: dct[var] += l.split('=')[-1].strip('\\').split()
    if not l.endswith('\\'): var = None
dct['fCODELHA'] = dct['Intf']
dct['Intf'] = ['']
for var, vals in dct.iteritems():
    new = []
    for idx, val in enumerate(vals): 
        if '$' in val and val.startswith('$(') and val.endswith(')'):
            try: new += dct[val[2:-1]]
            except: pass
        else: new += [val]
    dct[var] = new

# Copy the source.
incs = []
for fld in glob.glob(tgz + '/src/*'):
    srcs = glob.glob(fld + '/*')
    for src in srcs:
        dsts = []; dst = src.split('/')[-1]; obj = dst.replace('.f', '.o')
        if fld.endswith('inc'): incs += [dst]; dsts += ['../src/include/' + dst]
        else:
            if obj in dct['iCODELHA']: dsts += ['../src/init/' + dst]
            if obj in dct['sCODELHA']: dsts += ['../src/main/' + dst]
            if obj in dct['fCODELHA']: dsts += ['../src/intf/' + dst]
        for dst in dsts:
            if dst.split('/')[-1] in incs and not 'include' in dst:
                print('Warning: duplicate of %s in ../src/include.' % dst)
            shutil.copy(src, dst)

# Create the modified main.f SUPERCHIC source.
src = tgz + '/src/main/superchic.f'
com = common(src, 'superchic', 'sccom')
dct = {
    # Create the event fill sub-routine.
    32:   ("      subroutine sbr_superchic\n"),
    # Add the extra variables.
    90:   ("      integer entryp,exitp\n" + com + '\n'),
    # Fix the parameter output.
    709:  ("      write(56,98)' *',scorr,' : Include spin correlations'"),
    # Define the entry/exit point lines (< 0 is before, > 0 after).
    93:     0,  813:  1, # Full SUPERCHIC program.
    211:    2,  207:  3, # Read input from standard in.
    227:    4,  245:  5, # Set the SM parameters.
    491:    6,  494:  7, # Random generator initialization.
    613:    8,  614:  9, # The VEGAS2 event generation call.
    633:   10,  634: 11, # Write the unweighted events to file.
    }
generate(src, '../src/main/superchic.f', dct, 90)

# Create the modified init.f SUPERCHIC source.
src = tgz + '/src/init/init.f'
com = common(src, 'MAIN__', 'initcom', ['irts', 'iisurv', 'ipdfmember', 
                                        'entryp', 'exitp', 'iintag', 
                                        'ipdfname', 'dum'])
dct = {
    # Create the sub-routine.
    1:    ("      subroutine sbr_init"),
    # Add the extra variables.
    9:    ("      include 'pdfinf.f'\n      integer entryp,exitp\n" 
           "      character*100 iintag,ipdfname\n" + com + '\n'),
    13:   ("      rts = irts\n      isurv = iisurv\n      intag = iintag\n"
           "      pdfname = ipdfname\n      pdfmember = ipdfmember\n"),
    # Force unique subroutine names.
    38:   ("      call initsud           ! sudakov factor"),
    39:   ("      call inithg            ! skewed PDF"),
    # Define the entry/exit point lines (< 0 is before, > 0 after).
    10:     0,   42:  1, # Full INIT program.
    14:     2,   24:  3, # Read input from standard in.
    }
generate(src, '../src/init/init.f', dct, 9)

# Create the modified header.f SUPERCHIC source.
src = tgz + '/src/main/header.f'
dct = {108: ("      write(*,98)' *',scorr,' :  Include spin correlations'")}
generate(src, '../src/main/header.f', dct, 108)

# Create the modified inithg.f SUPERCHIC source.
src = tgz + '/src/init/inithg.f'
dct = {2: ("      subroutine inithg")}
generate(src, '../src/init/inithg.f', dct, 2)

# Create the modified initsud.f SUPERCHIC source.
src = tgz + '/src/init/initsud.f'
dct = {2: ("      subroutine initsud")}
generate(src, '../src/init/initsud.f', dct, 2)

# Create the modified ran.f SUPERCHIC source.
src = tgz + '/src/int/ran.f'
mod = file('../src/intf/ran.f', 'w')
mod.write('c Modified automatically with '
          'Gen/SuperChic2/python/SuperChic2.py.'
          + '\nc Do not manually modify. Generated from ' + src + '.\n'
          '      subroutine r2455(ran)\n      real*8 ran,cpyr\n' +
          '      integer idummy\n      ran = cpyr(idummy)\n      end')

# Generate the C-interface.
coms = ['vars', 'survin', 'in', 'pdfint', 'process', 'out', 'bveg1', 
        'prec', 'veg', 'nsurv', 'genunw', 'wtmax', 'yrange', 'mrange', 
        'gencut', 'corr', 'cuts', 'jetalg', 'pdg', 'ewpars', 'masses', 
        'masses0', 'pmass', 'mpi', 'mkp', 'widths', 'sccom', 'wmax',
        'unweighted', 'hepeup', 'rec', 'record']
cin = file('../SuperChic2/SuperChic2.h', 'w')
cin.write('// Generated automatically with Gen/SuperChic2/python/SuperChic2.py.'
          + '\n// Do not manually modify. Based on ' + tgz + '.\n')
cin.write('extern "C" {\n\n')
src = Fortran.Source('../src/main/superchic.f', recipe = rec)
for com in coms: cin.write(src.commons[com].cexpr() + '\n\n')
cin.write(src.procedures['sbr_superchic'].cexpr().replace('_(', '__(') + '\n\n')
src = Fortran.Source('../src/init/init.f', recipe = rec)
cin.write(src.commons['initcom'].cexpr() + '\n\n')
cin.write(src.procedures['sbr_init'].cexpr().replace('_(', '__(') + '\n\n')
cin.write('}\n')

# Move the 'include' directory to 'common'.
shutil.copy('../src/include', '../src/common')
