/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Confine.cpp,v 1.1.1.1 2006-04-24 21:45:50 robbep Exp $
// access BcGen common Confine
#include "LbBcVegPy/Confine.h"

// set pointer to zero at start
Confine::CONFINE* Confine::s_confine =0;

// Constructor
Confine::Confine() : m_dummy( 0 ) , m_realdummy( 0. ) { }

// Destructor
Confine::~Confine() { }

// access ptcut in common
double& Confine::ptcut() {
  init(); // check COMMON is initialized
  return s_confine->ptcut;
}

// access etacut in common
double& Confine::etacut() {
  init(); // check COMMON is initialized
  return s_confine->etacut;
}






