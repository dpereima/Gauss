/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Coloct.cpp,v 1.1.1.1 2006-04-24 21:45:50 robbep Exp $
// access BcGen common Coloct
#include "LbBcVegPy/Coloct.h"

// set pointer to zero at start
Coloct::COLOCT* Coloct::s_coloct =0;

// Constructor
Coloct::Coloct() : m_dummy( 0 ) , m_realdummy( 0. ) { }

// Destructor
Coloct::~Coloct() { }

// access ioctet in common
int& Coloct::ioctet() {
  init(); // check COMMON is initialized
  return s_coloct->ioctet;
}
