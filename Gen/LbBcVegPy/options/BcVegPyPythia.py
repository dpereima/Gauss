###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Import the necessary modules.
from Configurables import Generation, MinimumBias, Special, PythiaProduction
from Configurables import BcVegPyProduction

# Add Pythia as minimum bias production tool.
Generation().addTool(MinimumBias)
Generation().MinimumBias.ProductionTool = "PythiaProduction"
Generation().MinimumBias.addTool(PythiaProduction)

# Add BcVegPy as special production tool.
Generation().addTool(Special)
Generation().Special.ProductionTool = "BcVegPyProduction"
Generation().Special.addTool(BcVegPyProduction)
Generation().Special.BcVegPyProduction.ShowerToolName = "PythiaProduction"
