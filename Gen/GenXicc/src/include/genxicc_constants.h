/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
      INTEGER maxnup,maxpup,MAXNVBIN
      parameter (maxnup=500)
      parameter (maxpup=100)
      PARAMETER (MAXNVBIN=5000) ! GG 16/02/2012 protect against nvbin>MAXNVBIN

