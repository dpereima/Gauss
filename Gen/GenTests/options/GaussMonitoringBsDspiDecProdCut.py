###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id: GaussMonitoringBsDspiDecProdCut.py,v 1.1 2009-05-15 08:47:21 robbep Exp $ 

from Gauss.Configuration import *

LHCbApp().DDDBtag = "head-20090330"
LHCbApp().CondDBtag = "sim-20090402-vc-mu100"

importOptions('$GENTESTSROOT/options/GaussMonitoringBsDspiDecProdCut.opts')
