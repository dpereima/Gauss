/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// access GenXicc common Wbstate
#include "LbGenXicc/Wbstate.h"

// set pointer to zero at start
Wbstate::WBSTATE* Wbstate::s_wbstate =0;

// Constructor
Wbstate::Wbstate() { }

// Destructor
Wbstate::~Wbstate() { }

// access ratiou in common
double& Wbstate::ratiou() {
  init(); // check COMMON is initialized
  return s_wbstate->ratiou;
}

// access ratiod in common
double& Wbstate::ratiod() {
  init(); // check COMMON is initialized
  return s_wbstate->ratiod;
}

// access ratios in common
double& Wbstate::ratios() {
  init(); // check COMMON is initialized
  return s_wbstate->ratios;
}

// access nbound in common
int& Wbstate::nbound() {
  init(); // check COMMON is initialized
  return s_wbstate->nbound;
}






