/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// F. Zhang 01-04-11
#ifndef LBGENXICC_USERTRAN_H
#define LBGENXICC_USERTRAN_H 1

#ifdef WIN32
extern "C" {
  void* __stdcall USERTRAN_ADDRESS(void) ;
}
#else
extern "C" {
  void* usertran_address__(void) ;
}
#endif

class Usertran {
public:
  Usertran();
  ~Usertran();

  //int& ishower(); F. Zhang 01-04-11
  int& idpp();

  inline void init(); // inlined for speed of access (small function)

private:
  struct USERTRAN;
  friend struct USERTRAN;
  
  struct USERTRAN {
    //int ishower; F. Zhang 01-04-11
    int idpp;
  };
  int m_dummy;
  double m_realdummy;
  static USERTRAN* s_usertran;
};

// Inline implementations for Usertran
// initialise pointer
#ifdef WIN32
void Usertran::init(void) {
  if ( 0 == s_usertran ) s_usertran = static_cast<USERTRAN*>(USERTRAN_ADDRESS());
}
#else
void Usertran::init(void) {
  if ( 0 == s_usertran ) s_usertran = static_cast<USERTRAN*>(usertran_address__());
}
#endif
#endif // LBGENXICC_USERTRAN_H
 
