###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/Generators
--------------
#]=======================================================================]

gaudi_add_library(GeneratorsLib
    SOURCES
        src/Lib/ExternalGenerator.cpp
        src/Lib/F77Utils.cpp
        src/Lib/GenCounters.cpp
        src/Lib/LHAPDFCommonBlocks.cpp
        src/Lib/LhaPdf.cpp
        src/Lib/RandomForGenerator.cpp
        src/Lib/Signal.cpp
        src/Lib/StreamForGenerator.cpp
        src/Lib/StringParse.cpp
        src/Lib/cpyr.cpp
        src/Lib/getaddr.cpp
        src/Lib/LHAPDFUtils.F
        src/Lib/f77units.F
        src/Lib/pythiaoutput_utils.F
    LINK
        PUBLIC
            LHCb::GenEvent
            Gaudi::GaudiAlgLib
            Gauss::pythia6forgauss
            LHCb::PartPropLib
)

gaudi_add_module(Generators
    SOURCES
        src/Components/AsymmetricCollidingBeams.cpp
        src/Components/BeamGasFixedLuminosity.cpp
        src/Components/BeamSpot4D.cpp
        src/Components/BeamSpotMarkovChainSampleVertex.cpp
        src/Components/BeamSpotSmearVertex.cpp
        src/Components/CollidingBeams.cpp
        src/Components/FixedLuminosity.cpp
        src/Components/FixedLuminosityForRareProcess.cpp
        src/Components/FixedLuminosityForSpillOver.cpp
        src/Components/FixedNInteractions.cpp
        src/Components/FixedTarget.cpp
        src/Components/FlatSmearVertex.cpp
        src/Components/FlatZSmearVertex.cpp
        src/Components/Generation.cpp
        src/Components/HistoSmearVertex.cpp
        src/Components/Inclusive.cpp
        src/Components/MergedEventsFilter.cpp
        src/Components/MinimumBias.cpp
        src/Components/ReadHepMCAsciiFile.cpp
        src/Components/RepeatDecay.cpp
        src/Components/SaveSignalBInformation.cpp
        src/Components/SignalForcedFragmentation.cpp
        src/Components/SignalPlain.cpp
        src/Components/SignalRepeatedHadronization.cpp
        src/Components/Special.cpp
        src/Components/StandAloneDecayTool.cpp
        src/Components/TriangZSmearVertex.cpp
        src/Components/UniformSmearVertex.cpp
        src/Components/VariableLuminosity.cpp
        src/Components/WriteHepMCAsciiFile.cpp
        src/Components/XmlCounterLogFile.cpp
    LINK
        LHCb::LHCbKernel
        Gauss::GeneratorsLib
)

gaudi_add_module(GeneratorTests
    SOURCES
        test/src/IVertexSmearingTool_test.cpp
    LINK
        LHCb::LHCbKernel
        Gauss::GeneratorsLib
)
