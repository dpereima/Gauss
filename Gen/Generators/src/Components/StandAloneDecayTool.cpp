/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: StandAloneDecayTool.cpp,v 1.4 2008-07-24 22:06:07 robbep Exp $
// Include files 
#include "StandAloneDecayTool.h"

// from Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// from Generators
#include "MCInterfaces/IDecayTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : StandAloneDecayTool
//
// 2006-04-18 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the tool factory

DECLARE_COMPONENT( StandAloneDecayTool )

//=============================================================================
// Initialization
//=============================================================================
StatusCode StandAloneDecayTool::initialize() {
  StatusCode sc = Signal::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  LHCb::IParticlePropertySvc * ppSvc = 
    svc< LHCb::IParticlePropertySvc >( "LHCb::ParticlePropertySvc" ) ;
  const LHCb::ParticleProperty * prop = ppSvc -> find( LHCb::ParticleID( *m_pids.begin() ) ) ;
  m_signalMass = prop -> mass() ;

  release( ppSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
bool StandAloneDecayTool::generate( const unsigned int nPileUp , 
                                    LHCb::HepMCEvents * theEvents ,
                                    LHCb::GenCollisions * theCollisions ) {
  // prepare event
  LHCb::GenCollision * theGenCollision( 0 ) ;
  HepMC::GenEvent * theGenEvent( 0 ) ;

  // generate the requested number of "pile-up" events
  for ( unsigned int i = 0 ; i < nPileUp ; ++i ) {
    prepareInteraction( theEvents , theCollisions , theGenEvent , 
                        theGenCollision ) ;
    
    // Particle to decay
    HepMC::GenParticle * theParticle = new HepMC::GenParticle( ) ;
    theParticle -> 
      set_momentum( HepMC::FourVector( 0. , 0. , 0., m_signalMass ) ) ;

    // Decay the particle at (0,0,0,0)
    HepMC::GenVertex * theVertex = 
      new HepMC::GenVertex( HepMC::FourVector( 0., 0., 0., 0. ) ) ;
    theGenEvent -> add_vertex( theVertex ) ;
    theVertex -> add_particle_out( theParticle ) ;
    
    bool flip( false ) ;
    
    int thePID = *m_pids.begin() ;
    if ( m_cpMixture ) {
      // decide the PID to generate
      double flavour = m_flatGenerator() ;
      m_decayTool -> enableFlip() ;
      
      if ( flavour < 0.5 ) 
        theParticle -> set_pdg_id( +abs( thePID ) ) ;
      else
        theParticle -> set_pdg_id( -abs( thePID ) ) ;
    } else {
      m_decayTool -> disableFlip() ;
      theParticle -> set_pdg_id( thePID ) ;
    }

    if ( ! m_inclusive.value() ) 
      m_decayTool -> generateSignalDecay( theParticle , flip ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    else 
      m_decayTool -> generateDecay( theParticle ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    
    theParticle -> set_status( LHCb::HepMCEvent::SignalInLabFrame ) ;
  
    theGenEvent -> 
      set_signal_process_vertex( theParticle -> end_vertex() ) ;
    theGenCollision -> setIsSignal( true ) ;
  }
  
  return true ;
}

//=============================================================================

