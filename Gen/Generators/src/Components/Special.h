/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Special.h,v 1.5 2007-03-08 13:42:17 robbep Exp $
#ifndef GENERATORS_SPECIAL_H
#define GENERATORS_SPECIAL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Generators/ExternalGenerator.h"

// forward declarations
class ICounterLogFile ;

/** @class Special Special.h
 *
 *  Tool for special samples (Higgs, W, Z, ...) generation.
 *  Concrete implementation of ISampleGenerationTool using
 *  the ExternalGenerator base class.
 *
 *  @author Patrick Robbe
 *  @date   2005-11-14
 */
class Special : public ExternalGenerator {
 public:
  /// Standard constructor
  using ExternalGenerator::ExternalGenerator;
  
  /// Initialize method
  StatusCode initialize( ) override;

  /// Finalize function
  StatusCode finalize() override;

  /** Generate a single interaction (No Pile-up for the moment.
   *  Implements ISampleGenerationTool::generate.
   *  Accepts all events generated with the IProductionTool
   *  (usually configured with special options) and passing
   *  the generator level cut.
   */
  bool generate( const unsigned int nPileUp ,
                 LHCb::HepMCEvents * theEvents ,
                 LHCb::GenCollisions * theCollisions ) override;

  /// Implements ISampleGenerationTool::printCounters
  void printCounters( ) const override;

private:
  /// XML Log file
  ICounterLogFile * m_xmlLogTool{nullptr} ;

  /// Counter of events before the generator level cut
  unsigned int m_nEventsBeforeCut{0} ;

  /// Counter of events after the generator level cut
  unsigned int m_nEventsAfterCut{0} ;

  /// Number of pile-up events to generate at once
  Gaudi::Property<unsigned int> m_maxInteractions{this,"MaxInteractions",1000,"Number of pile-up events to generate at once"} ;

  /// Vector to contain pile-up events
  std::vector< HepMC::GenEvent * > m_pileUpEventsVector{} ;

  /// Vector to contain collision infos
  std::vector< LHCb::GenCollision * > m_pileUpCollisionsVector{} ;

  /// function to generate a set of pile up events
  void generatePileUp() ;

  /// production tool which generates pile-up interactions
  IProductionTool * m_pileUpProductionTool {nullptr};

  /// flag to reinitialize the pile up generator
  Gaudi::Property<bool> m_reinitialize{this,"ReinitializePileUpGenerator",true,"flag to reinitialize the pile up generator"} ;

  /// Name of the production tool for pile-up
  Gaudi::Property<std::string> m_pileUpProductionToolName{this,"PileUpProductionTool","Pythia8Production/MinimumBiasPythia8Production","Name of the production tool for pile-up"} ;

  /// Copy collision FROM to TO
  void copyCollision( const LHCb::GenCollision * FROM ,
                      LHCb::GenCollision * TO ) const ;
};
#endif // GENERATORS_SPECIAL_H
