/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "BeamSpot4D.h"

// from ROOT
#include <TVectorD.h>
#include <TDecompChol.h>
//-----------------------------------------------------------------------------
// Implementation file for class : BeamSpot4D
// Generates PVs in (x,y,z,t) using LU decomposition of the full 4D distribution
// 2019-09-06 Tim Evans
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BeamSpot4D::BeamSpot4D( const std::string& type,
    const std::string& name,
    const IInterface* parent )
: GaudiTool ( type, name , parent )
{
  declareInterface< IVertexSmearingTool >( this ) ;
}

//=============================================================================
// Copy constructor for multivariate gaussian distribution generator
//=============================================================================
BeamSpot4D::GaussND::GaussND(const BeamSpot4D::GaussND& other) 
: U(other.U) 
{
}
//=============================================================================
// Copy operator for multivariate gaussian distribution generator
//=============================================================================
BeamSpot4D::GaussND& BeamSpot4D::GaussND::operator=( const BeamSpot4D::GaussND& other)
{
  U.ResizeTo( other.U.GetNrows(), other.U.GetNrows() );
  U = other.U;
  return *this; 
}

//=============================================================================
// Constructor for multivariate gaussian distribution generator from its covariance matrix  
//=============================================================================
BeamSpot4D::GaussND::GaussND(const TMatrixD& C) 
: U(C.GetNrows(), C.GetNrows())
{
  TDecompChol decomposed(C);
  decomposed.Decompose();
  U = decomposed.GetU();
}

std::vector<double> BeamSpot4D::GaussND::operator()(Rndm::Numbers& rnd) const
{
  unsigned size = U.GetNrows();
  std::vector<double>   p(size, 0);
  std::vector<double>  rt(size, 0);

  for ( unsigned i = 0; i != size; ++i ) p[i] = rnd();
  for ( unsigned i = 0; i != size; ++i ){
    for( unsigned j = 0 ; j != size; ++j ) rt[i] += U(j, i) * p[j];
  }
  return rt;
}

//=============================================================================
// Calculate covariance matrix of 4D gaussian distribution for interactions
//=============================================================================

TMatrixD rotX(const double angle){
  TMatrixD Rx(3,3);
  Rx(0,0) = 1.0;
  Rx(1,1) = Rx(2,2) = cos(angle);
  Rx(1,2) = -sin(angle);
  Rx(2,1) = +sin(angle);
  return Rx; 
}

TMatrixD rotY(const double angle)
{
  TMatrixD Ry(3,3);
  Ry(0,0) =  Ry(2,2) = cos(angle);
  Ry(1,1) =  1;
  Ry(2,0) = -sin(angle);
  Ry(0,2) = +sin(angle);
  return Ry;
}

BeamSpot4D::GaussND BeamSpot4D::calculateInteractionDistribution(const LHCb::BeamParameters* beamp)
{
  const auto emittance = beamp -> emittance();
  const auto betastar  = beamp -> betaStar();
  const auto ha        = beamp -> horizontalCrossingAngle();
  const auto va        = beamp -> verticalCrossingAngle();
  const auto ha_bl     = beamp -> horizontalBeamlineAngle();
  const auto va_bl     = beamp -> verticalBeamlineAngle();
  const auto sb        = beamp -> sigmaS();
  const auto c         = Gaudi::Units::c_light ;
  const auto st        = std::sqrt( emittance*betastar );

  const auto sx        = st;
  const auto sy        = st;

  debug() << "Transverse beam size = " << sx << " mm x << " << sy <<  "mm" << endmsg;
  debug() << "Bunch RMS = " << sb      << "mm"        << endmsg; 
  debug() << "Emittance = " << emittance              << endmsg; 
  debug() << "Betastar  = " << betastar << "m"        << endmsg;
  debug() << "Half Crossing angles: " << ha << " " << va << endmsg;  
  
  // this is the (inverse) covariance matrix of each beam 
  TMatrixD c1(3,3);
  c1(0,0) = 1./(sx*sx);
  c1(1,1) = 1./(sy*sy);
  c1(2,2) = 1./(sb*sb); 
  TMatrixD c2 = c1; 

  // Crossing in the horizontal plane corresponds to a rotation about the y axis, 
  // and crossing in the vertical plane corresponds to a rotation about the x axis 
  // By convention, a positive angle means that beam1 is 
  // moving from - to + in the transverse plane, which corresponds to a rotation of +(-) the 
  // horizontal (vertical) crossing angle for beam 1 
  // Both beams are also rotated by the overall angle of the beam axis with respect to LHCb 

  auto r1 = rotY(ha + ha_bl) * rotX(-va - va_bl);
  auto r2 = rotY(-ha + ha_bl) * rotX(va - va_bl);

  TMatrixD r1_T(TMatrixD::kTransposed,r1);
  TMatrixD r2_T(TMatrixD::kTransposed,r2);

  // both (inverse) covariance matrices are rotated by the crossing angles 
  auto s1 = r1 * c1 * r1_T;
  auto s2 = r2 * c2 * r2_T;
  // From the (inverse) covariance matrix of the beams, we can determine the (inverse) covariance matrix of the
  // interactions (its just their sum) + the components in time
  TMatrixD D(4,4);
  for( unsigned i = 0; i < 3; ++i )
  {
    for( unsigned j = 0; j < 3; ++j )
    {
      D(i,j) = s1(i,j) + s2(i,j);
      // these terms are much simpler really as c1/c2 are diagonal, but left in for completeness, 
      // or in case we want to generalise to elliptical beams 
      D(3,i) += ( r1(i,j) * c1(j,2) - r2(i,j) * c1(j,2) ) * c;
      D(i,3) += ( r1(i,j) * c1(j,2) - r2(i,j) * c2(j,2) ) * c;
    }
  }
  D(3,3) = c * c * ( c1(2,2) + c2(2,2) );

  D.Invert();

  debug() << "Luminous Region : [" << sqrt(D(0,0)) << "mm, " 
    << sqrt(D(1,1)) << "mm, " 
    << sqrt(D(2,2)) << "mm, " 
    << sqrt(D(3,3)) << "ns ]" << endmsg; 
  
  return BeamSpot4D::GaussND(D); 
}


//=============================================================================
// Initialize
//=============================================================================
StatusCode BeamSpot4D::initialize( )
{
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc &= m_rand.initialize( randSvc , Rndm::Gauss(0., 1. ) );
  if ( sc.isFailure() )
  { return Error( "Could not initialize random number generators" ); }
  release( randSvc ).ignore();
  return sc;
}

//=============================================================================
// 'Smear' vertices by PV position
//=============================================================================
StatusCode BeamSpot4D::smearVertex( LHCb::HepMCEvent * theEvent )
{
  LHCb::BeamParameters * beamp = get<LHCb::BeamParameters>(m_beamParameters);
  auto genPV = calculateInteractionDistribution(beamp);
  auto * pEvt = theEvent -> pGenEvt() ;
  if ( !pEvt ){
    return Error("Could not get event");
  }
  auto d = genPV(m_rand);
  // This is just one PV, i.e. all the vertices should be 'smeared' by the same amount, i.e. the position of the PV. 
  for ( auto vit = pEvt -> vertices_begin() ; vit != pEvt -> vertices_end() ; ++vit )
  {
    const auto pos = (*vit) -> position();
    (*vit)->set_position(HepMC::FourVector(pos.x() + d[0] + beamp->beamSpot().x()
          , pos.y() + d[1] + beamp->beamSpot().y()
          , pos.z() + d[2] + beamp->beamSpot().z()
          , pos.t() + d[3]));
  }
  return StatusCode::SUCCESS;
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( BeamSpot4D )
