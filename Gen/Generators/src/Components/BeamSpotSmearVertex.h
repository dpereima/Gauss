/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BeamSpotSmearVertex.h,v 1.7 2010-05-09 17:05:43 gcorti Exp $
#ifndef GENERATORS_BEAMSPOTSMEARVERTEX_H
#define GENERATORS_BEAMSPOTSMEARVERTEX_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"

#include "Generators/IVertexSmearingTool.h"

// from Event
#include "Event/BeamParameters.h"

/** @class BeamSpotSmearVertex BeamSpotSmearVertex.h "BeamSpotSmearVertex.h"
 *
 *  VertexSmearingTool to smear vertex according to beam spot parameters.
 *  Concrete implementation of IVertexSmearingTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-24
 */
class BeamSpotSmearVertex : public extends<GaudiTool,IVertexSmearingTool> {
public:
  /// Standard constructor
  using extends::extends;

  /// Initialize function
  StatusCode initialize( ) override;

  /** Implementation of IVertexSmearingTool::smearVertex.
   *  Gaussian smearing of spatial position of primary event truncated
   *  at a given number of sigma.
   */
  StatusCode smearVertex( LHCb::HepMCEvent * theEvent ) override;

 private:
  /// Number of sigma above which to cut for x-axis smearing (set by options)
  Gaudi::Property<double> m_xcut{this,"Xcut",4.,"this times SigmaX "}   ;

  /// Number of sigma above which to cut for y-axis smearing (set by options)
  Gaudi::Property<double> m_ycut{this,"Ycut",4.,"this times SigmaY"}   ;

  /// Number of sigma above which to cut for z-axis smearing (set by options)
  Gaudi::Property<double> m_zcut{this,"Zcut",4.,"this times SigmaZ"}   ;

  /// Sign of time of interaction as given from position with respect to
  /// origin
  Gaudi::Property<int>  m_timeSignVsT0{this,"SignOfTimeVsT0",0,"SignOfTimeVsT0"} ;

  Gaudi::Property<std::string> m_beamParameters{this,"BeamParameters",
    LHCb::BeamParametersLocation::Default,"BeamParameters"} ; ///< Location of beam parameters (set by options)

  Rndm::Numbers m_gaussDist ; ///< Gaussian random number generator

};
#endif // GENERATORS_BEAMSPOTSMEARVERTEX_H
