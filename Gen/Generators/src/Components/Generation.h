/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Generation.h,v 1.9 2007-07-03 16:31:36 gcorti Exp $
#ifndef GENERATORS_GENERATION_H
#define GENERATORS_GENERATION_H 1

#include "GaudiAlg/GaudiAlgorithm.h"

#include <boost/array.hpp>

#include "Event/GenFSR.h"

// from Event
#include "Event/GenHeader.h"
#include "Event/GenCollision.h"
#include "Event/GenFSR.h"
// from Generators

#include "GenEvent/HepMCUtils.h"

// Forward declarations
class ISampleGenerationTool ;
class IPileUpTool ;
class IDecayTool ;
class IVertexSmearingTool ;
class IFullGenEventCutTool ;
class ICounterLogFile ;

namespace HepMC {
  class GenParticle ;
}

namespace LHCb {
  class HepMCEvent;
}

/** @class Generation Generation.h "Generation.h"
 *
 *  Main algorithm to generate Monte Carlo events.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-11
 */
class Generation : public GaudiAlgorithm {
 public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  /** Algorithm initialization.
   *  -# Initializes the common Gaudi random number generator used in all
   *     generators,
   *  -# Retrieve Pile-up tool, sample generation tool, decay tool,
   *     vertex smearing tool and full event cut tool used in the generation
   *     of events.
   */
  StatusCode initialize() override;

  /** Algorithm execution.
   *  Repeat the following sequence until a good set of interactions is
   *  generated.
   *  -# Compute the number of interactions to generate in the event, using
   *     the IPileUpTool.
   *  -# Generate this number of interactions using the IProductionTool.
   *     The IProductionTool indicates if a good event has been generated
   *     (containing for example a B hadron).
   *  -# Decay with the IDecayTool all particles in the event, if this is
   *     a good event.
   *  -# Apply the full event generator level cut (using IFullEventGenCutTool)
   *     and accept or reject the event.
   *  -# Store in event store the accepted event.
   */
  StatusCode execute   () override;

  /** Algorithm finalization.
   *  Print generation counters.
   */
  StatusCode finalize  () override;

protected:
  /// Decay the event with the IDecayTool.
  StatusCode decayEvent( LHCb::HepMCEvent * theEvent ) ;

private:
  Gaudi::Property<int> m_eventType{this,"EventType",30000000,"EventType"} ; ///< Event type (set by options)

  /// Location where to store generator events (set by options)
  Gaudi::Property<std::string> m_hepMCEventLocation{this,"HepMCEventLocation",
    LHCb::HepMCEventLocation::Default,"HepMCEventLocation"} ;

  /// Location where to store the Header of the events (set by options)
  Gaudi::Property<std::string> m_genHeaderLocation{this,"GenHeaderLocation",
    LHCb::GenHeaderLocation::Default,"GenHeaderLocation"} ;

  /// Location where to store HardInfo (set by options)
  Gaudi::Property<std::string> m_genCollisionLocation{this,"GenCollisionLocation",
    LHCb::GenCollisionLocation::Default,"GenCollisionLocation"} ;

  /// Location where to store FSR counters (set by options)
  Gaudi::Property<std::string> m_FSRName{this,"GenFSRLocation",
    LHCb::GenFSRLocation::Default,"GenFSRLocation"};

  /// Reference to file records data service
  IDataProviderSvc* m_fileRecordSvc;

  IPileUpTool              * m_pileUpTool{nullptr}          ; ///< Pile-up tool

  IDecayTool               * m_decayTool{nullptr}           ; ///< Decay tool

  ICounterLogFile          * m_xmlLogTool{nullptr}          ; ///< Xml Log file tool

  ISampleGenerationTool    * m_sampleGenerationTool{nullptr}; ///< Sample tool

  IVertexSmearingTool      * m_vertexSmearingTool{nullptr}  ; ///< Smearing tool

  IFullGenEventCutTool     * m_fullGenEventCutTool{nullptr} ; ///< Cut tool

  /// Name of the IPileUpTool (set by options).
  Gaudi::Property<std::string> m_pileUpToolName{this,"PileUpTool","FixedLuminosity","PileUpTool"};

  /// Name of the ISampleGenerationTool - MinimumBias, ... (set by options)
  Gaudi::Property<std::string> m_sampleGenerationToolName{this,"SampleGenerationTool","MinimumBias","SampleGenerationTool"} ;

  /// Name of the IDecayTool (set by options)
  Gaudi::Property<std::string> m_decayToolName{this,"DecayTool","EvtGenDecay","DecayTool"};

  /// Name of the IVertexSmearingTool (set by options)
  Gaudi::Property<std::string> m_vertexSmearingToolName{this,"VertexSmearingTool","BeamSpotSmearVertex","VertexSmearingTool"}   ;

  /// Name of the IFullGenEventCutTool (set by options)
  Gaudi::Property<std::string> m_fullGenEventCutToolName{this,"FullGenEventCutTool","","FullGenEventCutTool"}  ;

  /// Flag to generate all pile up events at the same PV (set by options)
  Gaudi::Property<bool> m_commonVertex{this,"CommonVertex",false,"CommonVertex"} ;

  unsigned int m_nEvents{0} ; ///< Number of generated events

  unsigned int m_nAcceptedEvents{0} ; ///< Number of accepted events

  unsigned int m_nInteractions{0} ; ///< Number of generated interactions

  /// Number of interactions in accepted events
  unsigned int m_nAcceptedInteractions{0} ;

  /// Description of the counter index
  enum interationCounterType{ Oneb = 0 , ///< interaction with >= 1 b quark
                              Threeb , ///< interaction with >= 3 b quarks
                              PromptB , ///< interaction with prompt B
                              Onec , ///< interaction with >= 1 c quark
                              Threec , ///< interaction with >= 3 c quarks
                              PromptC , ///< interaction with prompt C
                              bAndc ///< interaction with b and c
  } ;

  /// Type for interaction counter
  typedef boost::array< unsigned int , 7 > interactionCounter ;
  typedef boost::array< std::string  , 7 > interactionCNames  ;

  /// Counter of content of generated interactions
  interactionCounter m_intC{ {0, 0, 0, 0, 0, 0, 0} } ;

  /// Array of counter names
  interactionCNames  m_intCName{{"generated interactions with >= 1b", "generated interactions with >= 3b",
                                      "generated interactions with 1 prompt B", "generated interactions with >= 1c",
                                      "generated interactions with >= 3c", "generated interactions with >= prompt C",
                                      "generated interactions with b and c"}} ;

  /// Counter of content of accepted interactions
  interactionCounter m_intCAccepted{ {0, 0, 0, 0, 0, 0, 0} } ;

  /// Array of accepted counter names
  interactionCNames  m_intCAcceptedName{
      {"accepted interactions with >= 1b", "accepted interactions with >= 3b", "accepted interactions with 1 prompt B",
       "accepted interactions with >= 1c", "accepted interactions with >= 3c", "accepted interactions with >= prompt C",
       "accepted interactions with b and c"}} ;

  /// Counter of events before the full event generator level cut
  unsigned int m_nBeforeFullEvent{0} ;

  /// Counter of events after the full event generator level cut
  unsigned int m_nAfterFullEvent{0} ;

  /// TDS container
  /// LHCb::GenFSRs* m_genFSRs;
  /// FSR for current file
  LHCb::GenFSR* m_genFSR{nullptr};

  /** Update the counters counting on interactions.
   *  @param[in,out] theCounter Counter of events
   *  @param[in]     theEvent  The interaction to study
   */
  void updateInteractionCounters( interactionCounter & theCounter ,
                                  const LHCb::HepMCEvent * theEvent ) ;


  /** Update the counters counting on interactions.
   *  @param[in,out] m_genFSR     The counters in FSR
   *  @param[in]     theCounter   The interaction counters
   *  @param[in]     option       Separate generated and accepted counters
   */
  void updateFSRCounters( interactionCounter & theCounter ,
                          LHCb::GenFSR * m_genFSR,
                          const std::string option ) ;

};
#endif // GENERATORS_GENERATION_H
