/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: WriteHepMCAsciiFile.cpp,v 1.3 2008-07-09 14:39:08 robbep Exp $
//
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/PhysicalConstants.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiAlgorithm.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/HepMCEvent.h"
// ============================================================================
// HepMC
// ============================================================================
#include "HepMC/IO_GenEvent.h"
// ============================================================================
/** @class WriteHepMCAsciiFile WriteHepMCAsciiFile.cpp
 *
 *  Simple class to dump generators events in plain
 *  output file in HepMC Ascii format. It could be used for portable
 *  cross-transitions of events inbetween different generators
 *
 *   The algorithm has 3 properties:
 *
 *    - <c>Input</c>  : The TES location of LHCb::HepMCEvent::Container
 *              ( the default value is <c>LHCb::HepMCEventLocation::Default</c>)
 *    - <c>Ouput</c>  : The name of output HepMC Ascii file.
 *                     The file is opened in "new/overwrite" mode.
 *                     ( the default value is <c>""</c> (empty string)
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2005-10-05
 */
class WriteHepMCAsciiFile : public GaudiAlgorithm
{
public:
  /// initialize the algorithm
  StatusCode initialize () override
  {
    StatusCode sc = GaudiAlgorithm::initialize() ;
    if ( sc.isFailure() ) { return sc ; }
    /// check the output file
    if ( m_output.value().empty() )
    { return Error ( "Output file name is not specified!" ) ; }
    // open the file
    m_file = new HepMC::IO_GenEvent
      ( m_output.value().c_str() , std::ios::out | std::ios::trunc ) ;
    //
    if ( 0 == m_file || m_file->rdstate() == std::ios::failbit )
    { return Error ( "Failure to open the file '"+m_output.value()+"'" ) ; }
    if(m_momentum_unit_name.value() == "GeV"){
      m_momentum_unit = HepMC::Units::GEV;
    } else if (m_momentum_unit_name.value() == "MeV") {
      m_momentum_unit = HepMC::Units::MEV;
    } else {
      return Error("Invalid momentum unit");
    }

    if(m_length_unit_name.value() == "CM"){
      m_length_unit = HepMC::Units::CM;
    } else if (m_length_unit_name.value() == "MM") {
      m_length_unit = HepMC::Units::MM;
    } else {
      return Error("Invalid length unit");
    }
    //
    //m_file->write_comment( "Written by WriteHepMCAsciiFile/"+name() );
    //
    return StatusCode::SUCCESS ;
  } ;
  /// execute it!
  StatusCode execute    () override;
  /// finalize the algorithm
  StatusCode finalize   () override
  {
    // delete the stream  (close the file!)
    if ( 0 != m_file ) { delete m_file ; m_file = 0 ; }
    // finalize the base class ;
    return GaudiAlgorithm::finalize() ;
  }
  /** standard constructor*/
  using GaudiAlgorithm::GaudiAlgorithm;

  virtual ~WriteHepMCAsciiFile()
  { if ( 0 != m_file ) { delete m_file ; m_file = 0  ; } }
private:
  // TES location of HepMC-events
  Gaudi::Property<std::string> m_input{this,"Input",LHCb::HepMCEventLocation::Default,"TES location of HepMC-events"}; ///< TES location of HepMC-events
  // the name of the output file
  Gaudi::Property<std::string> m_output{this,"Output","","the name of the output file"}; ///< the name of the output file
  // rescale event from LHCb to Pythia units ?
  Gaudi::Property<std::string> m_momentum_unit_name{this,"MomentumUnit","","Momentum unit [GeV, MeV]"};
  Gaudi::Property<std::string> m_length_unit_name{this,"LengthUnit","MM","Length unit [MM, CM]"}; ///< rescale event from LHCb units ?
  HepMC::Units::MomentumUnit m_momentum_unit;
  HepMC::Units::LengthUnit m_length_unit;
  // the output file ;
  HepMC::IO_GenEvent* m_file{nullptr}   ; ///< the output file ;
} ;
// ===========================================================================
/// Declaration of the Algorithm Factory
DECLARE_COMPONENT( WriteHepMCAsciiFile )
// ===========================================================================
/// Execut the algorithm
StatusCode WriteHepMCAsciiFile::execute    ()
{
  /// get input events
  LHCb::HepMCEvent::Container* events
    = get<LHCb::HepMCEvent::Container>( m_input.value() ) ;
  if ( 0 == events ) { return StatusCode::FAILURE ; }
  //
  Assert ( 0 != m_file , "File is invalid!" ) ;
  //
  // loop over events and write them
  for ( LHCb::HepMCEvent::Container::iterator
          ievent = events->begin() ; events->end() != ievent  ; ++ievent )
  {
    LHCb::HepMCEvent* event = *ievent ;
    if ( 0 == event ) { continue ; }
    HepMC::GenEvent* evt = event->pGenEvt();
    if ( 0 == evt   ) { continue ; }
    auto old_mom_unit = evt->momentum_unit();
    auto old_length_unit = evt->length_unit();

    // rescale the event if needed
    evt->use_units(m_momentum_unit, m_length_unit);

    // write event to ascii file
    m_file->write_event(evt); 	//also writes HeavyIon and PdfInfo!

    // rescale back if needed (convert to LHCb units)
    evt->use_units(old_mom_unit, old_length_unit);
  }
  //
  return StatusCode::SUCCESS ;
}
// ===========================================================================

// ===========================================================================
// The END
// ===========================================================================
