###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#Configuration file for MC generic RIVET analyses run on LHCb MB events
from Gauss.Configuration import *
from GaudiKernel.Configurable import *
from GaudiKernel import SystemOfUnits
from Configurables import RivetAnalysisHandler
#Needed to add local directory path to RivetAnalysisHandler options
import os

importOptions("$GAUSSOPTS/GenStandAlone.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
importOptions("$DECFILESROOT/options/SwitchOffEvtGen.py")
importOptions("$DECFILESROOT/options/30000000.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2011.py")
importOptions("$GAUSSOPTS/Gauss-Job.py")
importOptions("$GAUSSOPTS/DBTags-2011.py")
# importOptions("$APPCONFIGOPTS/Gauss/Beam3500GeV-md100-2011-nominalBeamLine-fix1.py")
importOptions("$APPCONFIGOPTS/Gauss/Beam3500GeV-uniformHeadOn-fix1.py")

# GaussGen = GenInit("GaussGen")
# GaussGen.FirstEventNumber = 1
# GaussGen.RunNumber        = 1082

LHCbApp().EvtMax = 500
#LHCbApp().OutputLevel = WARNING
LHCbApp().OutputLevel = INFO
Gauss().DataType = "2011"
Gauss().Histograms = "NONE"
Gauss().OutputType = "NONE"
#Gauss().OutputType = "GEN"
Gauss().DatasetName = "GaussTestMC"
#force head on collisions:
#Gauss().BeamCrossingAngle = 0.0
#Gauss().BeamBetaStar = 0.0

GenMonitor = GaudiSequencer( "GenMonitor" )
GenMonitor.Members += [ "RivetAnalysisHandler", ]
rivet = RivetAnalysisHandler()
rivet.BaseFileName = "LHCb_MC_test"
rivet.RunName = "LHCbMBPy8"
##rivet.RunName = ""
rivet.Analyses = ["MC_LHCb_GENERIC","MC_IDENTIFIED"]
#to search Rivet plugins in current directory
rivet.AnalysisPath += [os.path.abspath('.'),]
#rivet.AnalysisPath += [os.path.abspath(os.getcwd()),]
#rivet.forceXSection = True
#rivet.xSectionValue = 91.1*SystemOfUnits.millibarn
rivet.CorrectStatusID = True
rivet.CorrectCrossingAngles = True

HistogramPersistencySvc().OutputFile = ''
#HistogramPersistencySvc().OutputFile = 'mytest.root'
