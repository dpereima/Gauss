<?xml version="1.0" ?>
<!--
    (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!DOCTYPE extension
  PUBLIC '-//QM/2.4.1/Extension//EN'
  'http://www.codesourcery.com/qm/dtds/2.4.1/-//qm/2.4.1/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
<text>$GENTUNEROOT/options/example/MC_Generic_Test.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>
# Histograms output by test are expected to have same binning and names as in reference
# TODO: Improve YODA based histogram comparison by checking distribution variation
import yoda
import os
print('Using YODA v. %s' % yoda.version().decode('ascii'))
while (True):
    refPath = os.path.normpath(os.path.expandvars('$GENTUNEROOT/tests/refs/LHCb_MC_refhisto.yoda'))
    if not os.path.exists(refPath) or not os.path.isfile(refPath):
        causes.append(os.path.expandvars('No reference file for target $CMTCONFIG'))
        break
    hstPath = os.path.join(os.getcwd(), 'LHCb_MC_test.yoda')
    if not os.path.exists(hstPath) or not os.path.isfile(hstPath):
        causes.append('Histogram file not found or not plain file')
        break
    rhd = yoda.readYODA(refPath)
    thd = yoda.readYODA(hstPath)
    if thd is None or len(thd) == 0:
        causes.append('Parsing of test output histograms has failed in YODA')
        break
    bHasPathPrefix = not list(rhd.keys())[0] in thd.keys()
    refHNames = bHasPathPrefix and [ x.path() for x in rhd.values() ] or list(rhd.keys())
    diffMsgs = []
    for k in refHNames:
        (basePath, hName) = os.path.split(k)
        if bHasPathPrefix: # do not compare internal counters (e.g. event counter)
            if basePath.startswith('_'):
                continue
        else:
            if hName.startswith('_'):
                continue
        if bHasPathPrefix:
            rh = rhd[list(filter(lambda x, key=k: x.endswith(key), rhd.keys()))[0]]
            thn = list(filter(lambda x, key=k: x.endswith(key) and not x.startswith('/RAW/'), thd.keys()))
            if len(thn) != 1:
                diffMsgs.append('Histogram \'%s\' not found in test output.' % (k))
                continue
            th = thd[thn[0]]
        else:
            rh = rhd[k]
            if k not in thd:
                diffMsgs.append('Histogram \'%s\' not found in test output.' % (k))
                continue
            th = thd[k]
        if rh.__class__.__name__ != th.__class__.__name__:
            diffMsgs.append('Distribution \'%s\' stored in different objects in test (%s) and reference (%s) output.' % (k, th.__class__.__name__, rh.__class__.__name__))
            continue
        clsName = rh.__class__.__name__
        if rh.dim() != th.dim():
            diffMsgs.append('Histogram \'%s\' has different dimensions in test (%d) and reference (%d).' % (k, th.dim(), rh.dim()))
            continue
        if clsName.startswith('Scatter'):
            if rh.numPoints() != th.numPoints():
                diffMsgs.append('%s plot \'%s\' has different numPoints in test (%d) and reference (%d).' % (clsName, k, th.numPoints(), rh.numPoints()))
                continue
        else:
            bCmpErr = False
            if isinstance(rh, yoda.core.Counter):
                if not isinstance(th, yoda.core.Counter):
                    diffMsgs.append('%s \'%s\' has different numBins in test (%d) and reference (%d).' % (clsName, k, 1, rh.numBins()))
                    bCmpErr = True
            else:
                if rh.dim() > 1:
                    if rh.numBinsX() != th.numBinsX():
                        diffMsgs.append('%s \'%s\' has different numBinsX in test (%d) and reference (%d).' % (clsName, k, th.numBinsX(), rh.numBinsX()))
                        bCmpErr = True
                    if rh.numBinsY() != th.numBinsY():
                        diffMsgs.append('%s \'%s\' has different numBinsY in test (%d) and reference (%d).' % (clsName, k, th.numBinsY(), rh.numBinsY()))
                        bCmpErr = True
                else:
                    if rh.numBins() != th.numBins():
                        diffMsgs.append('%s \'%s\' has different numBins in test (%d) and reference (%d).' % (clsName, k, th.numBins(), rh.numBins()))
                        bCmpErr = True
            if bCmpErr:
                continue
            if isinstance(rh, yoda.core.Counter):
                if rh.effNumEntries() > 0. and th.effNumEntries() &lt; 0.000000001: 
                    diffMsgs.append('%s \'%s\' is empty in test output.' % (clsName, k))
                    diffMsgs[-1] += (" Ref: %f vs. Test: %f." % (rh.effNumEntries(), th.effNumEntries()))
                    if th.effNumEntries() > 0:
                        diffMsgs[-1] += ' Output overflow bins contain (%f) entries.' % th.effNumEntries()
                    continue
            else:
                if rh.effNumEntries(False) > 0. and th.effNumEntries(False) &lt; 0.000000001: 
                    diffMsgs.append('%s \'%s\' is empty in test output.' % (clsName, k))
                    diffMsgs[-1] += (" Ref: %f vs. Test: %f." % (rh.effNumEntries(False), th.effNumEntries(includeoverflows=False)))
                    if th.effNumEntries(includeoverflows=True) > 0:
                        diffMsgs[-1] += ' Output overflow bins contain (%f) entries.' % th.effNumEntries(includeoverflows=True)
                    continue
        # add further statistical tests here!
    if len(diffMsgs) > 0:
        result['YODA diffs'] = result.Quote("\n".join(diffMsgs))
        causes.append('Test output differs from reference.')
    break
print('Test has finished!')
</text></argument>
</extension>
