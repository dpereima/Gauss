/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef GENERATORS_EVTGENINPHSPDECAY_H
#define GENERATORS_EVTGENINPHSPDECAY_H 1

#include "EvtGenDecay.h"

/** @class EvtGenInPhSpDecay EvtGenInPhSpDecay.h "EvtGenInPhSpDecay.h"
 *
 *  Tool to interface to EvtGen generator. Concrete implementation of
 *  a IDecayTool and inherits from EvtGenDecay
 *
 *  @author Alex Shires
 *  @date   2011-08-16
 */
class EvtGenInPhSpDecay : public extends<EvtGenDecay, IDecayTool> {
public:
  /// Standard constructor
  using extends::extends;

  /** Initialize method.
   *  In initialization:
   *  -# Call EvtGenDecay::initialize()
   *  -# Read the parameter file
   */
  StatusCode initialize() override;

  /// Finalize method
  StatusCode finalize() override;

  /// Implements IDecayTool::generateSignalDecay
  StatusCode generateSignalDecay( HepMC::GenParticle * theMother ,
                                  bool & flip ) const override;

 private:

  StatusCode doEvtGenLoop( EvtParticle* &thePart ,
                           HepMC::GenParticle * &theHepMCParticle ,
                           const EvtId & eid ) const ;

  /// load qsquared map
  StatusCode loadMap(void ) ;

  /// File name of parameter file
  boost::filesystem::path m_q2mapfile{""} ;

  std::string m_q2mapfilename ;
  Gaudi::Property<std::string> m_prefix{this,"ParamFile","Q2Param","ParamFile"} ;

  // number of bins in q2
  Gaudi::Property<int> m_q2nbins{this,"NumQ2Bins", 100,"NumQ2Bins"} ;
  Gaudi::Property<int> m_q2nmax{this,"MaxQ2Bins", 19,"MaxQ2Bins"} ;

  //vector for q2 values
  std::vector<double> m_q2mapvec ;

  //statistics
  double m_tot_calls{0.0} ;
  double m_num_evts{0.0} ;
  double m_ave_eff{0.0} ;

  Gaudi::Property<int> m_limit{this,"MaxEvtGenCalls",1000,"Maximum number of EvtGen Galls"} ;

};


#endif // GENERATORS_EVTGENDECAYINPHSP_H 1
