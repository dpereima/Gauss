/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBBOUND_DEUTERONPRODUCTION_H 
#define LBBOUND_DEUTERONPRODUCTION_H 1

// Gaudi.
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/RndmGenerators.h"

// HepMC.
#include "HepMC/GenEvent.h"
#include "HepMC/Units.h"

// ROOT.
#include "TGenPhaseSpace.h"

// Local.
#include "BoundProduction.h"
#include "GaudiRandomForROOT.h"

/** 
 * Production tool to generate deuteron bound states.
 *
 * Proton and neutrons (anti-protons and anti-neutrons) are taken from
 * a production generator, e.g. Pythia 8, and combined in pairs to
 * produce deuterons (anti-deuterons). To ensure conservation of
 * momentum and energy in the final state all productions channels are
 * required to have at least two final products, where one is a
 * deuteron. In the simple coalescence model, where traditionally
 * momentum and energy are not conserved, a deuteron and photon can be
 * used.
 * 
 * The tool is configured by specifying production channels for
 * deuterons. This is done through the 'IDs' list which specifies the
 * incoming and outgoing particles for each channel, where the
 * deuteron itself is excluded. For example the 'IDs' list '[[2212,
 * 2112, 22], [2212, 2112, 111]]' specifies two channels, the first is
 * p n -> gamma d and the second is p n -> pi0 d.

 * For each channel a mode must be specified through 'Modes'. This
 * mode specifies the parameterization of the differential
 * cross-section in k, the momentum difference between the two
 * incoming particles in their COM frame. In the example above 'Modes'
 * could be '[1, 2]'. See the 'sigma' method for more details on the
 * available modes.
 *
 * Coefficients for each parameterization must be provided through
 * 'Coefficients'. Note that the length of the 'IDs', 'Modes', and
 * 'Coefficients' must be the same, e.g. the number of specified
 * channels. Again, see the 'sigma' method for the required
 * coefficients.
 *
 * Finally, 'Norm' provides the overall normalization of the deuteron
 * production. This is a multiplicative factor of the maximum
 * differential cross-section for any of the channels and must be
 * greater than 1, otherwise channel sampling will be
 * biased. Consequently, 'Norm' is required to be 1 at a minimum.
 *
 * The default model is taken from arXiv:1504.07242 where 8 production
 * channels are defined:
 * - p n -> gamma d
 * - p n -> pi0 d
 * - p n -> pi0 pi0 d
 * - p n -> pi+ pi- d
 * - p p -> pi+ d
 * - p p -> pi+ pi0 d
 * - n n -> pi- d
 * - n n -> pi- pi0 d

 * Using this default model, the maximum differential cross-section is
 * found to be 3.178e+03 microbarn with the n n -> pi- d channel. To
 * convert the 1/sigma0 fit values from table VII of arXiv:1504.07242
 * into the multiplicative normalization factor, take 1/(value from
 * table * max cross-section). For the case of the ALICE 7 TeV
 * deuteron fit with 1/sigma0 of 2.63 inverse barn, the multiplicative
 * normalization is 119.6.
 *
 * It is also possible to specify a simple coalescence model. This can
 * be done by settings 'IDs' to '[[2212, 2112, 22]]', 'Modes' to
 * '[0]', 'Coefficients' to '[[0.228, 1]]', and 'Norm' to '1'. Here,
 * the 0.228 is the maximum allowed k for binding (in GeV) and 1 is
 * the overall normalization.
 *
 * @class  DeuteronProduction
 * @file   DeuteronProduction.h 
 * @author Sophie Baker, Philip Ilten
 * @date   2018-07-22
 */

class DeuteronProduction : public BoundProduction {
public:

  /// Default constructor.
  using BoundProduction::BoundProduction;
  /// Initialize the bound process tool.
  StatusCode boundInitialize() override;

  /// Finalize the bound process tool.
  StatusCode boundFinalize() override;
  
protected:
  
  // Methods.
  /// Bind the final particles into states.
  StatusCode bindStates(HepMC::GenEvent *theEvent) override;
  /// Build the nucleon-pair combinations and shuffle.
  void buildCombos(std::vector<HepMC::GenParticle*> &prts,
		   std::vector<std::pair<HepMC::GenParticle*,
		   HepMC::GenParticle*> > &cmbs);
  /// Bind the nucleon-pair combinations
  void bindCombos(HepMC::GenEvent *event,
		  std::vector<std::pair<HepMC::GenParticle*,
		  HepMC::GenParticle*> > &cmbs);
  /// Single pion final state fit, equations 10/13/14 of arXiv:1504.07242.
  double fit(double k, std::vector<double> &c, int i);
  /// Function to evaluate for TF1 maximizing.
  double eval(double *x, double *p);
  /**
   * Return the cross-section for a given channel.
   *
   * The cross-sections are differential in k. Note that in
   * arXiv:1504.07242 all equations are provided in GeV, so here while
   * the input k is in MeV, all coefficients treat k in GeV. The form
   * of the differential cross-section is given by the channel mode:
   * - 0: a step function where the first parameter is the cut-off and
   *      the second is the normalization.
   * - 1: equation 7 of arXiv:1504.07242, for p n -> gamma d. The first 
          parameter is the function split in k followed by the 11 (a) 
	  parameters and 2 (b) parameters.
   * - 2: equation 10 of arXiv:1504.07242 for p/n p/n -> pi d. Five
          parameters are required.
   * - 3: equations 13 and 14 of arXiv:1504.07242 for p/n p/n -> pi pi d.
          A multiple of 5 parameters must be supplied.
   */
  double sigma(double k, int chn);
  
  // Properties.
  //default configuration from arXiv:1504.07242
  Gaudi::Property<std::vector<std::vector<double>>> m_cs{this,"Coefficients",
  {
    {1.28,// p n -> gamma d, table II.
     2.3034605532591175,
    -93.663463313902028,
     2565.3904680353621,
    -25594.100560137995,
     143513.10872427333,
    -503572.89020794741,
     1149248.0196165806,
    -1723683.9119787284,
     1679348.7891145353,
    -1019888.5470232342,
     349840.35161061864,
    -51662.760038375141,
    -5.1885266705385051,
     2.9195632726211609},
     {85,// p n -> pi0 d, table III (unique initial state for first coefficient).
      1.34,
      1.77,
      0.38,
      0.096
    },
    {2.85519622e+06,// p n -> pi0 pi0 d, table IV.
     1.31114126e+01,
     2.96145497e+03},
    {6.46455516e+06,// p n -> pi+ pi- d, table V.
     1.05136338e+01,
     1.97924778e+03,
     5.36301369e+00,
     6.04534114e+05,
     2.54935423e+15,
     1.65669163e+01,
     2.32961298e+07,
     1.11937373e+01,
     2.86815089e+16
    },
    {85*2,// p p -> pi+ d, table III.
     1.34,
     1.77,
     0.38,
     0.096
    },
    {5.09870846e+15,// p p -> pi+ pi0 d, table VI.
     1.65581228e+01,
     2.33337076e+07,
     1.13304315e+01,
     2.86815089e+16
    },
    {85*2,// n n -> pi- d, table III.
     1.34,
     1.77,
     0.38,
     0.096
    },
    {5.09870846e+15,// n n -> pi- pi0 d, table VI.
     1.65581228e+01,
     2.33337076e+07,
     1.13304315e+01,
     2.86815089e+16
    }
  },"List of coefficients for each production channel."};       ///< Channel fit coefficients.
  
  Gaudi::Property<std::vector<std::vector<double>>> m_ids{this,"IDs",
  {
    {2212,2112,22},// p n -> gamma d, table II.
    {2212,2112,111},// p n -> pi0 d, table III (unique initial state for first coefficient).
    {2212,2112,111,111},// p n -> pi0 pi0 d, table IV.
    {2212,2112,211,-211},// p n -> pi+ pi- d, table V.
    {2212,2212,211},// p p -> pi+ d, table III.
    {2212,2212,211,111},// p p -> pi+ pi0 d, table VI.
    {2112,2112,-211},// n n -> pi- d, table III.
    {2112,2112,-211,111}// n n -> pi- pi0 d, table VI.
  },
  "List of particle IDs (deuteron excluded) "
  "for each production channel."};      ///< Channel particles IDs.
  
  Gaudi::Property<std::vector<int>> m_modes{this,"Modes",
    {1,2,3,3,2,3,2,3},
    "Mode for each production channel."};    ///< Channel modes.
  
  Gaudi::Property<double >m_norm{this,"Norm",0,"Multiplicative normalization, ignore if less than one."}; ///< Cross-section normalization.

  // Members.
  std::vector<std::vector<double>> m_ms;
  double                      m_nd  = 0;         ///< Number of bound deuterons.
  double                      m_nad = 0;         ///< Number of bound anti-deuterons.
  double                      m_mpi = 0;         ///< Mass of the charged pion.
  LHCb::IParticlePropertySvc* m_pps = nullptr;   ///< Particle property service.
  Rndm::Numbers               m_rflat;           ///< Flat random number generator.
  GaudiRandomForROOT*         m_rroot = nullptr; ///< Gaudi random generator for ROOT.
  TGenPhaseSpace              m_psgen;           ///< Phase-space generator for decays.
};

#endif // LBBOUND_DEUTERONPRODUCTION_H

