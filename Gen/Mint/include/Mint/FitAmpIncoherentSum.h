/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FITAMPINCOHERENTSUM_HH
#define FITAMPINCOHERENTSUM_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:03 GMT

#include <iostream>

#include "Mint/counted_ptr.h"

#include "Mint/IDalitzEventAccess.h"
#include "Mint/DalitzEventAccess.h"
#include "Mint/DalitzBoxSet.h"
#include "Mint/DalitzBWBoxSet.h"
#include "Mint/IntegCalculator.h"

#include "Mint/IGetRealEvent.h"
#include "Mint/IGetComplexEvent.h"
#include "Mint/IFastAmplitudeIntegrable.h"
#include "Mint/IIntegrationCalculator.h"

#include "Mint/ILookLikeFitAmpSum.h"

#include "Mint/FitAmpList.h"

class FitAmpIncoherentSum
: virtual public MINT::IGetRealEvent<IDalitzEvent>
, virtual public IFastAmplitudeIntegrable
, virtual public ILookLikeFitAmpSum
, public FitAmpList
{
 protected:
  static std::string IncPrefix();
 public:
  FitAmpIncoherentSum(const DalitzEventPattern& pat
	    , const char* fname=0
	    , MINT::MinuitParameterSet* pset=0
	    , const std::string& prefix=""
	    , const std::string& opt=""
	    );

  FitAmpIncoherentSum(const DalitzEventPattern& pat
	    , MINT::MinuitParameterSet* pset
	    , const std::string& prefix=""
	    , const std::string& opt=""
	    );
  FitAmpIncoherentSum(const DalitzEventPattern& pat
	    , const std::string& prefix
	    , const std::string& opt=""
	    );

  FitAmpIncoherentSum(IDalitzEventAccess* events
	    , const char* fname=0
	    , MINT::MinuitParameterSet* pset=0
	    , const std::string& prefix=""
	    , const std::string& opt=""
	    );

  FitAmpIncoherentSum(IDalitzEventAccess* events
	    , MINT::MinuitParameterSet* pset
	    , const std::string& prefix=""
	    , const std::string& opt=""
	    );
  FitAmpIncoherentSum(IDalitzEventAccess* events
	    , const std::string& prefix
	    , const std::string& opt=""
	    );

  FitAmpIncoherentSum(IDalitzEventList* events
	    , const char* fname=0
	    , MINT::MinuitParameterSet* pset=0
	    , const std::string& prefix=""
	    , const std::string& opt=""
	    );

  FitAmpIncoherentSum(IDalitzEventList* events
	    , MINT::MinuitParameterSet* pset
	    , const std::string& prefix=""
	    , const std::string& opt=""
	    );
  FitAmpIncoherentSum(IDalitzEventList* events
	    , const std::string& prefix
	    , const std::string& opt=""
	    );

  FitAmpIncoherentSum(const FitAmpIncoherentSum& other);
  FitAmpIncoherentSum(const FitAmpList& other);
  /*
     The copy constructor copies like this: There'll be 'physical'
     copies of all Amplitudes, but the FitParameters remain the
     same (pointers to the same FitParameter Object).  This is
     useful for the CP-conj coding as it is now, but perhaps a bit
     counter-intuitive.  needs to be reviewed at some point. This
     behaviour is defined in the copy constructor of the
     FitAmplitude class.
  */
  MINT::counted_ptr<FitAmpList> GetCloneSameFitParameters() const override;


  virtual DalitzBoxSet makeBoxes(double nSigma = 2){
    return FitAmpList::makeBoxes(this, nSigma);}
  virtual DalitzBoxSet makeBoxes(const DalitzEventPattern& pat
				 , double nSigma=2){
    return FitAmpList::makeBoxes(pat, this, nSigma);}

  DalitzBWBoxSet makeBWBoxes(TRandom* rnd=gRandom) override {
    return FitAmpList::makeBWBoxes(this, rnd);}
  virtual DalitzBWBoxSet makeBWBoxes(const DalitzEventPattern& pat
				     , TRandom* rnd=gRandom){
    return FitAmpList::makeBWBoxes(pat, this, rnd);}


  double getVal();
  double getVal(IDalitzEvent* evt);

  /*
  double getSmootherLargerVal();
  double getSmootherLargerVal(IDalitzEvent* evt);
  */

  MINT::counted_ptr<IIntegrationCalculator> makeIntegrationCalculator() override;
  MINT::counted_ptr<IntegCalculator> makeIntegCalculator() override;

  virtual double Prob(){
    return getVal();
  }

  /*
  virtual double SmootherLargerProb(){
    return getSmootherLargerVal();
  }
  */

  double RealVal() override {
    return Prob();
  }

  /*
  virtual double SmootherLargerRealVal(){
    return SmootherLargerProb();
  }
  */

  MINT::counted_ptr<MINT::IUnweightedEventGenerator<IDalitzEvent> >
    makeEventGenerator(TRandom* rnd=gRandom) override {
    MINT::counted_ptr<MINT::IUnweightedEventGenerator<IDalitzEvent> >
      ptr(new DalitzBWBoxSet(makeBWBoxes(rnd)));
    return ptr;
  }

  virtual ~FitAmpIncoherentSum();

  void printLargestAmp(std::ostream& os = std::cout);
  void print(std::ostream& os=std::cout) const override;
  void printNonZero(std::ostream& os=std::cout) const override;

  friend class FitAmplitude;

  FitAmpIncoherentSum& operator*=(double r);
  FitAmpIncoherentSum& operator*=(const std::complex<double>& z);
  FitAmpIncoherentSum& operator*=(const MINT::counted_ptr<MINT::IReturnComplex>& irc);

  FitAmpIncoherentSum operator*(double r) const;
  FitAmpIncoherentSum operator*(const std::complex<double>& z) const;
  FitAmpIncoherentSum operator*(const MINT::counted_ptr<MINT::IReturnComplex>& irc) const;


  FitAmpIncoherentSum& operator=(const FitAmpIncoherentSum& other);
  FitAmpIncoherentSum& operator=(const FitAmpList& other);
  FitAmpIncoherentSum& operator+=(const FitAmpIncoherentSum& other);
  FitAmpIncoherentSum operator+(const FitAmpIncoherentSum& other) const;

};

FitAmpIncoherentSum operator*(double r, const FitAmpIncoherentSum& rhs);
FitAmpIncoherentSum operator*(const std::complex<double>& z, const FitAmpIncoherentSum& rhs);
FitAmpIncoherentSum operator*(const MINT::counted_ptr<MINT::IReturnComplex>& irc
		     , const FitAmpIncoherentSum& rhs);


#endif
//
