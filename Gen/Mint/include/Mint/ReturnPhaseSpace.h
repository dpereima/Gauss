/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RETURN_PHASESPACE_HH
#define RETURN_PHASESPACE_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:00 GMT

#include "Mint/DalitzEventAccess.h"
#include "Mint/IReturnReal.h"
#include "Mint/IDalitzEventList.h"

class ReturnPhaseSpace : public DalitzEventAccess
, virtual public MINT::IGetRealEvent<IDalitzEvent>{
 public:
  ReturnPhaseSpace(IDalitzEventAccess* evts);
  ReturnPhaseSpace(IDalitzEventList* evts);
  ReturnPhaseSpace(const ReturnPhaseSpace& other);

  double RealVal() override;
};

#endif
//
