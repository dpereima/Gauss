/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ISPINFACTOR_HH
#define ISPINFACTOR_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:12 GMT

#include "Mint/IReturnReal.h"
#include <string>

class ISpinFactor : virtual public MINT::IReturnReal{
 protected:
 ISpinFactor() : MINT::IReturnReal(){}
 public:
  virtual double getVal()=0;
  double RealVal() override =0;
  // this is it, so far - but keep
  // option open to demand more
  // functionality than just returning
  // real.
  virtual std::string name() const=0;
  virtual ~ISpinFactor(){}
};

#endif
