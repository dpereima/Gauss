/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INTEG_CALCULATOR_HH
#define INTEG_CALCULATOR_HH

#include "Mint/IIntegrationCalculator.h"
#include "Mint/FitAmpPairList.h"
#include "Mint/counted_ptr.h"
#include "Mint/DalitzHistoSet.h"
#include "Mint/IDalitzEvent.h"
#include "Mint/FitFractionList.h"
#include <iostream>

namespace MINT{
  class Minimiser;
}

class IntegCalculator : public virtual IIntegrationCalculator{
 protected:
  FitAmpPairList _withEff, _noEff;

  FitAmpPairList& withEff() {return _withEff;}
  FitAmpPairList& noEff()   {return _noEff;}

  static std::string dirNameWithEff();
  static std::string dirNameNoEff();
  bool makeDirectories(const std::string& asSubdirOf=".")const;
 public:
  IntegCalculator();
  IntegCalculator(const IntegCalculator& other);
  IntegCalculator(const FitAmpPairList& wEff);
  MINT::counted_ptr<IIntegrationCalculator>
    clone_IIntegrationCalculator() const override;

  const FitAmpPairList& withEff()const {return _withEff;}
  const FitAmpPairList& noEff()const {return _noEff;}

  void setEfficiency(MINT::counted_ptr<IGetDalitzEvent> eff);
  void unsetEfficiency();
  double efficiency(IDalitzEvent* evtPtr);

  virtual void addAmps(FitAmplitude* a1, FitAmplitude* a2);
  void addEvent(IDalitzEvent* evtPtr, double weight=1) override;
  void addEvent(MINT::counted_ptr<IDalitzEvent> evtPtr
			, double weight=1) override;

  virtual bool add(const IntegCalculator& other);
  virtual bool add(const IntegCalculator* other);
  virtual bool add(const MINT::const_counted_ptr<IntegCalculator>& other);

  virtual bool append(const IntegCalculator& other);
  virtual bool append(const IntegCalculator* other);
  virtual bool append(const MINT::const_counted_ptr<IntegCalculator>& other);

  int numEvents()   const override;
  double integral() const override;
  double variance() const override;

  bool makeAndStoreFractions(MINT::Minimiser* mini=0) override;
  double getFractionChi2() const override;

  DalitzHistoSet histoSet() const override;
  void saveEachAmpsHistograms(const std::string& prefix) const override;
  virtual std::vector<DalitzHistoSet> GetEachAmpsHistograms();
  void doFinalStats(MINT::Minimiser* mini=0) override;

  bool save(const std::string& dirname) const override;
  bool retrieve(const std::string& commaSeparatedList) override;
  virtual bool retrieveSingle(const std::string& dirname);

  FitFractionList getFractions() const override;

  void print(std::ostream& os=std::cout) const override;

  virtual ~IntegCalculator(){}

};

#endif
//
