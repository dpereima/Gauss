/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PLOTSET_HH
#define PLOTSET_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:00 GMT

#include <vector>
#include <string>

#include "TObject.h"
#include "TFile.h"

class PlotSet : public std::vector<TObject*>{

 public:
  PlotSet()
    : std::vector<TObject*>(){}

  PlotSet(const PlotSet& other)
    : std::vector<TObject*>(other)
    {}
  /* beware!!!! this does not do
     the memory management for
     the plots, just passes
     around a set of pointers!
  */

  void save(const std::string& filename);
};
#endif
//
