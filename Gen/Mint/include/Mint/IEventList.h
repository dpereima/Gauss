/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IEVENTLISTB_HH
#define IEVENTLISTB_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:54 GMT

#include "Mint/Utils.h"
#include "Mint/ILoopable.h"
#include "Mint/IBasicEventAccess.h"

/*
  loop like this:

  list.Start();
  while(list.Next()){
    do somthing;
  }

  i.e. 1st call to Next
  after Start gives first event.
*/

namespace MINT{

template<typename RETURN_TYPE>
class IEventList : virtual public ILoopable
, virtual public IBasicEventAccess<RETURN_TYPE>
{
 protected:
  IEventList(){}
 public:

  virtual unsigned int size() const=0;
  RETURN_TYPE * getEvent() override =0;
  const RETURN_TYPE * getEvent() const override =0;
  /* these are declared in ILoopable */
  bool Next()   override =0;
  bool Start()  override =0;
  //  virtual bool Delete()  =0;

  virtual const RETURN_TYPE * getREvent(unsigned int i) const=0;

  virtual ~IEventList(){}

  // in future I'll delete the next two, keep for now, for backw. compat:
  virtual RETURN_TYPE * currentEvent(){return getEvent();}
  virtual const RETURN_TYPE * currentEvent() const{return getEvent();}

  const IEventList<RETURN_TYPE> * getEventRecord() const override {return this;}
  IEventList<RETURN_TYPE> * getEventRecord() override {return this;}
};
}//namespace MINT
#endif
//
