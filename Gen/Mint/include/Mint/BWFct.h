/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BW_FCT_HH
#define BW_FCT_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:58 GMT

#include "TRandom.h"

//#include "Mint/counted_ptr.h"
#include "Mint/IGenFct.h"

class BWFct : virtual public IGenFct{
  DalitzCoordinate _coord;
  double _M, _G;

  double rhoMi() const;
  double rhoMa() const;
  double norm() const;

  double unitFactor() const;

 public:
  BWFct(const DalitzCoordinate& c, double M, double Gamma);
  BWFct(const BWFct& other);
  //  virtual MINT::counted_ptr<IGenFct> Clone() const;

  void setLimits(double sMin, double sMax) override;
  double getSMi()const override {return _coord.min();}
  double getSMa()const override {return _coord.max();}
  double generateRho(TRandom* rnd=gRandom) const override;
  double generate(TRandom* rnd=gRandom) const override;

  DalitzCoordinate getCoordinate() const override;
  void setCoordinate(const DalitzCoordinate& c) override;

  double generatingPDFValue(double sij) const override;
  double generatingFctValue(double sij) const override;
  double integral() const override;
  // normalised to limits passed via DalitzCoordinate!!!

  bool flat() const override {return false;}

  // co-ordinate transformation stuff (by default there is none)
  double coordTransformFromS(double s)const override;
  double coordTransformToS(double rho)const override;
  virtual double transformedPDFValue(double rho)const;
  double transformedFctValue(double rho)const override;
  double transformedFctMax()const override;
  virtual double transformedIntegral()const;
  double getRhoMi()const override {return rhoMi();}
  double getRhoMa()const override {return rhoMa();}

  virtual ~BWFct(){};
};

#endif
//
