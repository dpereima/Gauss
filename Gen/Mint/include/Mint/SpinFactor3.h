/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SPINFACTOR_THREE_BODY_HH
#define SPINFACTOR_THREE_BODY_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:13 GMT

#include "Mint/SpinFactor.h"
#include "Mint/counted_ptr.h"
#include <iostream>

class SpinFactor3 : public SpinFactor{
 protected:
  static DecayTree* _exampleDecay;
  MINT::const_counted_ptr<AssociatedDecayTree> R; // resonance.
  int _spin;
  bool _nonResonant;
  bool parseTree();

  double nonResVal() const{return 1;}
  double spinZeroVal() const{ return 1;}
  double spinOneVal();
  double spinTwoVal();

  double spinOneFromMasses(); //db
  double spinTwoFromMasses(); //db

  double spinOneFromZemach(); //experimental

  bool setSpin();

 public:
  SpinFactor3(IDalitzEventAccess* events
	      , const DecayTree& theDecay)
    : SpinFactor(events, theDecay, 3)
    , R(0)
    , _spin(-1)
    , _nonResonant(false)
    {
      if( ! parseTree()){
	throw "error in parseTree in constructor of SpinFactor3";
      }
    }

  double getVal() override;
  static const DecayTree& getExampleDecay();
  const DecayTree& exampleDecay() override;
  std::string name() const override {
    return "SpinFactor3(" + theDecay().oneLiner() + ")";
  }
  void printYourself(std::ostream& os = std::cout)const override;
  virtual ~SpinFactor3(){}
};

#endif
//
