/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SINGLE_EVENT_LIST_HH
#define SINGLE_EVENT_LIST_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:54 GMT

// a simple wrapper, useful mainly in IEventAccess, to treat
// events like eventlists...

#include "Mint/IEventList.h"

namespace MINT{
template<typename RETURN_TYPE>
class SingleEventList : virtual public IEventList<RETURN_TYPE>{
  RETURN_TYPE* _eventPtr;
  bool _alternator;
 public:
  SingleEventList(RETURN_TYPE* ep=0){
    _eventPtr = ep;
    _alternator = true;
  }
  SingleEventList(const SingleEventList& other)
    : _eventPtr(other._eventPtr)
    , _alternator(other._alternator)
    {
    }
  virtual ~SingleEventList(){};

  unsigned int size() const override {
    if(0 == _eventPtr) return 0;
    else return 1;
  }
  RETURN_TYPE * getEvent() override {return _eventPtr;}
  const RETURN_TYPE * getEvent() const override {return _eventPtr;}
  const RETURN_TYPE * getREvent(unsigned int i=0) const override {
    if (i != 0) return 0;
    return _eventPtr;}
  /* these are declared in ILoopable */
  bool Next() override {_alternator = ! _alternator; return _alternator;}
  bool Start() override {_alternator = false; return true;}
  virtual bool Delete(){_eventPtr = 0; return true;}

  // in future I'll delete the next two, keep for now, for backw. compat:
  RETURN_TYPE * currentEvent() override {return getEvent();}
  const RETURN_TYPE * currentEvent() const override {return getEvent();}

  const IEventList<RETURN_TYPE> * getEventRecord() const override {return this;}
  IEventList<RETURN_TYPE> * getEventRecord() override {return this;}
};

}//namespace MINT

#endif
