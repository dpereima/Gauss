/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FLAT_FCT_HH
#define FLAT_FCT_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:58 GMT

#include "TRandom.h"

//#include "Mint/counted_ptr.h"
#include "Mint/IGenFct.h"

class FlatFct : virtual public IGenFct{
 protected:
  DalitzCoordinate _coord;
  double _boxLimit_min, _boxLimit_max;
  double _min, _max;
  void redoLimits();
  // the limits passed through _coord and changed with setLimits
  // are the limits over
  // which the function is defined, the other limits are on top of that
  // it will return non-zero values if within both limits.
  // Important for the way DalitzBWArea interacts with this.
 public:
  virtual double min() const;
  virtual double max() const;

  FlatFct(const DalitzCoordinate& c);
  FlatFct(const FlatFct& other);
  //  virtual counted_ptr<IGenFct> Clone() const;

  void setLimits(double sMin, double sMax) override;
  virtual void setBoxLimits(double sMin, double sMax);
  virtual void resetBoxLimits();
  double getSMi()const override {return _coord.min();}
  double getSMa()const override {return _coord.max();}
  double generate(TRandom* rnd=gRandom) const override;
  double generateRho(TRandom* rnd=gRandom) const override;

  DalitzCoordinate getCoordinate() const override;
  void setCoordinate(const DalitzCoordinate& c) override;

  double generatingPDFValue(double sij) const override;
  double generatingFctValue(double sij) const override;
  double transformedFctValue(double sij) const override;
  double transformedFctMax()const override;
  double integral()const override;
  // normalised to limits passed via DalitzCoordinate & boxLimits

  bool flat()const override {return true;}

  virtual ~FlatFct(){};
};

#endif
//
