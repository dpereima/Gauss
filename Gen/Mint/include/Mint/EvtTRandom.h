/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MINT_EVTTRANDOM_HH
#define MINT_EVTTRANDOM_HH 1

// Include files
#ifndef ROOT_TRandom
#include "TRandom.h"
#endif

/** @class EvtTRandom EvtTRandom.h Mint/EvtTRandom.h
 *
 *
 *  @author Philip Hunt (LHCB)
 *  @date   2013-09-20
 */
namespace MINT {
  class IEvtRandom
  {
  public:
    virtual ~IEvtRandom() = default;
    virtual void SetSeed(UInt_t seed)=0;
    virtual UInt_t GetSeed() const=0;
    virtual double Rndm()=0;
  };

  class EvtTRandom : public TRandom {
  public:
    EvtTRandom(MINT::IEvtRandom* rnd=NULL);
    virtual ~EvtTRandom( ); ///< Destructor
    UInt_t GetSeed() const override;
    using TRandom::Rndm;
    Double_t Rndm() override;
    void RndmArray(Int_t n, Float_t* array) override;
    void RndmArray(Int_t n, Double_t* array) override;
    void SetSeed(ULong_t seed=0) override;
    enum {
      kBigNumber=1234567890
    };
  private:
    MINT::IEvtRandom* m_rnd;
  };
}

#endif // MINT_EVTTRANDOM_HH
