/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:13 GMT
#ifndef SPINFACTOR_MAKER
#define SPINFACTOR_MAKER

#include <iostream>
#include "Mint/DecayTree.h"
#include "Mint/ISpinFactor.h"

#include "Mint/IDalitzEventAccess.h"

void PrintAllSpinFactors(std::ostream& out = std::cout);

ISpinFactor* SpinFactorMaker(const DecayTree& thisDcy
			     , IDalitzEventAccess* dad
			     , char SPD_Wave='?'
			     , const std::string& lopt=""
			    );
ISpinFactor* SpinFactorMaker4Body(const DecayTree& thisDcy
				  , IDalitzEventAccess* dad
				  , char SPD_Wave='?'
				  //, const std::string& lopt=""
				  );
#endif
//
