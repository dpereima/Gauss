/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:06 GMT
#ifndef DATATHROWERAWAY
#define DATATHROWERAWAY

#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooArgList.h"
#include "RooDataSet.h"
double getLargestValue(const RooAbsReal& pdf
		       , RooArgList& emms
		       , RooDataSet* data
		       );
RooDataSet* dataThrowerAway(const RooAbsReal& pdf
			    , RooArgList& emms
			    , RooDataSet* data
			    , int maxEvents = -1
			    , double largestValue = -9999
			    , bool saveOldData = false
			    );

#endif
//
