/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BcDaughtersInLHCb.h,v 1.3 2008-09-03 09:04:49 gcorti Exp $
#ifndef GENERATORS_BCDAUGHTERSINLHCBANDMASSCUT_H
#define GENERATORS_BCDAUGHTERSINLHCBANDMASSCUT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Transform4DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"

// Forward declaration
class IDecayTool ;

/** @class BcDaughtersInLHCbAndMassCut BcDaughtersInLHCbAndMassCut.h
 *
 *  Tool to keep events with daughters from B_c
 *  in LHCb acceptance.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-24
 */

class BcDaughtersInLHCbAndMassCut : public extends<GaudiTool,IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;
  
  StatusCode initialize( ) override;   ///< Initialize method

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

  StatusCode finalize( ) override;   ///< Finalize method

protected:



private:
  /// Decay tool
  IDecayTool*  m_decayTool{nullptr};

  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Minimum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged daughters"} ;

  /** Maximum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged daughters"} ;

  /** Minimum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin",5 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for neutral daughters"} ;

  /** Maximum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for neutral daughters"} ;

  /// Name of the decay tool to use
  Gaudi::Property<std::string> m_decayToolName{this,"DecayTool","EvtGenDecay","Name of the decay tool to use"} ;

  /** PDG ID of the two particle pair used to compute Mass
   * (set by options)
   */
  Gaudi::Property<int> m_LeptonOneID{this,"LeptonOneID",-13,"PDG ID of the two particle pair used to compute Mass (particle 1)"} ;
  Gaudi::Property<int> m_LeptonTwoID{this,"LeptonTwoID", 13,"PDG ID of the two particle pair used to compute Mass (particle 2)"} ;

  /** Switch for Minimal PT cut on the two daughters
   * (defined by m_LeptonOneID, m_LeptonOTwoID) and cut value
   * (set by options)
   */
  Gaudi::Property<bool> m_PreselDausPT{this,"PreselDausPT",false,"Switch for Minimal PT cut on the two daughters"} ;
  Gaudi::Property<double> m_DausPTMin{this,"DausPTMin",0.2 * Gaudi::Units::GeV,"Minimal PT cut on the two daughters"} ;

  /** Switch for minimal mass cut on the two daughters pair
   * (defined by m_LeptonOneID, m_LeptonOTwoID) and cut value
   * (set by options)
   */
  Gaudi::Property<bool> m_PreselMinMass{this,"PreselMinMass",false,"Switch for minimal mass cut on the two daughters pair"} ;
  Gaudi::Property<double> m_mmMinMass{this,"mmMinMass",4.5 * Gaudi::Units::GeV,"Minimal mass cut on the two daughters pair"} ;

  /** Switch for maximal PT cut on the two daughters pair
   * (defined by m_LeptonOneID, m_LeptonOTwoID) and cut value
   * (set by options)
   */
  Gaudi::Property<bool> m_PreselMaxMass{this,"PreselMaxMass",false,"Switch for maximal PT cut on the two daughters pair"} ;
  Gaudi::Property<double> m_mmMaxMass{this,"mmMaxMass",6. * Gaudi::Units::GeV,"Maximal PT cut on the two daughters pair"} ;

  int  m_sigBcPID{541}        ;  ///< PDG Id of the B_c

};
#endif // GENERATORS_BCDAUGHTERSINLHCBANDMASSCUT_H
