/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBHARD_HARDPPRODUCTION_H
#define LBHARD_HARDPPRODUCTION_H 1

// Generators.
#include "Generators/IProductionTool.h"

// Parton showers packages.
#include "LbPythia/PythiaProduction.h"
#include "LbPythia8/Pythia8Production.h"

/**
 * Production tool base class to generate a hard process and then
 * shower the event.
 *
 * @class  HardProduction
 * @file   HardProduction.h
 * @author Philip Ilten
 * @date   2015-05-01
 */
class HardProduction : public extends<GaudiTool, IProductionTool> {
 public:
  typedef std::vector<std::string> CommandVector;

  /// Default constructor.
  using extends::extends;

  /// Initialize the tool.
  StatusCode initialize() override;

  /// Initialize the generator.
  StatusCode initializeGenerator() override;

  /// Finalize the tool.
  StatusCode finalize() override;

  /// Generate an event.
  StatusCode generateEvent(HepMC::GenEvent *theEvent,
				   LHCb::GenCollision *theCollision) override;

  /// Set a particle stable.
  void setStable(const LHCb::ParticleProperty *thePP) override;

  /// Update a particle.
  void updateParticleProperties(const LHCb::ParticleProperty *thePP) override;

  /// Turn off fragmentation.
  void turnOnFragmentation() override;

  /// Turn on fragmentation.
  void turnOffFragmentation() override;

  /// Hadronize an event.
  StatusCode hadronize(HepMC::GenEvent *theEvent,
		       LHCb::GenCollision *theCollision) override;

  /// Save the event record.
  void savePartonEvent(HepMC::GenEvent* theEvent) override;

  /// Retrieve the event record.
  void retrievePartonEvent( HepMC::GenEvent* theEvent) override;

  /// Print the running conditions.
  void printRunningConditions() override;

  /// Returns whether a particle has special status.
  bool isSpecialParticle(const LHCb::ParticleProperty* thePP) const override;

  /// Setup forced fragmentation.
  StatusCode setupForcedFragmentation(const int thePdgId) override;

  /**
   * Initialize the hard process tool. Called before the shower tool
   * is initialized.
   */
  virtual StatusCode hardInitialize();

  /**
   * Initialize the hard process generator. Called before the shower
   * generator is initialized.
   */
  virtual StatusCode hardInitializeGenerator();

  /// Finalize the hard process tool.
  virtual StatusCode hardFinalize();

  /// Set a particle stable for the hard process generator.
  virtual void hardSetStable(const LHCb::ParticleProperty *thePP);

  /// Update a particle for the hard process generator.
  virtual void hardUpdateParticleProperties(const LHCb::ParticleProperty
					    *thePP);

  /// Print the running conditions for the hard process generator.
  virtual void hardPrintRunningConditions();

  /**
   * Returns whether a particle has special status for the hard
   * process generator.
   */
  virtual bool hardIsSpecialParticle(const LHCb::ParticleProperty* thePP) const;

  // Members needed externally.
  PythiaProduction *m_pythia{nullptr};   ///< The Pythia tool (may be null).
  Pythia8Production *m_pythia8{nullptr}; ///< The Pythia 8 tool (may be null).

protected:

  // Properties.
  Gaudi::Property<CommandVector> m_userSettings{this,"Commands",{},"The commands to pass to the hard process generator."}; ///< The user settings vector.
  Gaudi::Property<std::string> m_beamToolName{this,"BeamToolName","CollidingBeams","The beam tool to use."};   ///< The beam tool name.
  Gaudi::Property<std::string> m_showerToolName{this,"ShowerToolName","PythiaProduction",
  "The shower tool to use: PythiaProduction, Pythia8Production,"
  " or HerwigppProduction."}; ///< The shower generator name.

  // Members.
  int m_nEvents{0};                ///< Number of events.
  IProductionTool *m_shower{nullptr};    ///< The shower tool.
  IProductionTool *m_hard{nullptr};      ///< The hard process tool (may be null).
  IBeamTool *m_beamTool{nullptr};        ///< The Gaudi beam tool.

  /**
   * Pythia 8 user hooks  vetor pointer.
   *
   * The Pythia 8 tool deletes this object, so must be malloced with
   * new and should not be deleted by this tool.
   */
  std::vector <Pythia8::UserHooks*> m_hooks;
  /**
   * Pythia 8 Les Houches Accord user process pointer.
   *
   * The Pythia 8 tool deletes this object, so must be malloced with
   * new and should not be deleted by this tool.
   */
  Pythia8::LHAup *m_lhaup{nullptr};
};

#endif // LBHARD_HARDPRODUCTION_H
