/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LbHard.
#include "LbHard/HardProduction.h"

//-----------------------------------------------------------------------------
//  Implementation file for class: HardProduction
//
//  2015-05-01 : Philip Ilten
//-----------------------------------------------------------------------------


//=============================================================================
// Initialize the tool.
//=============================================================================
StatusCode HardProduction::initialize() {

  // Print the initialization banner.
  debug() << "Entered initialize()." << endmsg;
  always() << "============================================================="
           << "=====" << endmsg;
  always() << "Using as production engine " << this->type() << endmsg;
  always() << "============================================================="
           << "=====" << endmsg;

  // Initialize the Gaudi tool.
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) Exception("Failed to initialize the Gaudi tool.");

  // Initialize the beam tool.
  if (!m_beamTool) m_beamTool = tool<IBeamTool>(m_beamToolName.value(), this);
  if (!m_beamTool) Exception("Failed to initialize the IBeamTool.");

  // Initialize the hard process tool.
  sc = hardInitialize();
  if (sc.isFailure()) Exception("Failed to initialize the generator.");

  // Initialize the shower tool.
  if (!m_shower) {
    m_shower = tool<IProductionTool>(m_showerToolName.value(), this);
    if (m_showerToolName.value() == "PythiaProduction") {
      if (!m_pythia) m_pythia = dynamic_cast<PythiaProduction*>(m_shower);
      m_pythia->m_beamToolName = m_beamToolName.value();
    } else if (m_showerToolName.value() == "Pythia8Production") {
      if (!m_pythia8) m_pythia8 = dynamic_cast<Pythia8Production*>(m_shower);
      m_pythia8->m_beamToolName = m_beamToolName.value();
      if(m_hooks.size()) m_pythia8->m_hooks = m_hooks;  
      if (m_lhaup) m_pythia8->m_lhaup = m_lhaup;
    } else Exception("Unavailable shower generator requested.");
    if (!m_shower) Exception("Failed to initialize the shower generator.");
  }
  m_nEvents = 0;
  return sc;
}

//=============================================================================
// Initialize the generator.
//=============================================================================
StatusCode HardProduction::initializeGenerator() {
  if (!m_shower) return StatusCode::FAILURE;
  StatusCode sc = hardInitializeGenerator();
  if (m_shower && sc) sc = m_shower->initializeGenerator();
  return sc;
}

//=============================================================================
// Finalize the tool.
//=============================================================================
StatusCode HardProduction::finalize() {
  StatusCode sc = hardFinalize();
  if ( sc ) sc = GaudiTool::finalize();
  return sc;
}

//=============================================================================
// Generate an event.
//=============================================================================
StatusCode HardProduction::generateEvent(HepMC::GenEvent *theEvent,
                                         LHCb::GenCollision *theCollision) {
  if (!m_shower) return StatusCode::FAILURE;
  ++m_nEvents;
  return m_shower->generateEvent(theEvent, theCollision);
}

//=============================================================================
// Set a particle stable.
//=============================================================================
void HardProduction::setStable(const LHCb::ParticleProperty *thePP) {
  hardSetStable(thePP);
  if (m_shower) m_shower->setStable(thePP);
}

//=============================================================================
// Update a particle.
//=============================================================================
void HardProduction::updateParticleProperties(const LHCb::ParticleProperty
                                              *thePP) {
  if (!hardIsSpecialParticle(thePP)) hardUpdateParticleProperties(thePP);
  if (m_shower && !m_shower->isSpecialParticle(thePP))
    m_shower->updateParticleProperties(thePP);
}

//=============================================================================
// Turn on and off fragmentation.
//=============================================================================
void HardProduction::turnOnFragmentation()
{if (m_shower) m_shower->turnOnFragmentation();}

void HardProduction::turnOffFragmentation()
{if (m_shower) m_shower->turnOffFragmentation();}

//=============================================================================
// Hadronize an event.
//=============================================================================
StatusCode HardProduction::hadronize(HepMC::GenEvent* theEvent,
                                     LHCb::GenCollision* theCollision) {
  if (!m_shower) return StatusCode::FAILURE;
  return m_shower->hadronize(theEvent, theCollision);
}

//=============================================================================
// Save the event record.
//=============================================================================
void HardProduction::savePartonEvent(HepMC::GenEvent* theEvent)
{if (m_shower) m_shower->savePartonEvent(theEvent);}

//=============================================================================
// Retrieve the event record.
//=============================================================================
void HardProduction::retrievePartonEvent(HepMC::GenEvent* theEvent)
{if (m_shower) m_shower->retrievePartonEvent(theEvent);}

//=============================================================================
// Print the running conditions.
//=============================================================================
void HardProduction::printRunningConditions() {
  hardPrintRunningConditions();
  if (m_shower) m_shower->printRunningConditions();
}

//=============================================================================
// Returns whether a particle has special status.
//=============================================================================
bool HardProduction::isSpecialParticle(const LHCb::ParticleProperty */*thePP*/)
  const {return false;}

//=============================================================================
// Setup forced fragmentation.
//=============================================================================
StatusCode HardProduction::setupForcedFragmentation(const int thePdgId) {
  if (!m_shower) return StatusCode::FAILURE;
  return m_shower->setupForcedFragmentation(thePdgId);
}

//=============================================================================
// The hard process methods.
//=============================================================================
StatusCode HardProduction::hardInitialize() {return StatusCode::SUCCESS;}

StatusCode HardProduction::hardInitializeGenerator() {
  if (!m_hard || m_hard == m_shower) return StatusCode::SUCCESS;
  return m_hard->initializeGenerator();
}

StatusCode HardProduction::hardFinalize() {return StatusCode::SUCCESS;}

void HardProduction::hardSetStable(const LHCb::ParticleProperty *thePP)
{if (m_hard && m_hard != m_shower) m_hard->setStable(thePP);}

void HardProduction::hardUpdateParticleProperties(const LHCb::ParticleProperty
						  *thePP)
{if (m_hard && m_hard != m_shower) m_hard->updateParticleProperties(thePP);}

void HardProduction::hardPrintRunningConditions()
{if (m_hard && m_hard != m_shower) m_hard->printRunningConditions();}

bool HardProduction::hardIsSpecialParticle(const LHCb::ParticleProperty
					   */*thePP*/) const {return false;}

//=============================================================================
// The END.
//=============================================================================
