###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/LbHerwigpp
--------------
#]=======================================================================]

# TODO: [New CMake] investigate
string(REPLACE "-Wl,--no-undefined" "" CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}")

gaudi_add_module(LbHerwigpp
    SOURCES
        src/component/HerwigppProduction.cpp
    LINK
        ThePEG::ThePEG
        Gauss::GeneratorsLib
)

gaudi_add_library(LbHerwigppRandom
    SOURCES
        src/Lib/GaudiRandomForHerwigpp.cpp
    LINK
        PUBLIC
            ThePEG::ThePEG
            Gaudi::GaudiKernel
            GSL::gsl
)
