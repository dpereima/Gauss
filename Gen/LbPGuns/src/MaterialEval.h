/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MaterialEval.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_MATERIALEVAL_H
#define PARTICLEGUNS_MATERIALEVAL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h"
// from ParticleGuns
#include "LbPGuns/IParticleGunTool.h"

/** @class MaterialEval MaterialEval.h
 *
 *  Particle gun to evalute material.
 *  Generate either random flat distributions or regular scan with a given
 *  step of particles. The homogeneus distribution can be in an x-y rectangular
 *  area or in eta-phi. The origin is a fixed point with fix momentum.
 *
 *  @author Witek Pokorski
 *  @author Gloria Corti, port code from Yasmine Amhis
 *  @date   last modified on 2007-08-21
 *
 */
class MaterialEval : public extends<GaudiTool, IParticleGunTool> {
public:

  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;    ///< Algorithm initialization

  /// Specialized method called by base class in execute
  void generateParticle( Gaudi::LorentzVector & fourMomentum ,
                         Gaudi::LorentzVector & origin , int & pdgId ) override;

  /// Print counters
  void printCounters( ) override { ; } ;

protected:

  /// Generate 3-momentum for a uniformly flat distribution in x-y plane
  void generateUniformXY(double& px, double& py, double& pz);

  /// Generate 3-momentum for a regular grid in x-y plane
  StatusCode generateGridXY(double& px, double& py, double& pz);

  /// Generate 3-momentum for a uniformly flat distribution in eta-phi plane
  void generateUniformEtaPhi(double& px, double& py, double& pz);

  /// Generate 3-momentum for a regular grid in eta-phi plane
  StatusCode generateGridEtaPhi(double& px, double& py, double& pz);


private:

  Gaudi::Property<double> m_xVtx{this, "Xorig", 0. * Gaudi::Units::mm, "x position of the vertex"};   ///< x position of the vertex
  Gaudi::Property<double> m_yVtx{this, "Yorig", 0. * Gaudi::Units::mm, "y position of the vertex"};   ///< y position of the vertex
  Gaudi::Property<double> m_zVtx{this, "Zorig", 0. * Gaudi::Units::mm, "z position of the verte"};   ///< z position of the verte
  
  Gaudi::Property<double> m_zplane{this, "ZPlane", 10. * Gaudi::Units::m, "z position of the rectangular target"}; ///< z position of the rectangular target
  Gaudi::Property<double> m_xmin{this, "Xmin", -3.2 * Gaudi::Units::m, "x position of the left side of the rectangular target"};   ///< x position of the left side of the rectangular target
  Gaudi::Property<double> m_xmax{this, "Xmax", 3.2 * Gaudi::Units::m, "x position of the right side of the rectangular target"};   ///< x position of the right side of the rectangular target
  Gaudi::Property<double> m_ymin{this, "Ymin", -2.6 * Gaudi::Units::m, "y position of the bottom side of the target"};   ///< y position of the bottom side of the target
  Gaudi::Property<double> m_ymax{this, "Ymax", 2.6 * Gaudi::Units::m, "y position of the top side of the target"};   ///< y position of the top side of the target
  Gaudi::Property<double> m_ptotal{this, "ModP", 500. * Gaudi::Units::GeV, "Momentum of the particles"}; ///< Momentum of the particles
  
  Gaudi::Property<double> m_minEta{this, "MinEta", 2.1 , "minimum value of eta"}; ///< minimum value of eta
  Gaudi::Property<double> m_maxEta{this, "MaxEta", 4.9 , "maximum value of eta"}; ///< maximum value of eta
  Gaudi::Property<double> m_minPhi{this, "MinPhi", 0.0 * Gaudi::Units::rad, "minimum value of phi"}; ///< minimum value of phi
  Gaudi::Property<double> m_maxPhi{this, "MaxPhi", 6.28 * Gaudi::Units::rad , "maximum value of phi"}; ///< maximum value of phi

  Gaudi::Property<bool> m_useGrid{this, "UseGrid", false, "Flag to generate in a regular grid or randomly flat"};  ///< Flag to generate in a regular grid or randomly flat
  Gaudi::Property<bool> m_etaPhi{this, "EtaPhi", false, "Flag to generate in eta-phi plane or x-y"};   ///< Flag to generate in eta-phi plane or x-y
  
  Gaudi::Property<double> m_nXstep{this, "NStepsInX", 320, "Number of x steps in the grid"};   ///< Number of x steps in the grid
  Gaudi::Property<double> m_nYstep{this, "NStepsInY", 260, "Number of y steps in the grid"};   ///< Number of y steps in the grid
  Gaudi::Property<double> m_nEtastep{this, "NStepsInEta", 280, "Number of eta steps in the grid"}; ///< Number of eta steps in the grid
  Gaudi::Property<double> m_nPhistep{this, "NStepsInPhi", 314, "Number of phi steps in the grid"}; ///< Number of phi steps in the grid
  
  Gaudi::Property<int> m_pdgCode{this, "PdgCode", 12,"PDG Code of the generated particles"};     ///< PDG Code of the generated particles

  double m_mass{0.};     ///< Mass of the particle to generate

  double m_dXstep{0.};   ///< Size of step in x
  double m_dYstep{0.};   ///< Size of step in y
  double m_dEtastep{0.}; ///< Size of step in eta
  double m_dPhistep{0.}; ///< Size of step in phi

  int m_counterX{0};    ///< Size of step in x
  int m_counterY{0};    ///< Size of step in y
  int m_counterEta{0};  ///< Size of step in eta
  int m_counterPhi{0};  ///< Size of step in phi

  Rndm::Numbers m_flatGenerator; ///< Random number generator
};

#endif // PARTICLEGUNS_MATERIALEVAL_H
