/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FixedMomentum.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $

// This class
#include "FixedMomentum.h"

// From STL
#include <cmath>

// FromGaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "GaudiKernel/IRndmGenSvc.h" 

//===========================================================================
// Implementation file for class: FixedMomentum
//
// 2008-05-18: Patrick Robbe adaptation to tool structure
//===========================================================================

DECLARE_COMPONENT( FixedMomentum )


//===========================================================================
// Initialize Particle Gun parameters
//===========================================================================
StatusCode FixedMomentum::initialize() {
  StatusCode sc = GaudiTool::initialize() ;
  if ( ! sc.isSuccess() ) return sc ;

  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_flatGenerator.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if ( ! sc.isSuccess() ) 
    return Error( "Cannot initialize flat generator" ) ;
  
  // Get the mass of the particle to be generated
  //
  LHCb::IParticlePropertySvc* ppSvc = 
    svc< LHCb::IParticlePropertySvc >( "LHCb::ParticlePropertySvc" , true ) ;

  // setup particle information
  m_masses.clear();

  info() << "Particle type chosen randomly from :";
  PIDs::iterator icode ;
  for ( icode = m_pdgCodes.value().begin(); icode != m_pdgCodes.value().end(); ++icode ) {
    const LHCb::ParticleProperty * particle = 
      ppSvc->find( LHCb::ParticleID( *icode ) ) ;
    m_masses.push_back( ( particle->mass() ) ) ;
    m_names.push_back( particle->particle() ) ;
    info() << " " << particle->particle() ;
  }
  
  info() << endmsg ;

  info() << "Momentum: (" << m_px.value() / Gaudi::Units::GeV << " GeV, " 
          << m_py.value() / Gaudi::Units::GeV << " GeV, " << m_pz.value() / Gaudi::Units::GeV << " GeV)" 
          << endmsg ;
 
  release( ppSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return sc ;
}

//===========================================================================
// Generate the particles
//===========================================================================
void FixedMomentum::generateParticle( Gaudi::LorentzVector & momentum , 
                                      Gaudi::LorentzVector & origin , 
                                      int & pdgId ) {  
  unsigned int currentType = 
    (unsigned int)( m_pdgCodes.value().size() * m_flatGenerator() );
  // protect against funnies
  if ( currentType >= m_pdgCodes.value().size() ) currentType = 0; 

  origin.SetCoordinates( 0. , 0. , 0. , 0. ) ;  
  
  momentum.SetPx( m_px.value() ) ; momentum.SetPy( m_py.value() ) ; momentum.SetPz( m_pz.value() ) ;
  momentum.SetE( std::sqrt( m_masses[currentType] * m_masses[currentType] + 
                            momentum.P2() ) ) ;  
                        
  pdgId = m_pdgCodes.value()[ currentType ] ;                      
    
  debug() << " -> " << m_names[ currentType ] << endmsg 
          << "   P   = " << momentum << endmsg ;
}

