/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Cosmics.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
// Include files

// This class
#include "Cosmics.h"

// From STL
#include <cmath>

// From Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "GaudiKernel/IRndmGenSvc.h"

//===========================================================================
// Implementation file for class: Cosmics
//
// 2008-05-18: Giulia Manca
//===========================================================================

DECLARE_COMPONENT( Cosmics )


//===========================================================================
// Initialize Particle Gun parameters
//===========================================================================
StatusCode Cosmics::initialize() {
  StatusCode sc = GaudiTool::initialize() ;
  if ( ! sc.isSuccess() ) return sc ;

  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_flatGenerator.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if ( ! sc.isSuccess() )
    return Error( "Cannot initialize flat generator" ) ;

  // Get the mass of the particle to be generated
  LHCb::IParticlePropertySvc* ppSvc =
    svc< LHCb::IParticlePropertySvc >( "LHCb::ParticlePropertySvc" , true ) ;

  // check momentum and angles
  if (
       ( ( m_minMom.value()   > m_maxMom.value()   ) ||
         ( m_minTheta.value() > m_maxTheta.value() ) ||
         ( m_minPhi.value()   > m_maxPhi.value()   ) ) )
    return Error( "Incorrect values for momentum, theta or phi!" ) ;

  // setup particle information
  m_masses.clear();

  info() << "Particle type chosen randomly from :";
  PIDs::const_iterator icode ;
  for ( icode = m_pdgCodes.value().begin(); icode != m_pdgCodes.value().end(); ++icode ) {
    const LHCb::ParticleProperty * particle = ppSvc->find( LHCb::ParticleID( *icode ) );
    m_masses.push_back( ( particle->mass() ) ) ;
    m_names.push_back( particle->particle() ) ;
    info() << " " << particle->particle() ;
  }

  info() << endmsg ;
  // printout vertex information
  info() << "Interaction region : ( "
         << m_minxvtx.value() / Gaudi::Units::mm << " mm < x < " << m_maxxvtx.value() / Gaudi::Units::mm << " mm"
         << ", " << m_minyvtx.value() / Gaudi::Units::mm << " mm < y < " << m_maxyvtx.value() / Gaudi::Units::mm << " mm"
         << ", " << " z = " << m_zvtx.value() / Gaudi::Units::mm << " mm"
         << " )" << endmsg ;

  info() << "Momentum range: " << m_minMom.value() / Gaudi::Units::GeV << " GeV <-> "
         << m_maxMom.value() / Gaudi::Units::GeV << " GeV" << endmsg ;
  info() << "Zenith angle range: " << m_minTheta.value() / Gaudi::Units::rad << " rad <-> "
         << m_maxTheta.value() / Gaudi::Units::rad << " rad" << endmsg ;
  info() << "Phi range: " << m_minPhi.value() / Gaudi::Units::rad << " rad <-> "
         << m_maxPhi.value() / Gaudi::Units::rad << " rad" << endmsg ;

  release( ppSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return sc ;
}

//===========================================================================
// Generate the particles
//===========================================================================
void Cosmics::generateParticle( Gaudi::LorentzVector & fourMomentum , 
                                Gaudi::LorentzVector & origin , 
                                int & pdgId ) {
  double px(0.), py(0.), pz(0.) ;
  double verx(0.), very(0.), verz(0.) ;

  //GM
  //get the momentum according to the cosmic spectrum from
  //two different models
  //as a function of P=x (GeV) and theta'=y (in rads)

  double t = 0;
  double p = 0;
  double flux=0;
  double fluxMax=0.0;
  if(m_model.value()==1) fluxMax=0.002;
  else if(m_model.value()==0) fluxMax = 0.000005;
  //take into account integration over pi
  //GM: 29.01.09: the flux defined in muonSpec does not have the
  //factor 2pi.The fluxmax is the maximum of the function
  //in muonSpec
  //fluxMax*=Gaudi::Units::twopi;
  for(int i=0; i<1000000; i++) {
    //theta uniform in radians
    t = ( m_minTheta.value() + m_flatGenerator()*(m_maxTheta.value()-m_minTheta.value()) )  ;
    //translate momentum in GeV
    p = ( m_minMom.value()/Gaudi::Units::GeV + m_flatGenerator()*(m_maxMom.value()-m_minMom.value())/Gaudi::Units::GeV )  ;
    //
    //This is the function I need to use for the generation of the events;
    //* m_model.value()==1 => flux from nucl-ex/0601019, eq (1). take out the
    //             p^3 factor and add the sin(theta)
    //* m_model.value()==0 => flux from D.Reyna (hep-ph 0604145), adding the sin(theta)
    //             factor; flux calculated through "sphere" of cross-section 1cm^2
    if ( m_model.value()==1 ) {
      warning()<<" You chose to use model 1; the use of this model is discouraged. You do it at your own risk "<< endmsg;
      flux = sin(t)*(18.0*(p+5.0)*pow(p+2.7/cos(t),-2.7)) / ((p*cos(t)+145.0)*(p+5.0/cos(t))) ;
    } else if(m_model.value()==0) {
      //depth in Meters Water Equivalent,
      //80 meters * density of the standard rock w.r.t density of water = 1 = 2.65 g/cm^3
      double depth = 80.0*2.65;
      double angle = 0.0;   //flat overburden
      flux = muonSpec(p, t, depth , angle);
    }

    if(flux>fluxMax ) warning() <<"Cosmic flux =" <<flux<<" > Max = "<<fluxMax<<endmsg;
    double temp = m_flatGenerator()*fluxMax ;
    //std::cout << " temp ="<<temp<<std::endl;
    if(temp < flux)    break;
  }

  //translate back into MeV
  p*=Gaudi::Units::GeV;
  //Momentum does not need to be changed.
  const double momentum = p;
  //Phi random between +pi and -pi;
  double phiprime      = m_minPhi.value()   + m_flatGenerator() *
    (m_maxPhi.value()-m_minPhi.value());
  //
  //the angle (t) is the zenith, angle of the particle with the vertical (theta');
  //I want to translate it into theta = angle with the beam in the LHCb system.
  //
  //Now have p, theta' and phi';
  //need ^x,^y,^z=versors in cartesian system;
  //
  verx=sin(t)*cos(phiprime);
  very=-1*cos(t);
  verz=sin(t)*sin(phiprime);
  /*
    expressing ^x,^y,^z as a function of the
    LHCb coordinates phi, theta (assuming
    theta positive from -y axis => vertical track->theta = zero,y=-1,
    and phi positive from x axis)
    verx=sin(theta)*cos(phi)
    very=sin(theta)*sin(phi)
    verz=cos(theta),
    from this I get theta and phi as a function of the versors
  */
  const double theta = acos(verz);
  const double phi   = atan2(very,verx);
  //
  debug() <<" CosmicsGun : "<<endmsg;
  debug() <<" p = "<<momentum/Gaudi::Units::GeV<<" GeV; "<< " Phi = "<< phi<< endmsg;
  debug() <<" Theta = "<<theta*180.0/Gaudi::Units::pi<<" degrees, "<<theta
          <<" rad"<<" ( from t = "<<t*180.0/Gaudi::Units::pi<<" "<<t <<")"<< endmsg;
  // Transform to x,y,z coordinates of the momentum
  const double pt = momentum*sin(theta);
  px              = pt*cos(phi);
  py              = pt*sin(phi);
  pz              = momentum*cos(theta);
  // randomly choose a particle type
  unsigned int currentType =
    (unsigned int)( m_pdgCodes.value().size() * m_flatGenerator() );
  // protect against funnies
  if ( currentType >= m_pdgCodes.value().size() ) currentType = 0;
          
  pdgId = m_pdgCodes.value()[ currentType ] ;
  fourMomentum.SetPx( px ) ;
  fourMomentum.SetPy( py ) ;
  fourMomentum.SetPz( pz ) ; 
  fourMomentum.SetE( std::sqrt( m_masses[ currentType ] * m_masses[ currentType ] + 
                                fourMomentum.P2() ) ) ;        
    
  //
  //Generate random space point anywhere inside the detector
  //(as specified by job options )
  //z=z, -5<x<5m, -5<y<5m
  //
  double xprime = m_minxvtx.value() + m_flatGenerator()*(m_maxxvtx.value() - m_minxvtx.value());
  double yprime = m_minyvtx.value() + m_flatGenerator()*(m_maxyvtx.value() - m_minyvtx.value());
  double zprime = m_zvtx.value() ;

  //GM: Add the time component; this is the time I want the cosmics
  //to be at the point in the detector.
  double tprime = m_time.value();
  //
  //now extrapolate at y=6meters (above the detector), assuming
  //cosmics always going down
  //
  const double y = 6.0 * Gaudi::Units::m ;//m_maxyvtx.value();
  const double x = xprime + (y-yprime)*(verx/very);
  const double z = zprime + (y-yprime)*(verz/very);
  double tofLight = sqrt( (xprime-x)*(xprime-x) +
                          (yprime-y)*(yprime-y) + (zprime-z)*(zprime-z) )/Gaudi::Units::c_light ;
  //get the time at the origin
  const double time = tprime - tofLight;
          
  origin.SetCoordinates( x , y , z , time ) ;
  
  debug() << " -> " << m_names[ currentType ] << endmsg
          << "   P   = " << fourMomentum << endmsg                             
          << "   Vtx = " << origin << endmsg ;
}

double Cosmics::muonSpec(double muMomentum, double Theta, double vDepth,
		double overAngle) {

/*************************************************************************
 This is a function which is based on the surface muon spectrum
 that was developed in hep-ph/0604145.  It will return the rate of muon
 events at a given depth as a function of muon momentum and the
 zenith angle.  Energy loss is calculated based on the geometric
 path length to the surface and an energy loss of 217 MeV / mwe
 which is consistent with "standard rock".  The input and output
 are defined as follows:
  Inputs:
      muMomentum  --  momentum of the detected muon in GeV
      cosTheta    --  cos of the zenith angle (no units)
      vDepth      --  The vertical depth directly over the detector in
                      meters of water equivalent (mwe)
      overAngle   --  Angle from flat (in degrees) of the overburden.
                      this yields a generic cone in which the peak of the
                      overburden is assumed to be directly over the
                      detector with height vDepth and base at detector
                      level
                             0 = a flat overburden
  Output:
      muonSpec    -- muon rate in Hz/cm2/sr/GeV 
      
 D. Reyna  14 November 2006

 Update--Giulia Manca: 29/01/2009: 
         Changed the output to give the rate on a vertical surface.
**************************************************************************/
  double zeta, Eloss, Alpha, ctAlpha;
  double delta = 3.38;
  double result = 0;
  double cosTheta = cos(Theta);

  if (muMomentum < 0) {
    error() << "Error: unphysical muon momentum: "<<muMomentum<<endmsg;
    return(0);
  }

  /* Calculate energy loss for generic overburden */
  Eloss = vDepth;                                //hemisphere
  if ((overAngle >= 0) && (overAngle < 90)) {    //generic cone (includes flat)
    Alpha = (90 - overAngle)*Gaudi::Units::pi/180.;
    ctAlpha = cos(Alpha)/sin(Alpha);
    Eloss /= cosTheta + ctAlpha*sqrt(1-pow(cosTheta,2));
  }
  /* assume 217 MeV loss per meter of water equivalent which is roughly true
     for standard rock */
  Eloss *= 0.217;


  /* calculate rate based on the starting momentum at the surface */
  zeta = cosTheta*(muMomentum + Eloss) + delta;

  /* adding extra factor sin(theta) from d(omega) to d(theta)*/
  /* adding extra factor 2pi from integration */
  /*
  */
  //Old function
  //  result = Gaudi::Units::twopi * sin(Theta) * pow(cosTheta,2.94)*0.14*pow(zeta,-2.7)*
  //          (1/(1 + 1.11*zeta/115) + 1/(1 + 1.11*zeta/850));
  //
  //Flux Through Vertical Surface
  //extra term (sinTheta)^2 cosphi; integral -pi to pi of (cosphi)=4;
  //  result = (1-cosTheta*cosTheta)*pow(cosTheta,2.94)*0.14*pow(zeta,-2.7)*
  //(1./(1. + 1.11*zeta/115.) + 1./(1. + 1.11*zeta/850.));
  result = 4.0 * sin(Theta) * sin(Theta) * pow(cosTheta,2.94)*0.14*pow(zeta,-2.7)*
              (1/(1 + 1.11*zeta/115) + 1/(1 + 1.11*zeta/850));

  return(result);
}


