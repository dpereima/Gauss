/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
* $Id: fortchar.h,v 1.1.1.1 1996/02/15 17:49:18 mclareni Exp $
*
* $Log: fortchar.h,v $
* Revision 1.1.1.1  1996/02/15 17:49:18  mclareni
* Kernlib
*
*
*   for C routines receiving a Fortran Character string
*
* fortchar.inc
*/
#if defined(CERNLIB_QMCRY)
#include <fortran.h>
#endif
