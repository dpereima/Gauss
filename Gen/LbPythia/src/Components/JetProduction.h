/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: JetProduction.h,v 1.2 2007-10-10 14:37:55 gcorti Exp $
#ifndef LBPYTHIA_JETPRODUCTION_H
#define LBPYTHIA_JETPRODUCTION_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Generators/IProductionTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IRndmGenSvc.h"

#include "GaudiKernel/PhysicalConstants.h"
// from Event
#include "Event/GenCollision.h"



// Forward declaration
class IBeamTool ;

/** @class JetProduction JetProduction.h
 *
 *  Tool to produce Jet events with Pythia.
 *  Description :
 *    This code is used to generate jets of particles,
 *      using the Pythia generator.
 *    The functionning is very similar to the PythiaProduction algorithm.
 *    See the option file JetGuns.opts in the Gen/LbPythia package
 *      for more informations.
 *
 *  @author Gueissaz Neal
 *  @date   2005-08-16
 *  @date   2007-10-09 (last modified)
 */

namespace GenMode {
  enum Mode { GaussMode=1, FlatMode };
}

class JetProduction : public extends<GaudiTool, IProductionTool> {

  //class JetProduction : public PythiaProduction {


public:
  typedef std::vector<std::string> CommandVector ;

  /// Standard constructor
  using extends::extends;

  StatusCode initialize( ) override;   ///< Initialize method

  StatusCode finalize( ) override;   ///< Finalize method

  StatusCode generateEvent( HepMC::GenEvent * theEvent ,
                            LHCb::GenCollision * theCollision ) override;

  StatusCode initializeGenerator( ) override;

  void setStable( const LHCb::ParticleProperty * thePP ) override;

  void updateParticleProperties( const LHCb::ParticleProperty * thePP ) override;

  void turnOnFragmentation( ) override;

  void turnOffFragmentation( ) override;

  StatusCode hadronize( HepMC::GenEvent * theEvent ,
                        LHCb::GenCollision * theCollision ) override;

  void savePartonEvent( HepMC::GenEvent * theEvent ) override;

  void retrievePartonEvent( HepMC::GenEvent * theEvent ) override;

  void printRunningConditions( ) override;

  bool isSpecialParticle( const LHCb::ParticleProperty * thePP ) const override;

  StatusCode setupForcedFragmentation( const int thePdgId ) override;

protected:
  /// Parse Pythia commands from a string vector
  StatusCode parsePythiaCommands( const CommandVector & theVector ) ;

  /// Print Pythia parameters
  void printPythiaParameter( ) ;

  /// Retrieve hard process information
  void hardProcessInfo( LHCb::GenCollision * theCollision ) ;

  int m_userProcess{0} ; ///< type of User process

protected:

  void setPygive ( const CommandVector& vct ) { m_pygive.value() = vct ; }
  void addPygive ( const std::string&   item ) { m_pygive.value().push_back ( item ) ; }
  const CommandVector& pygive() const { return m_pygive ; }

  /// PYTHIA -> HEPEVT -> HEPMC conversion
  StatusCode toHepMC
  ( HepMC::GenEvent*     theEvent    ,
    LHCb::GenCollision * theCollision ) ;

protected:
  // Set the default settings for Pythia here:
  CommandVector m_defaultSettings{
    "pysubs msel 0",
    "pysubs msub 11 1",
    "pysubs msub 12 1",
    "pysubs msub 13 1",
    "pysubs msub 28 1",
    "pysubs msub 53 1",
    "pysubs msub 68 1",
    "pysubs msub 91 1",
    "pysubs msub 92 1",
    "pysubs msub 93 1",
    "pysubs msub 94 1",
    "pysubs msub 95 1",
    "pysubs msub 86 1",
    "pysubs msub 87 1",
    "pysubs msub 88 1",
    "pysubs msub 89 1",
    "pysubs msub 106 1",
    "pypars mstp 2 2",
    "pypars mstp 33 3",
    "pypars mstp 128 2",
    "pypars mstp 82 3",
    "pypars mstp 52 2",
    "pypars mstp 51 10042",
    "pypars parp 67 1.0",
    "pypars parp 82 3.41",
    "pypars parp 89 14000",
    "pypars parp 90 0.162",
    "pypars parp 85 0.33",
    "pypars parp 86 0.66",
    "pypars parp 91 1.0",
    "pydat1 parj 13 0.750",
    "pydat1 parj 14 0.162",
    "pydat1 parj 15 0.018",
    "pydat1 parj 16 0.054",
    "pydat1 parj 17 0.090",
    "pydat1 mstj 26 0",
    "pydat1 parj 33 0.4"
  } ;
	Gaudi::Property<CommandVector> m_commandVector{this,"Commands",{},"Commands to setup pythia"} ; ///< Commands to setup pythia

  Gaudi::Property<CommandVector> m_pygive{this,"PygiveCommands",{},"Commands in Pygive format"} ; ///< Commands in "Pygive" format


  // event listing level for "generateEvent"
  int m_eventListingLevel{-1}  ;
  // event listing level for "hadronize"
  int m_eventListingLevel2{-1} ;
  int m_initializationListingLevel{1} ;
  int m_finalizationListingLevel{-1} ;

  std::string m_pythiaListingFileName{""} ;
  int m_pythiaListingUnit{0} ;

  int         m_particleDataUnit{59}   ;
  std::string m_particleDataOutput{""} ;
  std::string m_particleDataInput{""}  ;
  int         m_particleDataLevel{0}  ;

private:

  // MSTU(1)/MSTU(2) for initialization PYLIST
  int m_ini_mstu_1{0} ;
  int m_ini_mstu_2{0} ;
  // MSTU(1)/MSTU(2) for "generateEvent" PYLIST
  int m_eve_mstu_1{0} ;
  int m_eve_mstu_2{0} ;
  // MSTU(1)/MSTU(2) for "hadronize" PYLIST
  int m_had_mstu_1{0} ;
  int m_had_mstu_2{0} ;
  // list of particles to be printed
  Gaudi::Property<std::vector<int>> m_pdtlist{this,"PDTList",{},"list of particles to be printed"} ;
  int m_nEvents{0} ; ///< Internal event counter

  Gaudi::Property<int> m_njets{this,"NJets",1,"NJets"};
  int m_index{-1};

  Gaudi::Property<int> m_particlesmin{this,"ParticlesMin", 5,"ParticlesMin"}; 
  Gaudi::Property<int> m_particlesmax{this,"ParticlesMax", 5,"ParticlesMax"};
  Gaudi::Property<int> m_particlesmean{this,"ParticlesMean", 5,"ParticlesMean"}; 
  Gaudi::Property<double> m_particlessigma{this,"ParticlesSigma", 0.01,"ParticlesSigma"};
  Gaudi::Property<std::vector<int>> m_particles{this,"Particles",{},"Particles"};
  Gaudi::Property<int> m_particlesgenmode{this,"ParticlesMode", GenMode::FlatMode,"ParticlesMode"}; 
  int m_particlesdiff{0};

  Gaudi::Property<double> m_energymin{this,"EnergyMin", 10.*Gaudi::Units::GeV,"EnergyMin"}; 
  Gaudi::Property<double> m_energymax{this,"EnergyMax", 50.*Gaudi::Units::GeV,"EnergyMax"};
  Gaudi::Property<double> m_energymean{this,"EnergyMean", 20.*Gaudi::Units::GeV,"EnergyMean"}; 
  Gaudi::Property<double> m_energysigma{this,"EnergySigma", 5.*Gaudi::Units::GeV,"EnergySigma"};
  Gaudi::Property<std::vector<double>> m_energy{this,"Energy",{},"Energy"};
  Gaudi::Property<int> m_energygenmode{this,"EnergyMode", GenMode::FlatMode,"EnergyMode"}; 
  int m_energydiff{0};

  Gaudi::Property<double> m_thetamin{this,"ThetaMin", 0.015 ,"ThetaMin"};
  Gaudi::Property<double> m_thetamax{this,"ThetaMax", 0.33 ,"ThetaMax"};
  Gaudi::Property<double> m_thetamean{this,"ThetaMean", 0.1575 ,"ThetaMean"};  
  Gaudi::Property<double> m_thetasigma{this,"ThetaSigma", 0.1 ,"ThetaSigma"};
  Gaudi::Property<std::vector<double>> m_theta{this,"Theta",{},"Theta"};
  Gaudi::Property<int> m_thetagenmode{this,"ThetaMode", GenMode::GaussMode ,"ThetaMode"}; 
  int m_thetadiff{0};

  Gaudi::Property<double> m_phimin{this,"PhiMin",0.0 ,"PhiMin"};
  Gaudi::Property<double> m_phimax{this,"PhiMax",6.283185 ,"PhiMax"};
  Gaudi::Property<double> m_phimean{this,"PhiMean",3.1415 ,"PhiMean"};  
  Gaudi::Property<double> m_phisigma{this,"PhiSigma", 1. ,"PhiSigma"};
  Gaudi::Property<std::vector<double>> m_phi{this,"Phi",{} ,"Phi"};
  Gaudi::Property<int> m_phigenmode{this,"PhiMode",GenMode::FlatMode ,"PhiMode"}; 
  int m_phidiff{0};

  double generateValue( const int mode, const double mean,
                        const double sigma, const double min,
                        const double max );

  //Define the method and the Flat random number generator
  mutable IRndmGenSvc* m_RGS{0};
  IRndmGenSvc* randSvc() const;
  Rndm::Numbers m_flatGenerator;
  Rndm::Numbers m_gaussGenerator;

};
#endif // LBPYTHIA_JETPRODUCTION_H
