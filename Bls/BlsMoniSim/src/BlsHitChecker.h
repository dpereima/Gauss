/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BlsHitChecker.h,v 1.1.1.2 2010-03-10 17:38:47 vtalanov Exp $
#ifndef BLSHITCHECKER_H
#define BLSHITCHECKER_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Event/MCHit.h"

// from STL
#include <map>

/** @class BlsHitChecker BlsHitChecker.h
 *
 *
 *  @author Vadim Talanov
 *  @date   2010-02-06
 */

class BlsHitChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;


  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:

  LHCb::MCHits* m_blsMCHits={};
  Gaudi::Property<std::string> m_blsHitsLocation{this,"HitsLocation","MC/Bls/Hits","HitsLocation"};
  Gaudi::Property<bool> m_blsAOn{this,"BlsAOn",false,"BlsAOn"};
  Gaudi::Property<bool> m_blsCOn{this,"BlsCOn",false,"BlsCOn"};
  Gaudi::Property<std::string> m_blsHTitlePrefix{this, "HistogramTitlePrefix","BLS: ","HistogramTitlePrefix"};

  Gaudi::Property<double> m_blsHEntryXMin{this, "EntryXMin", -150.0 * Gaudi::Units::mm,"EntryXMin"};
  Gaudi::Property<double> m_blsHEntryXMax{this, "EntryXMax", +150.0 * Gaudi::Units::mm,"EntryXMax"};
  Gaudi::Property<unsigned long> m_blsHEntryXNbins{this, "EntryXNbins",300,"EntryXNbins"};
  Gaudi::Property<double> m_blsHEntryYMin{this, "EntryYMin",-150.0 * Gaudi::Units::mm,"EntryYMin"};
  Gaudi::Property<double> m_blsHEntryYMax{this, "EntryYMax",+150.0 * Gaudi::Units::mm,"EntryYMax"};
  Gaudi::Property<unsigned long> m_blsHEntryYNbins{this, "EntryYNbins",300,"EntryYNbins"};
  Gaudi::Property<double> m_blsHEntryZMin{this, "EntryZMin",-2200.0 * Gaudi::Units::mm,"EntryZMin"};
  Gaudi::Property<double> m_blsHEntryZMax{this, "EntryZMax",-1900.0 * Gaudi::Units::mm,"EntryZMax"};
  Gaudi::Property<unsigned long> m_blsHEntryZNbins{this, "EntryZNbins",300,"EntryZNbins"};
  Gaudi::Property<double> m_blsHEntryTimeOffset{this, "EntryTimeOffset",0.0 * Gaudi::Units::ns,"EntryTimeOffset"};
  Gaudi::Property<double> m_blsHEntryTimeMin{this, "EntryTimeMin", -50.0 * Gaudi::Units::ns,"EntryTimeMin"};
  Gaudi::Property<double> m_blsHEntryTimeMax{this, "EntryTimeMax", +50.0 * Gaudi::Units::ns,"EntryTimeMax"};
  Gaudi::Property<unsigned long> m_blsHEntryTimeNbins{this, "EntryTimeNbins",100,"EntryTimeNbins"};
  Gaudi::Property<double> m_blsHTrackEnDepMin{this, "TrackEnDepMin",0.0 ,"TrackEnDepMin"};
  Gaudi::Property<double> m_blsHTrackEnDepMax{this, "TrackEnDepMax",50.0,"TrackEnDepMax"};
  Gaudi::Property<unsigned long> m_blsHTrackEnDepNbins{this,"TrackEnDepNbins",500,"TrackEnDepNbins"};
  Gaudi::Property<double> m_blsHTrackLengthMin{this, "TrackLengthMin",0.0,"TrackLengthMin"};
  Gaudi::Property<double> m_blsHTrackLengthMax{this, "TrackLengthMax",7.0,"TrackLengthMax"};
  Gaudi::Property<unsigned long> m_blsHTrackLengthNbins{this, "TrackLengthNbins",70,"TrackLengthNbins"};
  Gaudi::Property<double> m_blsHEventNumTracksMin{this, "EventNumTracksMin",0.0,"EventNumTracksMin"};
  Gaudi::Property<double> m_blsHEventNumTracksMax{this, "EventNumTracksMax",50.0,"EventNumTracksMax"};
  Gaudi::Property<unsigned long> m_blsHEventNumTracksNbins{this, "EventNumTracksNbins",50,"EventNumTracksNbins"};
  Gaudi::Property<double> m_blsHEventNumMin{this, "EventNumMin",0.0,"EventNumMin"};
  Gaudi::Property<double> m_blsHEventNumMax{this, "EventNumMax",1000.0,"EventNumMax"};
  Gaudi::Property<unsigned long> m_blsHEventNumNbins{this, "EventNumNbins",1000,"1000"};

  unsigned long m_blsHits{0};
  unsigned long m_blsTracks{0};

   typedef std::multimap < const LHCb::MCParticle*,
                            LHCb::MCHit* >
                            t_blsMCParticle2MCHitMultimap;

};
#endif // BLSHITCHECKER_H
