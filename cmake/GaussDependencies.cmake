###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      include(${LHCbFindPackage_FILE})
  else()
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()

# -- Public dependencies
lhcb_find_package(Run2Support 2.2.2 REQUIRED)
lhcb_find_package(Geant4 10.6.2.6 REQUIRED)
# Geant4Config.cmake does not set GEANT4_PROJECT_ROOT (needed for environment relocation)
if(NOT GEANT4_PROJECT_ROOT AND Geant4_DIR MATCHES "^(.*)/InstallArea/")
    set(GEANT4_PROJECT_ROOT "${CMAKE_MATCH_1}")
endif()

find_data_package(AppConfig 3.0 REQUIRED)
find_data_package(BcVegPyData 4.0 REQUIRED)
find_data_package(LamarrData 3.0 REQUIRED)
find_data_package(Det/GDMLData 1.0 REQUIRED)
find_data_package(FieldMap 5.0 REQUIRED)
find_data_package(GenXiccData 3.0 REQUIRED)
find_data_package(Gen/PGunsData 2.0 REQUIRED)
find_data_package(Gen/MadgraphData 20903.7.0 REQUIRED)
find_data_package(Gen/DecFiles 32.0 REQUIRED)
find_data_package(LHAPDFSets 62.3 REQUIRED)
find_data_package(MIBData 3.0 REQUIRED)
find_data_package(ParamFiles 8.0 REQUIRED)
find_data_package(Vis/XmlVis 2.0 REQUIRED)
find_data_package(FastCaloSimData 1.0 REQUIRED)

# Custom vars from *.xml files of the data packages
lhcb_env(SET LHAPDF_DATA_PATH ${LHAPDFSets_ROOT_DIR}/data)

find_package(ROOT REQUIRED EG Gpad Graf HistPainter MathCore MathMore Matrix Minuit Physics RIO Tree TreePlayer)
find_package(HepMC REQUIRED HepMC fio)
find_package(LHAPDF REQUIRED)
find_package(Pythia6 REQUIRED pythia6 pythia6_dummy)
find_package(Pythia8 REQUIRED pythia8)
find_package(FastJet REQUIRED)
find_package(Rivet REQUIRED)
find_package(YODA REQUIRED)
find_package(STARlight REQUIRED)
find_package(CRMC REQUIRED)
find_package(ThePEG REQUIRED)
find_package(Herwig++ REQUIRED)
find_package(Madgraph REQUIRED)
find_package(Tauola++ REQUIRED Fortran CxxInterface)
find_package(Photos++ REQUIRED Fortran CxxInterface)

# -- Private dependencies
if(WITH_Gauss_PRIVATE_DEPENDENCIES)
  if(BUILD_TESTING)
    find_package(Boost REQUIRED unit_test_framework)
  endif()
endif()
