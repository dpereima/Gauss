/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef    GIGA_GIGADETECTORELEMENT_H
#define    GIGA_GIGADETECTORELEMENT_H 1

// Include files
// from GiGaCnv
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"

/** @class GiGaDetectorElementCnv GiGaDetectorElementCnv.h 
 *
 *  converter of DetectorElements into Geant4 Geometry tree
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */

class GiGaDetectorElementCnv: public GiGaCnvBase
{
public:

  /** create the representation
   *  @param Object  pointer to data object
   *  @param Address address
   *  @return status code
   */
  StatusCode createRep
  ( DataObject*     Object  ,
    IOpaqueAddress*& Address ) override;

  /** update the  representation
   *  @param Address address
   *  @param Object  pointer to data object
   *  @return status code
   */
  StatusCode updateRep
  ( IOpaqueAddress*  Address,
    DataObject*      Object   ) override;

  /// class ID for converted objects
  static const CLID&         classID();

  /// storage Type
  static unsigned char storageType() ;
  ///

  /** standard constructor 
   *  @param svc pointer to Service Locator 
   */
  GiGaDetectorElementCnv( ISvcLocator* svc );

  /// virtual destructor
  virtual ~GiGaDetectorElementCnv();

private:

  GiGaLeaf  m_leaf;

};


#endif  // GIGA_GIGADETECTORELEMENT_H
// ============================================================================
