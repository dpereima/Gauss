###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6.py')
importOptions('$GAUSSOPTS/BeamGasWithBeamBeam.py')
importOptions('$GAUSSOPTS/Gauss-Upgrade-Baseline.py')
importOptions('$GAUSSOPTS/DBTags-2022.py')
importOptions('$GAUSSOPTS/Smog2TriangularProfile.py')

from Configurables import Gauss
from GaudiKernel import SystemOfUnits
Gauss().FixedTargetParticle = 'Ar'
Gauss().FixedTargetLuminosity = 7*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 620.*SystemOfUnits.millibarn
