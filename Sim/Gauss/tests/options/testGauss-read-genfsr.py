###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Configuration import *
from Configurables import GaudiSequencer

seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += ["GenFSRRead", "GenFSRLog"]

ApplicationMgr().TopAlg += [seqGenFSR]

from Configurables import LHCbApp
LHCbApp().EvtMax = -1
LHCbApp().Simulation = True
LHCbApp().DataType = "2012"

EventSelector().Input += ["DATAFILE='PFN:genFSR_2012_Gauss_created.xgen' TYP='POOL_ROOTTREE' OPT='READ'"]

