###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
from Gauss.Configuration import *

importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-mu100-2018-nu1.6.py")
importOptions('$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py')
importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")
importOptions('$GAUSSOPTS/DBTags-2016.py')
importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")
importOptions("$DECFILESROOT/options/10005000.py")
importOptions('$LBPYTHIA8ROOT/options/Pythia8.py')
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")
importOptions('$APPCONFIGOPTS/Gauss/ReDecay-100times.py')

from Configurables import LHCbApp
LHCbApp().EvtMax = 5
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 1
