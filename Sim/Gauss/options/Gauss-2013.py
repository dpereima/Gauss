###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running Gauss with Sim10 configuration and beam conditions as in
# production for 2013 data (1.38 TeV beams, nu=0.36, no spill-over)
#
# Syntax is:
#   gaudirun.py Gauss-2013.py <someInputJobConfiguration>.py
##############################################################################

#--Pick beam conditions as set in AppConfig
from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam1380GeV-md100-2013-nu0.36.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2013.py")
importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")
importOptions("$APPCONFIGOPTS/Gauss/NoPacking.py")

#--Set database tags
importOptions("$GAUSSOPTS/DBTags-2013.py")
