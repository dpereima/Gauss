###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##
##  File containing options to lower productions cuts
##

from Gaudi.Configuration import *
from GaudiKernel import SystemOfUnits

def setProductionCuts():
    from Configurables import ( GiGa, GiGaPhysListModular )
    giga = GiGa()
    giga.addTool( GiGaPhysListModular("ModularPL") , name="ModularPL" ) 

    giga.ModularPL.CutForElectron = 0.01 * SystemOfUnits.mm
    giga.ModularPL.CutForPositron = 0.01 * SystemOfUnits.mm
    giga.ModularPL.CutForGamma    = 0.01 * SystemOfUnits.mm


appendPostConfigAction(setProductionCuts)

