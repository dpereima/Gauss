###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from itertools import islice
from GaudiTesting.BaseTest import (normalizeExamples, normalizeEOL,
                                   RegexpReplacer, LineSkipper, FilePreprocessor)


def replacenumbers(m):
    res = m.groups()[0]
    for i in m.groups()[1].lstrip().split():
        res = res + " {0:f}".format(float(i))
    return res

class FastCaloFilter(FilePreprocessor):
  def __processFile__(self, lines):
    result = []
    for ll in lines:
      if ('******Stat******' in ll) or (ll[:2]=='= ') or ('Fast' in ll) or \
         ('ApplicationMgr' in ll):
        result.append(ll)
    return result
   

preprocessor = (
    normalizeExamples + normalizeEOL +  # workaround for gaudi/Gaudi#108
    LineSkipper(
        [
            "SUCCESS Number of counters : ",
            "INFO Number of counters : ",
            "SUCCESS 1D histograms in directory",
            "SUCCESS 1D profile histograms in directory",
            "TimingAuditor.TIMER",
            "ToolSvc.SequencerTime...",
            "INFO Disconnect from database after being idle",
            "G4Transportation constructor> set fShortStepOptimisation to false",
            "PDFset name",
            "a\x02",  # LHAPDF seems to print \x02 (^B)
        ],
        regexps=[
            r"MainEventGaussSim\s*DEBUG event 1 memory.*",
            r"GaussGen\s*DEBUG event 1 memory.*",
        ],
    ) + RegexpReplacer(
        when="Connected to database",
        orig=r"(\"sqlite_file:)(.*/)([\w.]+/[0-9A-Z_]{1,8}\")",
        repl=r"\1\3",
    ) + RegexpReplacer(
        when="RFileCnv                   INFO opening Root file",
        orig=r"(opening Root file \")(.*/)([\w]+/[a-z.]{1,14}\")",
        repl=r"\1\3",
    ) + RegexpReplacer(
        when="HistogramDataSvc           INFO Added stream file",
        orig=r"(Added stream file:)(.*/)([\w.]+/[a-z]{1,10})(.root)",
        repl=r"\1\3",
    ) + RegexpReplacer(
        when="MagneticFieldSvc           INFO Opened magnetic field file",
        orig=
        r"(Opened magnetic field file : )(.*/)([\w.]+/[0-9a-z_]{1,14})(.cdf)",
        repl=r"\1\3\4",
    ) + RegexpReplacer(
        when="  KORB:",
        orig=
        r"(  KORB: [a-zA-Z0-9*]+\s*=|  KORB: [A-Z0-9- ]+:)([0-9. \t-E]+)\n",
        repl=replacenumbers,
    ) + RegexpReplacer(
        when="Loading simulation attributes",
        orig=r"(\")(.*/)([\w.]+\")",
        repl=r"\1.../\3",
    ) + RegexpReplacer(
        when='LHAPDF',
        orig=r'(LHAPDF .* loading ).*(/LHAPDFSets/.*)',
        repl=r'\1...\2',
    ) + RegexpReplacer(
        when="READ FSR",
        orig=r'(DecFiles version:.*v)(\d+)r\d*',
        repl=lambda m: m.group(1) + (m.group(2) if m.group(2) != "999" else "32"),
    ))


preprocessor_epos = (preprocessor + RegexpReplacer(
    when=" Geant4 version Name",
    orig=r"(Geant4 version Name)(.*)(\(.*\))",
    repl=r"\1",
) + RegexpReplacer(
    when="read from",
    orig=r"(read from )(.*)( \.\.\.)",
    repl=r"\1\3",
) + RegexpReplacer(
    when="Generation.MinimumBia...SUCCESS LbCRMC : Created temporary file",
    orig=r"(file \/tmp\/lbcrmc\.)(.{6})( for generator)",
    repl=r"\1\3",
) + RegexpReplacer(
    when="Generation.MinimumBia...SUCCESS  Configuration file",
    orig=r"(file: \/tmp\/lbcrmc\.)(.{6})",
    repl=r"\1",
) + RegexpReplacer(
    when="Generation.MinimumBia...SUCCESS LbCRMC : Cleanup:",
    orig=r"(file \/tmp\/lbcrmc\.)(.{6})",
    repl=r"\1",
) + RegexpReplacer(
    when="BeamGasGeneration.Min...SUCCESS LbCRMC : Created temporary file",
    orig=r"(file \/tmp\/lbcrmc\.)(.{6})( for generator)",
    repl=r"\1\3",
) + RegexpReplacer(
    when="BeamGasGeneration.Min...SUCCESS  Configuration file",
    orig=r"(file: \/tmp\/lbcrmc\.)(.{6})",
    repl=r"\1",
))

preprocessor_fastcalo = (
    normalizeExamples + normalizeEOL + 
    LineSkipper(
        [
            "TimingAuditor.TIMER",
        ]
    ) +
    FastCaloFilter())

