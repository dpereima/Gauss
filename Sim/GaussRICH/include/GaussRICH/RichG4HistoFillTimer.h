/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichG4HistoFillTimer.h,v 1.1 2003-04-29 17:08:05 seaso Exp $
#ifndef GAUSSRICH_RICHG4HISTOFILLTIMER_H 
#define GAUSSRICH_RICHG4HISTOFILLTIMER_H 1

// Include files
#include "G4Timer.hh"

/** @class RichG4HistoFillTimer 
 * RichG4HistoFillTimer.h 
 * RichAnalysis/RichG4HistoFillTimer.h
 *  
 *
 *  @author Sajan EASO
 *  @date   2003-04-23
 */
class IHistogramSvc;

class RichG4HistoFillTimer {
public:
  /// Standard constructor
  RichG4HistoFillTimer( ); 

  virtual ~RichG4HistoFillTimer( ); ///< Destructor

  G4Timer* timerRichEvent() 
  {
    return m_timerRichEvent;
  }

  
  void RichG4BeginEventTimer();
  

  void RichG4EndEventTimer();

  
  
protected:

private:

  G4Timer* m_timerRichEvent;

  IHistogramSvc* m_currentHistoSvc;
  
};
#endif // GAUSSRICH_RICHG4HISTOFILLTIMER_H
