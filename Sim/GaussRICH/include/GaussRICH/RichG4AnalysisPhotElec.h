/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichG4AnalysisPhotElec.h,v 1.3 2009-07-03 11:59:49 seaso Exp $
#ifndef GAUSSRICH_RICHG4ANALYSISPHOTELEC_H
#define GAUSSRICH_RICHG4ANALYSISPHOTELEC_H 1

// Include files
#include "globals.hh"
#include "G4Step.hh"


//extern void RichG4AnalysisPhotElecA ( const G4Step& aStep,
//                                      G4int currentRichDetNumber,
//                                      G4int currentHpdNumber, G4double PhotonEnergy);

//extern void RichG4AnalysisPhotElecB ( const G4Step& aStep,
//                                      G4int currentRichDetNumber,
//                                      G4int currentHpdNumber, G4double PhotonEnergy);

extern void RichG4AnalysisPhotElecA ( const G4Step& aStep,
                                      G4int currentRichDetNumber);

extern void RichG4AnalysisPhotElecB ( const G4Step& aStep,
                                      G4int currentRichDetNumber);

#endif // GAUSSRICH_RICHG4ANALYSISPHOTELEC_H
