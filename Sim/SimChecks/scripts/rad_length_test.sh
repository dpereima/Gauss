#!/usr/bin/env sh
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
echo "Running RadLength Test Part 1/2"
python $SIMCHECKSROOT/scripts/rad_length_scan.py --zlim #Option set for limiting Z in profiles
echo "Running RadLength Test Part 2/2"
python $SIMCHECKSROOT/scripts/rad_length_scan_velo_z.py
