###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Sim/GaussTools
--------------
#]=======================================================================]

gaudi_add_library(GaussToolsLib
    SOURCES
        src/Lib/GaussTrackActionBase.cpp
        src/Lib/GaussTrackActionZ.cpp
        src/Lib/GaussTrackInformation.cpp
        src/Lib/GaussTrajectory.cpp
        src/Lib/MCTruthManager.cpp
        src/Lib/OscillationFlag.cpp
        src/Lib/ZMaxPlane.cpp
        src/Lib/createGaussTrajectory.cpp
    LINK
        PUBLIC
            Gauss::GiGaCnvLib
)

gaudi_add_library(GaussToolsGenConfHelperLib
    SOURCES
        src/genConf/genConf_forG4UAction.cpp
    LINK
        PUBLIC
            Geant4::G4run
            Geant4::G4intercoms
)

gaudi_add_module(GaussTools
    SOURCES
        src/Components/CommandTrackAction.cpp
        src/Components/CutsStepAction.cpp
        src/Components/GaussEventActionHepMC.cpp
        src/Components/GaussG4UserLimits.cpp
        src/Components/GaussPostTrackAction.cpp
        src/Components/GaussPreTrackAction.cpp
        src/Components/GaussStepAction.cpp
        src/Components/GaussTargetMultiplicity.cpp
        src/Components/GaussTrackActionByEnergy.cpp
        src/Components/GaussTrackActionByEnergyProcess.cpp
        src/Components/GaussTrackActionByEnergyType.cpp
        src/Components/GaussTrackActionByProcess.cpp
        src/Components/GaussTrackActionByType.cpp
        src/Components/GaussTrackActionFinal.cpp
        src/Components/GaussTrackActionGeneral.cpp
        src/Components/GaussTrackActionHepMC.cpp
        src/Components/GaussTrackActionStart.cpp
        src/Components/GiGaEventActionCommand.cpp
        src/Components/GiGaEventActionDraw.cpp
        src/Components/GiGaEventActionSequence.cpp
        src/Components/GiGaFieldMgr.cpp
        src/Components/GiGaInputStream.cpp
        src/Components/GiGaMagFieldGlobal.cpp
        src/Components/GiGaMagFieldSpecial.cpp
        src/Components/GiGaMagFieldUniform.cpp
        src/Components/GiGaOutputStream.cpp
        src/Components/GiGaPhysListGeantino.cpp
        src/Components/GiGaPhysListModular.cpp
        src/Components/GiGaRegionTool.cpp
        src/Components/GiGaRegionsTool.cpp
        src/Components/GiGaRunActionCommand.cpp
        src/Components/GiGaRunActionSequence.cpp
        src/Components/GiGaSensDetBudget.cpp
        src/Components/GiGaSensDetSequence.cpp
        src/Components/GiGaSetSimAttributes.cpp
        src/Components/GiGaStepActionDraw.cpp
        src/Components/GiGaStepActionSequence.cpp
        src/Components/GiGaStream.cpp
        src/Components/GiGaTrackActionSequence.cpp
        src/Components/KillAtOriginCut.cpp
        src/Components/LoopCuts.cpp
        src/Components/MinEkineCuts.cpp
        src/Components/PingPongCut.cpp
        src/Components/RadLengthColl.cpp
        src/Components/SpecialCuts.cpp
        src/Components/TrCutsRunAction.cpp
        src/Components/WorldCuts.cpp
        src/Components/ZeroFieldMgr.cpp
        src/Components/ZeroStepsCut.cpp
    LINK
        Gauss::GaussToolsLib
	Gauss::SimSvcLib
        Geant4::G4run
        Geant4::G4intercoms
    GENCONF_OPTIONS
        --load-library=GaussToolsGenConfHelperLib
)
add_dependencies(GaussTools GaussToolsGenConfHelperLib)

gaudi_add_executable(test_zMaxTilt
    SOURCES
        tests/src/test_zMaxTilt.cpp
    LINK
        Gauss::GaussToolsLib
        Boost::unit_test_framework
    TEST
)
