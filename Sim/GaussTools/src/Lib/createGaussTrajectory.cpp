/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: createGaussTrajectory.cpp,v 1.1 2004-02-20 19:35:30 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $ 
// ============================================================================
// Include files
// ============================================================================
// G4
// ============================================================================
#include "G4Track.hh"
// ============================================================================
// GaussTools
// ============================================================================
#include "GaussTools/GaussTrajectory.h"
#include "GaussTools/createGaussTrajectory.h"
#include "GaussTools/OscillationFlag.h"
// ============================================================================

/** @file 
 *  Implementation file for function createGaussTrajectory
 *  
 *  @date 2004-02-20 
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 */

// ============================================================================
/** @fn createGaussTrajectory 
 *  
 *  create the GaussTrajectory from G4Track , 
 *   set the oscillation flag 
 *
 *  @param track track 
 *  @return the valid trajectory 
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2004-02-19
 */
// ============================================================================
GaussTrajectory* createGaussTrajectory ( const G4Track* track ) 
{
  if ( 0 == track ) { return 0 ; }
  GaussTrajectory* trajectory = new GaussTrajectory ( track ) ;
  // set oscillation flag 
  OscillationFlag( track , trajectory ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return trajectory ;
}


// ============================================================================
// The END 
// ============================================================================
