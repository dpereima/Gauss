/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaEventActionDraw.h,v 1.2 2007-01-12 15:36:45 ranjard Exp $
#ifndef       GIGA_GiGaEventActionDraw_H
#define       GIGA_GiGaEventActionDraw_H 1
// include files
// GiGa
#include "GiGa/GiGaEventActionBase.h"
// forward declaration
//template <class TYPE> class GiGaFactory;

/** @class GiGaEventActionDraw GiGaEventActionDraw.h
 *
 *  Example of "primitive" implementation of Event Action class
 *  It is just Draw!
 *
 *  @author  Vanya Belyaev
 *  @date    17/03/2001
 */

class GiGaEventActionDraw: public GiGaEventActionBase
{
  /// frined factory for instantiatio
  //friend class GiGaFactory<GiGaEventActionDraw>;

  //protected:
public:

  /** standard constructor
   *  @see GiGaEventActionBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  GiGaEventActionDraw
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;

  /// desctrustor (virtual and protected)
  virtual ~GiGaEventActionDraw();

public:

  /// G4
  void EndOfEventAction   ( const G4Event* event ) override;

private:

  GiGaEventActionDraw(); ///< no default constructor
  GiGaEventActionDraw( const GiGaEventActionDraw& ); ///< no copy
  GiGaEventActionDraw& operator=( const GiGaEventActionDraw& ) ; ///< no =

private:

};
// ============================================================================

// ============================================================================
// The END
// ============================================================================
#endif  //    GIGA_GiGaEventActionDraw_H
// ============================================================================
