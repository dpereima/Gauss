/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef       GaussTools_CutsStepAction_H
#define       GaussTools_CutsStepAction_H 1
// ============================================================================
// include files
// GiGa
#include "GiGa/GiGaStepActionBase.h"
// forward declarations
//template <class TYPE> class GiGaFactory;

/** @class CutsStepAction CutsStepAction.h
 *
 *  @author  Witek Pokorski
 *  @date    1/11/2002
 */

class CutsStepAction: virtual public GiGaStepActionBase
{
  /// friend factory for instantiation
  //  friend class GiGaFactory<CutsStepAction>;

  //protected:
public:

  /** standard constructor
   *  @see GiGaStepActionBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  CutsStepAction
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;

  /// destructor (virtual and protected)
  virtual ~CutsStepAction();

public:

  /** stepping action
   *  @see G4UserSteppingAction
   *  @param step Geant4 step
   */
  void UserSteppingAction ( const G4Step* ) override;

private:
  CutsStepAction(); ///< no default constructor
  CutsStepAction( const CutsStepAction& ); ///< no copy
  CutsStepAction& operator=( const CutsStepAction& ) ; ///< no =

  double m_trcuteg;
  double m_trcuthadr;
};
// ============================================================================

// ============================================================================
// The END
// ============================================================================
#endif  ///<    GaussTools_CutsStepAction_H
// ============================================================================
