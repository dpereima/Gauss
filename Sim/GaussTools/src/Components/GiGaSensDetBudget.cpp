/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaSensDetBudget.cpp,v 1.4 2008-07-26 15:43:52 robbep Exp $
// Include files

// from Gaudi
#include "GaudiKernel/MsgStream.h" 
// GiGa
//#include "GiGa/GiGaMACROs.h"
// local
#include "GiGaSensDetBudget.h"
// G4 
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"

// ============================================================================
/** @file
 *  
 *  Implementation file for class GiGaSensDetBudget.
 *  Sensitive Detector, which is able to calculate the material budget 
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   04/05/2002
 */
// ============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( GiGaSensDetBudget )


// ============================================================================
/** standard constructor 
 *  @see GiGaSensDetBase 
 *  @see GiGaBase 
 *  @see AlgTool 
 *  @param type type of the object (?)
 *  @param name name of the object
 *  @param parent  pointer to parent object
 */
// ============================================================================
GiGaSensDetBudget::GiGaSensDetBudget
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent ) 
  : G4VSensitiveDetector ( name  )
  , GiGaSensDetBase      ( type , name , parent ) 
  , m_budgetL            (       )
  , m_budgetP            (       )
  , m_budgetM            (       )
  , m_lvolume            ( true  ) // accumulate by logical  volume name 
  , m_pvolume            ( true  ) // accumulate by physical volume name 
  , m_material           ( true  ) // accumulate by material name 
{
  declareProperty( "Logical"  , m_lvolume  );
  declareProperty( "Physical" , m_pvolume  );
  declareProperty( "Material" , m_material );
}



// ============================================================================
// destructor 
// ============================================================================
GiGaSensDetBudget::~GiGaSensDetBudget() {} 


// ============================================================================
/** process the hit
 *  @param step     pointer to current Geant4 step 
 *  @param history  pointer to touchable history 
 */
// ============================================================================
bool GiGaSensDetBudget::ProcessHits
( G4Step*                step       , 
  G4TouchableHistory* /* history */ ) 
{
  // check argument
  if( 0 == step ) { Error(" G4Step* points to  NULL!"    ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); return false ; }
  
  G4TouchableHistory* TT =  
    (G4TouchableHistory*)( step -> GetPreStepPoint () -> GetTouchable () );
  if( 0 == TT   ) { Error(" TouchableHistory* is NULL!"  ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); return false ; }
  const G4VPhysicalVolume*  PV =   TT->GetVolume();
  if( 0 == PV   ) { Error(" G4VPhysicalVolume* is NULL!" ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); return false ; }
  const G4LogicalVolume*    LV =   PV->GetLogicalVolume();
  if( 0 == PV   ) { Error(" G4LogicalVolume*   is NULL!" ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); return false ; }
  const G4Material*         MT = LV->GetMaterial() ;
  if( 0 == MT   ) { Error(" G4Material*        is NULL!" ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); return false ; }
  
  // step length 
  const double length = 
    ( step->GetPostStepPoint()->GetPosition() - 
      step->GetPreStepPoint ()->GetPosition() ).r();
  
  // radiation length of material  
  const double radlen = MT->GetRadlen();
  
  // accumulate material budget 
  if( m_lvolume  ) { m_budgetL[ LV->GetName() ] += length / radlen ; }
  if( m_pvolume  ) { m_budgetP[ PV->GetName() ] += length / radlen ; }
  if( m_material ) { m_budgetM[ MT->GetName() ] += length / radlen ; }
  
  return false ;
  
}
// ============================================================================


namespace Local
{  
  inline std::string print( const double value            , 
                            const char*  format = "%9.3g" )
  {
    static char buf[64];
    return std::string( buf , buf + sprintf( buf , format , value ) ) ;
  }
}


// ============================================================================
/** finalize the sensitive detector 
 *  @see GiGaSensDetBase 
 *  @see GiGaBase 
 *  @return status code 
 */
// ============================================================================
StatusCode GiGaSensDetBudget::finalize   () 
{
  // printout 
  MsgStream log( msgSvc() , name() ) ;
  const std::string stars( 80 , '*' );
  log << MSG::INFO   << stars                                << endmsg ;
  log << MSG::ALWAYS << " Sorted Material budget listing "             ;
  if( m_material ) { log << " Materials " ; }
  if( m_lvolume  ) { log << " LogVols   " ; }
  if( m_pvolume  ) { log << " PhysVols  " ; }
  log << endmsg ;
  typedef std::pair<double,const std::string*> Entry   ;
  typedef std::vector<Entry>                   Entries ;
  if( m_material )
    {
      log << MSG::INFO   << stars                                << endmsg ;
      Entries tmp  ;      
      double total = 0 ;
      for( Budget::const_iterator it1 = m_budgetM.begin() ; 
           it1 != m_budgetM.end() ; ++it1 )
        {
          total += it1 -> second ;
          tmp.push_back( Entry( it1->second , &(it1->first) ) );
        }
      std::stable_sort( tmp.begin() , tmp.end() );
      for( Entries::reverse_iterator ri = tmp.rbegin() ;
           tmp.rend() != ri ; ++ri )
        {
          log << MSG::INFO 
              << " Budget: "       << Local::print( ri->first )
              << " Material   : '" <<            *(ri->second)  
              << "'" << endmsg ;
        }
      log << MSG::ALWAYS << " Total Evaluated Budget ( Materials   ) = " ;
      log << Local::print( total ) << endmsg  ;
      tmp.clear();
    }
  if( m_lvolume )
    {    
      log << MSG::INFO   << stars                                << endmsg ;
      Entries tmp  ;      
      double total = 0 ;
      for( Budget::const_iterator it1 = m_budgetL.begin() ; 
           it1 != m_budgetL.end() ; ++it1 )
        {
          total += it1 -> second ;
          tmp.push_back( Entry( it1->second , &(it1->first) ) );
        }
      std::stable_sort( tmp.begin() , tmp.end() );
      for( Entries::reverse_iterator ri = tmp.rbegin() ;
           tmp.rend() != ri; ++ri )
        {
          log << MSG::INFO 
              << " Budget: "       << Local::print( ri->first )
              << " LogVolume  : '" <<            *(ri->second)  
              << "'" << endmsg ;
        }
      log << MSG::ALWAYS << " Total Evaluated Budget ( LogVolumes  ) = " ;
      log << Local::print( total ) << endmsg  ;
      tmp.clear();
    }
  if( m_pvolume )
    {    
      log << MSG::INFO   << stars                                << endmsg ;
      Entries tmp  ;      
      double total = 0 ;
      for( Budget::const_iterator it1 = m_budgetP.begin() ; 
           it1 != m_budgetP.end() ; ++it1 )
        {
          total += it1 -> second ;
          tmp.push_back( Entry( it1->second , &(it1->first) ) );
        }
      std::stable_sort( tmp.begin() , tmp.end() );
      for( Entries::reverse_iterator ri = tmp.rbegin() ;
           tmp.rend() != ri   ; ++ri )
        {
          log << MSG::INFO 
              << " Budget: "       << Local::print( ri->first )
              << " PhysVolume : '" <<            *(ri->second)  
              << "'" << endmsg ;
        }
      log << MSG::ALWAYS << " Total Evaluated Budget ( PhysVolumes ) = " ;
      log << Local::print( total ) << endmsg  ;
      tmp.clear();
    }
  log << MSG::INFO << stars << endmsg ;  
  // clear the budget tables 
  m_budgetL.clear () ;  
  m_budgetP.clear () ;
  m_budgetM.clear () ;
  // finalize the base class 
  return GiGaSensDetBase::finalize ();
}


// ============================================================================
// The END 
// ============================================================================
