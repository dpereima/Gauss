/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4XiccPlusPlus_h
#define G4XiccPlusPlus_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"

// ######################################################################
// ###                         XiccPlusPlus                        ###
// ######################################################################

class G4XiccPlusPlus : public G4ParticleDefinition
{
 private:
  static G4XiccPlusPlus * theInstance ;
  G4XiccPlusPlus( ) { }
  ~G4XiccPlusPlus( ) { }


 public:
  static G4XiccPlusPlus * Definition() ;
  static G4XiccPlusPlus * XiccPlusPlusDefinition() ;
  static G4XiccPlusPlus * XiccPlusPlus() ;
};


#endif
