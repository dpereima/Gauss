/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: G4AntiOmegaccMinus.cpp,v 1.1 2018-09-27 22:53:54 Jingyi Xu Exp $

#include "G4AntiOmegaccMinus.h"
#include "G4ParticleTable.hh"

// ######################################################################
// ###                      AntiOmegaccMinus                        ###
// ######################################################################

G4AntiOmegaccMinus * G4AntiOmegaccMinus::theInstance = 0 ;

G4AntiOmegaccMinus * G4AntiOmegaccMinus::Definition()
{
  if (theInstance !=0) return theInstance;
  const G4String name = "anti-omega_cc-";
  // search in particle table
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance ==0)
  {
  // create particle
  //
  //    Arguments for constructor are as follows
  //               name             mass          width         charge
  //             2*spin           parity  C-conjugation
  //          2*Isospin       2*Isospin3       G-parity
  //               type    lepton number  baryon number   PDG encoding
  //             stable         lifetime    decay table
  //             shortlived      subType    anti_encoding
    anInstance = 
      new G4ParticleDefinition( name ,          3.738*CLHEP::GeV ,   5.e-10*CLHEP::MeV ,    -1.*CLHEP::eplus ,
                                1,              -1,             0,
                                0,               0,             0,
                                "baryon",       0,              -1,             -4432,
                                false,          0.160e-3*CLHEP::ns,    NULL,
                                false,          "omega_cc" );
  }
  theInstance = reinterpret_cast<G4AntiOmegaccMinus*>(anInstance);
  return theInstance;
}

G4AntiOmegaccMinus * G4AntiOmegaccMinus::AntiOmegaccMinusDefinition() {
  return Definition( ) ;
}

G4AntiOmegaccMinus * G4AntiOmegaccMinus::AntiOmegaccMinus() {
  return Definition( ) ;
}
