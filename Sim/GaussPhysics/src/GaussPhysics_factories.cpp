/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 

#include "GiGa/GiGaExtPhysics.h"


/** @file 
 *  The mandatory file for declaration of component library entries 
 *  @author Witold Pokorsky
 *  @author Vanya Belyaev
 *  @author Gloria Corti, port to Gaudi v19
 *  @date 2002-09-26, last modified 2007-01-19
 */

// Geant4 physics lists
#include "G4DecayPhysics.hh"

// EM physics
#include "G4EmStandardPhysics_option1.hh"
#include "G4EmStandardPhysics_option2.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmExtraPhysics.hh"

// LHCb Physics Lists
#include "G4EmStandardPhysics_option1LHCb.hh"
#include "G4EmStandardPhysics_option1NoApplyCuts.hh"
#include "G4EmStandardPhysics_LHCbTest.hh"

// Ion and hadrons
#include "G4IonPhysics.hh"
#include "G4StoppingPhysics.hh"                // G4QStoppingPhysics.hh -> G4StoppingPhysics.hh in G4r10
#include "G4HadronElasticPhysics.hh"
//#include "G4HadronElasticPhysicsLHEP.hh"     // Removed from G4r10
#include "G4HadronElasticPhysicsHP.hh"
#include "G4NeutronTrackingCut.hh"

// LHEP hadrons
//#include "HadronPhysicsLHEP.hh"              // Removed from G4r10

// QGSP hadrons
//#include "G4HadronPhysicsQGSP.hh"            // Removed from G4r10
#include "G4HadronPhysicsQGSP_BERT.hh"
#include "G4HadronPhysicsQGSP_BERT_HP.hh"
//#include "G4HadronPhysicsQGSP_BERT_CHIPS.hh" // Removed from G4r10
#include "G4HadronPhysicsQGSP_FTFP_BERT.hh"

// FTFP hadrons
#include "G4HadronPhysicsFTFP_BERT.hh"
#include "G4HadronPhysicsFTFP_BERT_HP.hh"

// Parallel World Physics
#include "G4ParallelWorldPhysics.hh"


// =========== Specialized extensions to GiGaExtPhysics ===========

template <>
class GiGaExtPhysicsExtender<G4EmStandardPhysics> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
    // No specific properties
  }
  inline G4EmStandardPhysics *newInstance(const std::string &name, int verbosity) const {
    return new G4EmStandardPhysics(verbosity, name);
  }
};

template <>
class GiGaExtPhysicsExtender<G4EmStandardPhysics_option1> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
    // No specific properties
  }
  inline G4EmStandardPhysics_option1 *newInstance(const std::string &name, int verbosity) const {
    return new G4EmStandardPhysics_option1(verbosity, name);
  }
};

template <>
class GiGaExtPhysicsExtender<G4EmStandardPhysics_option2> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
    // No specific properties
  }
  inline G4EmStandardPhysics_option2 *newInstance(const std::string &name, int verbosity) const {
    return new G4EmStandardPhysics_option2(verbosity, name);
  }
};

template <>
class GiGaExtPhysicsExtender<G4EmStandardPhysics_option3> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
    // No specific properties
  }
  inline G4EmStandardPhysics_option3 *newInstance(const std::string &name, int verbosity) const {
    return new G4EmStandardPhysics_option3(verbosity, name);
  }
};

template <>
class GiGaExtPhysicsExtender<G4EmStandardPhysics_option1NoApplyCuts> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
    // No specific properties
  }
  inline G4EmStandardPhysics_option1NoApplyCuts *newInstance(const std::string &name, int verbosity) const {
    return new G4EmStandardPhysics_option1NoApplyCuts(verbosity, name);
  }
};

template <>
class GiGaExtPhysicsExtender<G4EmStandardPhysics_option1LHCb> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("ApplyCuts", m_applyCuts = true, 
                          "Apply production cuts to all EM processes for the LHCb EM constructor");
    tool->declareProperty("NewModelForE", m_newForE = true,
                          "Use new MS models for electrons and positrons");
  }
  inline G4EmStandardPhysics_option1LHCb *newInstance(const std::string &/*name*/, int verbosity) const {
    return new G4EmStandardPhysics_option1LHCb(verbosity, m_applyCuts, m_newForE);
  }
private:
  bool m_applyCuts;
  bool m_newForE;
};

template <>
class GiGaExtPhysicsExtender<G4EmStandardPhysics_LHCbTest> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("ApplyCuts", m_applyCuts = true, 
                          "Apply production cuts to all EM processes for the LHCb EM constructor");
    tool->declareProperty("NewModelForE", m_newForE = true,
                          "Use new MS models for electrons and positrons");
  }
  inline G4EmStandardPhysics_LHCbTest *newInstance(const std::string &/*name*/, int verbosity) const {
    return new G4EmStandardPhysics_LHCbTest(verbosity, m_applyCuts, m_newForE);
  }
private:
  bool m_applyCuts;
  bool m_newForE;
};

template <>
class GiGaExtPhysicsExtender<G4DecayPhysics> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
    // No specific properties
  }
  inline G4DecayPhysics *newInstance(const std::string &name, int verbosity) const {
    return new G4DecayPhysics(name, verbosity);
  }
};


template <>
class GiGaExtPhysicsExtender<G4StoppingPhysics> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("UseMuonMinusCapture", m_useMuonMinusCapture = true,
                          "Parameter 'UseMuonMinusCapture' for the constructor of G4StoppingPhysics");
  }
  inline G4StoppingPhysics *newInstance(const std::string &name, int verbosity) const {
    return new G4StoppingPhysics(name, verbosity, m_useMuonMinusCapture);
  }
private:
  bool m_useMuonMinusCapture;
};


template <>
class GiGaExtPhysicsExtender<G4HadronElasticPhysics> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
  // no specialised properties
    //    tool->declareProperty("HighPrecision", m_highPrecision = false,
    //                          "Parameter 'HighPrecision' for the constructor of G4HadronElasticPhysics");
    // tool->declareProperty("Glauber", m_glauber = false,
    //                          "Parameter 'Glauber' for the constructor of G4HadronElasticPhysics");
  }
  inline G4HadronElasticPhysics *newInstance(const std::string &/*name*/, int verbosity) const {
    //return new G4HadronElasticPhysics(name, verbosity, m_highPrecision, m_glauber);
    return new G4HadronElasticPhysics(verbosity);
  }
//private:
//  bool m_highPrecision;
//  bool m_glauber;
};

// Removed in G4r10
/*template <>
class GiGaExtPhysicsExtender<G4HadronElasticPhysicsLHEP> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
  // No specific properties
  }
  inline G4HadronElasticPhysicsLHEP *newInstance(const std::string &name, int verbosity) const {
    return new G4HadronElasticPhysicsLHEP(verbosity);
  }
};*/

template <>
class GiGaExtPhysicsExtender<G4HadronElasticPhysicsHP> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
  // No specific properties
  }
  inline G4HadronElasticPhysicsHP *newInstance(const std::string &/*name*/, int verbosity) const {
    return new G4HadronElasticPhysicsHP(verbosity);
  }
};

// Removed in G4r10
/*template <>
class GiGaExtPhysicsExtender<HadronPhysicsQGSP> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("QuasiElastic", m_quasiElastic = true,
                          "Parameter 'quasiElastic' for the constructor of HadronPhysicsQGSP");
  }
  inline HadronPhysicsQGSP *newInstance(const std::string &name, int verbosity) const {
    return new HadronPhysicsQGSP(name, m_quasiElastic);
  }
private:
  bool m_quasiElastic;
};*/

template <>
class GiGaExtPhysicsExtender<G4HadronPhysicsQGSP_BERT> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("QuasiElastic", m_quasiElastic = true,
                          "Parameter 'quasiElastic' for the constructor of HadronPhysicsQGSP_BERT");
  }
  inline G4HadronPhysicsQGSP_BERT *newInstance(const std::string &name, int /*verbosity*/) const {
    return new G4HadronPhysicsQGSP_BERT(name, m_quasiElastic);
  }
private:
  bool m_quasiElastic;
};

template <>
class GiGaExtPhysicsExtender<G4HadronPhysicsQGSP_BERT_HP> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("QuasiElastic", m_quasiElastic = true,
                          "Parameter 'quasiElastic' for the constructor of HadronPhysicsQGSP_BERT_HP");
  }
  inline G4HadronPhysicsQGSP_BERT_HP *newInstance(const std::string &name, int /*verbosity*/) const {
    return new G4HadronPhysicsQGSP_BERT_HP(name, m_quasiElastic);
  }
private:
  bool m_quasiElastic;
};

// Removed in G4r10
/*template <>
class GiGaExtPhysicsExtender<HadronPhysicsQGSP_BERT_CHIPS> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("QuasiElastic", m_quasiElastic = true,
                          "Parameter 'quasiElastic' for the constructor of HadronPhysicsQGSP_BERT_HP");
  }
  inline HadronPhysicsQGSP_BERT_CHIPS *newInstance(const std::string &name, int verbosity) const {
    return new HadronPhysicsQGSP_BERT_CHIPS(name, m_quasiElastic);
  }
private:
  bool m_quasiElastic;
};*/

template <>
class GiGaExtPhysicsExtender<G4HadronPhysicsQGSP_FTFP_BERT> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("QuasiElastic", m_quasiElastic = true,
                          "Parameter 'quasiElastic' for the constructor of HadronPhysicsQGSP_FTFP_BERT");
  }
  inline G4HadronPhysicsQGSP_FTFP_BERT *newInstance(const std::string &name, int /*verbosity*/) const {
    return new G4HadronPhysicsQGSP_FTFP_BERT(name, m_quasiElastic);
  }
private:
  bool m_quasiElastic;
};

template <>
class GiGaExtPhysicsExtender<G4HadronPhysicsFTFP_BERT> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("QuasiElastic", m_quasiElastic = false,
                          "Parameter 'quasiElastic' for the constructor of HadronPhysicsFTFP_BERT");
  }
  inline G4HadronPhysicsFTFP_BERT *newInstance(const std::string &name, int /*verbosity*/) const {
    return new G4HadronPhysicsFTFP_BERT(name, m_quasiElastic);
  }
private:
  bool m_quasiElastic;
};

template <>
class GiGaExtPhysicsExtender<G4HadronPhysicsFTFP_BERT_HP> {
public:
  inline void addPropertiesTo(AlgTool *tool) {
    tool->declareProperty("QuasiElastic", m_quasiElastic = false,
                          "Parameter 'quasiElastic' for the constructor of HadronPhysicsFTFP_BERT_HP");
  }
  inline G4HadronPhysicsFTFP_BERT_HP *newInstance(const std::string &name, int /*verbosity*/) const {
    return new G4HadronPhysicsFTFP_BERT_HP(name, m_quasiElastic);
  }
private:
  bool m_quasiElastic;
};

template <>
class GiGaExtPhysicsExtender<G4NeutronTrackingCut> {
public:
  inline void addPropertiesTo(AlgTool */*tool*/) {
    // No specific properties
  }
  inline G4NeutronTrackingCut *newInstance(const std::string &name, int verbosity) const {
    return new G4NeutronTrackingCut(name, verbosity);
  }
};

// Parallel World Physics

template <>
class GiGaExtPhysicsExtender<G4ParallelWorldPhysics> {

  public:
    inline void addPropertiesTo(AlgTool *tool) {
      tool->declareProperty("WorldName", m_worldName, "Corresponding name of the parallel world");
      // Layered Mass Geometry: if on, then you can define materials in the parallel world and they will overwrite
      // those in the mass geometry. If more than one parallel world is defined, then those later defined will be 
      // on the top of the hierarchy.  
      tool->declareProperty("LayeredMass", m_layeredMass, "Parallel world on top of the mass geometry");
    }

    inline G4ParallelWorldPhysics *newInstance(const std::string /* &name */, int /* verbosity */) const {
      return new G4ParallelWorldPhysics(m_worldName, m_layeredMass);
    }

  private:
    std::string m_worldName = "";
    bool m_layeredMass = false;
};

typedef GiGaExtPhysics< G4EmStandardPhysics_option1 > EmStdOpt1PhysFactory;
DECLARE_COMPONENT( EmStdOpt1PhysFactory )
typedef GiGaExtPhysics< G4EmStandardPhysics_option2 > EmStdOpt2PhysFactory;
DECLARE_COMPONENT( EmStdOpt2PhysFactory )
typedef GiGaExtPhysics< G4EmStandardPhysics_option3 > EmStdOpt3PhysFactory;
DECLARE_COMPONENT( EmStdOpt3PhysFactory )
typedef GiGaExtPhysics< G4EmStandardPhysics > EmStdPhysFactory;
DECLARE_COMPONENT( EmStdPhysFactory )
typedef GiGaExtPhysics< G4DecayPhysics > DecayFactory;
DECLARE_COMPONENT( DecayFactory )

typedef GiGaExtPhysics< G4EmStandardPhysics_option1LHCb > EmStdLHCbPhysFactory;
DECLARE_COMPONENT( EmStdLHCbPhysFactory )
typedef GiGaExtPhysics< G4EmStandardPhysics_option1NoApplyCuts > EmStdOpt1NoCutsPhysFactory;
DECLARE_COMPONENT( EmStdOpt1NoCutsPhysFactory )
typedef GiGaExtPhysics< G4EmStandardPhysics_LHCbTest > EmStdLHCbTestPhysFactory;
DECLARE_COMPONENT( EmStdLHCbTestPhysFactory )

typedef GiGaExtPhysics< G4EmExtraPhysics > EmExtraPhysFactory;
DECLARE_COMPONENT( EmExtraPhysFactory )

typedef GiGaExtPhysics< G4IonPhysics > IonPhysFactory;
DECLARE_COMPONENT( IonPhysFactory )
typedef GiGaExtPhysics< G4StoppingPhysics > StopPhysFactory;
DECLARE_COMPONENT( StopPhysFactory )
typedef GiGaExtPhysics< G4HadronElasticPhysics > HadElPhysFactory;
DECLARE_COMPONENT( HadElPhysFactory )

typedef GiGaExtPhysics< G4HadronElasticPhysicsHP > HadElHPPhysFactory;
DECLARE_COMPONENT( HadElHPPhysFactory )

typedef GiGaExtPhysics< G4NeutronTrackingCut > NeuTrkCutFactory;
DECLARE_COMPONENT( NeuTrkCutFactory )

typedef GiGaExtPhysics< G4HadronPhysicsQGSP_BERT > HadPhysQGSP_BERTFactory;
DECLARE_COMPONENT( HadPhysQGSP_BERTFactory )
typedef GiGaExtPhysics< G4HadronPhysicsQGSP_BERT_HP > HadPhysQGSP_BERT_HPFactory;
DECLARE_COMPONENT( HadPhysQGSP_BERT_HPFactory )

typedef GiGaExtPhysics< G4HadronPhysicsQGSP_FTFP_BERT > HadPhysQGSP_FTFP_BERTFactory;
DECLARE_COMPONENT( HadPhysQGSP_FTFP_BERTFactory )

typedef GiGaExtPhysics< G4HadronPhysicsFTFP_BERT > HadPhysFTFP_BERTFactory;
DECLARE_COMPONENT( HadPhysFTFP_BERTFactory )

typedef GiGaExtPhysics< G4HadronPhysicsFTFP_BERT_HP > HadPhysFTFP_BERT_HPFactory;
DECLARE_COMPONENT( HadPhysFTFP_BERT_HPFactory )

using ParallelWorldPhysicsFactory = GiGaExtPhysics<G4ParallelWorldPhysics>;
DECLARE_COMPONENT_WITH_ID( ParallelWorldPhysicsFactory, "DefaultParallelPhysics" )
