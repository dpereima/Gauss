/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaussCherenkov/RichPmtPSF.h"


RichPmtPSF::RichPmtPSF()
  : m_pmtPointSpreadFunctionVect(4),
    m_pmtPSFPhoEnergyVect(4),
    m_pmtPSFRadialPosVect(4)
 { ; }
// the 4 above is just a dummy value.

RichPmtPSF::~RichPmtPSF() { ; }

void RichPmtPSF::resetPSFVect() {

  m_pmtPointSpreadFunctionVect.clear();
  m_pmtPSFPhoEnergyVect.clear();
  m_pmtPSFRadialPosVect.clear();

}


