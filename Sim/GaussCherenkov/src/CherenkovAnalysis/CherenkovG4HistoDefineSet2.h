/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CherenkovG4HistoDefineSet2_H
#define CherenkovG4HistoDefineSet2_H 1

// Include files
// #include "GaudiKernel/Algorithm.h"
// from STL
#include <string>
#include <cmath>
#include <vector>
#include "GaudiKernel/ISvcLocator.h"

// Forward declarations
class IHistogram1D;
class IHistogram2D;

// Author SE 21-8-2002
class CherenkovG4HistoDefineSet2 {

public:
  CherenkovG4HistoDefineSet2();

private:
  // These data members are used in the execution of this algorithm
  // They are set in the initialisation phase by the job options service
  // for the RichG4RunAction.
  // bool        m_DefineRichG4HistogramSet2;  // flag for histo production
  /// Book histograms
  void bookRichG4HistogramsSet2();

  // Histograms ( used if m_produceHistogramSet2 = 1 (true) )

  IHistogram1D*         m_hMomChPartRich1{nullptr};
  IHistogram1D*         m_hMomChPartRich2{nullptr};
  IHistogram1D*         m_hNumTotHitAgelRich1{nullptr};
  IHistogram1D*         m_hNumHitAgelPrim{nullptr};
  IHistogram1D*         m_hNumHitAgelSat{nullptr};
  IHistogram1D*         m_hNumHitAgelSatNoRefl{nullptr};
  IHistogram1D*         m_hNumHitAgelWithRlySat{nullptr};
  IHistogram1D*         m_hNumTotHitC4F10Rich1{nullptr};
  IHistogram1D*         m_hNumHitC4F10Prim{nullptr};
  IHistogram1D*         m_hNumHitC4F10Sat{nullptr};
  IHistogram1D*         m_hNumHitC4F10SatNoRefl{nullptr};
  IHistogram1D*         m_hNumHitC4F10SatNoReflHighMom{nullptr};
  IHistogram1D*         m_hNumTotHitCF4Rich2{nullptr};
  IHistogram1D*         m_hNumTotHitCF4NonScintRich2{nullptr};
  IHistogram1D*         m_hNumTotHitCF4ScintRich2{nullptr};
  
  IHistogram1D*         m_hNumTotHitNoRadiatorRich1{nullptr};
  IHistogram1D*         m_hNumTotHitNoRadiatorRich2{nullptr};
  IHistogram1D*         m_hNumTotHitNoRadiator{nullptr};
  IHistogram1D*          m_hNumHitCF4Prim{nullptr};
  IHistogram1D*          m_hNumHitCF4Sat{nullptr};
  IHistogram1D*          m_hNumHitCF4SatNoRefl{nullptr};
  IHistogram1D*          m_hNumHitCF4SatNoReflNoScintHighMom{nullptr};
  IHistogram1D*          m_hNumHitCF4SatNoReflNoScint{nullptr};
  IHistogram1D*          m_hNumHitCF4SatScin{nullptr};
  IHistogram1D*         m_hNumTotHitRich1Large{nullptr};
  IHistogram1D*         m_hNumTotHitAgelRich1Large{nullptr};
  IHistogram1D*         m_hNumTotHitGasRich1Large{nullptr};
  IHistogram1D*         m_hNumTotHitRich2Large{nullptr};
 
  IHistogram2D*         m_hCkvProdAgelRich1{nullptr};
  IHistogram2D*         m_hCkvProdC4F10Rich1{nullptr};
  IHistogram2D*         m_hCkvProdCF4Rich2{nullptr};

  IHistogram1D*        m_hNumTotHitAgelFullAcceptSat{nullptr};  
  IHistogram1D*        m_hNumTotHitC4F10FullAcceptSat{nullptr};
  IHistogram1D*        m_hNumTotHitCF4FullAcceptSat{nullptr};
  IHistogram1D*        m_hNumTotSignalHitC4F10FullAcceptSat{nullptr};
  IHistogram1D*        m_hNumTotSignalHitCF4FullAcceptSat{nullptr};
  
  IHistogram2D*        m_hNumHitVsAngAgelFullAcceptSat{nullptr};  
  IHistogram2D*        m_hNumHitVsAngC4F10FullAcceptSat{nullptr};
  IHistogram2D*        m_hNumHitVsAngCF4FullAcceptSat{nullptr};

  IHistogram2D*        m_hNumHitVsTrPhiC4F10FullAcceptSat{nullptr};
  IHistogram2D*        m_hNumHitVsTrPhiCF4FullAcceptSat{nullptr};  

  std::string m_RichG4HistoPathSet2{"RICHG4HISTOSET2/"}; ///< Histo path
  ISvcLocator* m_svcLocSet2{nullptr};



};

#endif  //end of CherenkovG4DefineHistoSet2




