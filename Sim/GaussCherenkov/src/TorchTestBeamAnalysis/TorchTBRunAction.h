/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef TORCHTESTBEAMANALYSIS_TORCHTBRUNACTION_H
#define TORCHTESTBEAMANALYSIS_TORCHTBRUNACTION_H 1

// Include files
#include "GiGa/GiGaRunActionBase.h"

/** @class TorchTBRunAction TorchTBRunAction.h TorchTestBeamAnalysis/TorchTBRunAction.h
 *
 *
 *  @author Sajan Easo
 *  @date   2012-06-01
 */
class TorchTBG4DefineHistSet6;

class TorchTBRunAction: public virtual GiGaRunActionBase {
public:
  /// Standard constructor
  TorchTBRunAction(const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent  );

  virtual ~TorchTBRunAction( ); ///< Destructor
  void BeginOfRunAction ( const G4Run* run ) override;


protected:

private:
  TorchTBRunAction();
  TorchTBRunAction(const TorchTBRunAction&);
  TorchTBRunAction& operator = (const  TorchTBRunAction& );
private:
  bool m_ActivateDefineTorchTBHistoSet6;
  TorchTBG4DefineHistSet6* m_TorchTBG4DefineHistSet6;




};
#endif // TORCHTESTBEAMANALYSIS_TORCHTBRUNACTION_H
