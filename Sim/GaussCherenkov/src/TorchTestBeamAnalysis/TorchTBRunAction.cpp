/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
// Include files 



// local
#include "TorchTBRunAction.h"
#include "TorchTBG4DefineHistSet6.h"

DECLARE_COMPONENT( TorchTBRunAction )

//-----------------------------------------------------------------------------
// Implementation file for class : TorchTBRunAction
//
// 2012-06-01 : Sajan Easo
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TorchTBRunAction::TorchTBRunAction(const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent   ) 
  : GiGaRunActionBase( type , name , parent ),
    m_ActivateDefineTorchTBHistoSet6(false),
    m_TorchTBG4DefineHistSet6(0)
{
  declareProperty("ActivateDefineHistoSet6", m_ActivateDefineTorchTBHistoSet6);
  

}
//=============================================================================
// Destructor
//=============================================================================
TorchTBRunAction::~TorchTBRunAction() {} 

//=============================================================================
void TorchTBRunAction::BeginOfRunAction( const G4Run* run ) 
{
  if( 0 == run )
  { Warning("BeginOfRunAction:: G4Run* points to NULL!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
   
  if( m_ActivateDefineTorchTBHistoSet6 ) {
    m_TorchTBG4DefineHistSet6= new TorchTBG4DefineHistSet6(); 
    
  }
  
}

