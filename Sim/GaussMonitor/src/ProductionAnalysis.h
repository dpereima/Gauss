/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ProductionAnalysis.h,v 1.1 2007-05-16 17:33:29 gcorti Exp $
#ifndef PRODUCTIONANALYSIS_H
#define PRODUCTIONANALYSIS_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// From HepMC
#include "Event/HepMCEvent.h"

/** @class ProductionAnalysis ProductionAnalysis.h
 *
 *  Based on the Generator Analysis Algorithm,
 *  Allows generator-level studies on particles with specific PID
 *  Analysis algorithms for the generator sequences with PID reference
 *  histograms for generators comparisons
 *
 *  @author R. Lambert
 *  @date   2007-04-24
 */
class ProductionAnalysis : public GaudiHistoAlg {
public:
  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:


private:
  Gaudi::Property<std::string>    m_dataPath{this,"Input",LHCb::HepMCEventLocation::Default,"location of input data"};            ///< location of input data
  Gaudi::Property<double>         m_minEta{this,"MinEta",2.0,"Min pseudo rapidity acceptance"};              ///< Min pseudo rapidity acceptance
  Gaudi::Property<double>         m_maxEta{this,"MaxEta",4.9,"Max pseudo rapidity acceptance"};              ///< Max pseudo rapidity acceptance

  int            m_counter{0};             ///<counter of particles in acceptance
  int            m_nEvents{0};             ///<counter of events

  Gaudi::Property<std::string>    m_generatorName{this, "ApplyTo", "","specifically apply to this generator"}; ///< specifically apply to this generator
  Gaudi::Property<std::string>    m_signalName{this, "Signal","B0 ","which Particle to look for?"};   ///< which Particle to look for?

  Gaudi::Property<int>            m_hepPID{this,"SignalPID",511,"an option for the PID, which particle to look for."};       ///< an option for the PID, which particle to look for.
  LHCb::ParticleID m_sPID;       ///< PID of signal particle


  std::string m_inputData ;  ///< Location where to find HepMC event


};
#endif // PRODUCTIONANALYSIS_H
