/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GaussGenUtil.h,v 1.1 2007-03-07 18:51:00 gcorti Exp $
#ifndef GAUSSGENUTIL_H 
#define GAUSSGENUTIL_H 1

// Include files

// forward declaration
namespace HepMC {
  class GenParticle;
}


/** @namespace GenUtil GenUtil.h
 *  @brief Utilities for generator
 *  The lifetime returned is c*tau (mm)
 *
 *  @author Gloria CORTI
 *  @date   2007-02-16
 */

namespace GaussGenUtil {

  double lifetime( const HepMC::GenParticle* );
  
}

#endif // GAUSSGENUTIL_H
