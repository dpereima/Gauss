###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# content of the file BParticleGun.py
#pgun.FlatNParticles.MinNParticles = 2
#pgun.FlatNParticles.MaxNParticles = 2
from Configurables import ParticleGun
from Configurables import MomentumRange
ParticleGun().addTool( MomentumRange )
from GaudiKernel import SystemOfUnits
ParticleGun().MomentumRange.MomentumMin = 1.0*SystemOfUnits.GeV
from GaudiKernel import SystemOfUnits
ParticleGun().MomentumRange.MomentumMax = 100.*SystemOfUnits.GeV
#ParticleGun().EventType = 11102013
#ParticleGun().EventType = 34512107
ParticleGun().EventType = 34102100
ParticleGun().ParticleGunTool = "MomentumRange"
ParticleGun().NumberOfParticlesTool = "FlatNParticles"
ParticleGun().MomentumRange.PdgCodes = [ 310 ]
from Configurables import ToolSvc
from Configurables import EvtGenDecay
tsvc = ToolSvc()
tsvc.addTool( EvtGenDecay , name = "EvtGenDecay" )
#tsvc.EvtGenDecay.UserDecayFile = "/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/Gen/DecFiles/v30r18/dkfiles/D0_pipi=DecProdCut.dec"
#tsvc.EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Bd_pi+pi-=CPV,DecProdCut.dec"
tsvc.EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Ks_pipi.dec"
#tsvc.EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Ks_pipi,mm=TightCut.dec"
ParticleGun().DecayTool = "EvtGenDecay"

from Gaudi.Configuration import *
importOptions( "$DECFILESROOT/options/CaloAcceptance.py" )



