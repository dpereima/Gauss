###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## 
##  Example on how to run only the generator phase
##  It can be passed as additional argument to gaudirun.py directly
##   > gaudirun.py $APPCONFIGOPTS/Gauss/MC09-b5TeV-md100.py \
##                 $APPCONFIGOPTS/Conditions/MC09-20090602-vc-md100.py \
##                 $DECFILESROOT/options/EVENTTYPE.opts \
##                 $LBPYTHIAROOT/options/Pythia.opts \
##                 $GAUSSOPTS/GenStandAlone.py \
##                 $GAUSSOPTS/Gauss-Job.py
##  or you can set the property in your Gauss-Job.py
##  Port to python of GenStandAlone.opts
## 

from Configurables import Gauss
from Gaudi.Configuration import * 
import os
Gauss().Phases = ["Generator"]

def lamarrForGauss():
    from Configurables import (GaudiSequencer, SimInit, 
                               ApplicationMgr, LamarrHist, LamarrTuple,
                               LamarrCaloProto, BooleInit, PGPrimaryVertex, LamarrPropagator)
    
    from Configurables import ( LamarrRecoSummary,
                                LamarrParticleId,
                                ChargedProtoCombineDLLsAlg,
                                ChargedProtoParticleAddRichInfo,
                                ChargedProtoParticleAddMuonInfo  )
    
    _modeldir = "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190823/CompiledModel"
    if 'HOSTNAME' in os.environ and 'pclhcb06' in os.environ['HOSTNAME']:
      _modeldir = "/pclhcb06/landerli/DiamondGanModels/compiled_model_2019-08-20_13_50_49.864570/"

    LamarrPID = LamarrParticleId ( "LamarrParticleId",
          CompiledModel = _modeldir,
          CompiledModelInput = 'inputs',
          CompiledModelOutput = 'output', 
        )

    

###                                    RichGan   = {
###         ###### New Yandex models
###         'mu+': "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/FastFastRICH_Cramer_muon_tfScaler/",
###         'mu-': "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/FastFastRICH_Cramer_muon_tfScaler/",
###         'p+': "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/FastFastRICH_Cramer_proton_tfScaler/",
###         'p~-': "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/FastFastRICH_Cramer_proton_tfScaler/",
###         'K+': "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/FastFastRICH_Cramer_kaon_tfScaler/",
###         'K-': "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/FastFastRICH_Cramer_kaon_tfScaler/",
###         'pi+': "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/FastFastRICH_Cramer_pion_tfScaler/",
###         'pi-': "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/FastFastRICH_Cramer_pion_tfScaler/",
###                   },
###                                    RichGanInput = 'x',
###                                    RichGanOutput = 'QuantileTransformerTF_3/stack',
###                                    
###                                    MuonLLGan = {
###         'mu+' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/diamondGan-2019-04-2401_43_08.499323/",
###         'mu-' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/diamondGan-2019-04-2401_43_08.499323/",
###         'pi+' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/MuGanPion-2019-04-2913_16_04.022940/",
###         'pi-' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/MuGanPion-2019-04-2913_16_04.022940/",
###         'K+'  : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/MuGanKaon-2019-04-2913_16_43.860800/",
###         'K-'  : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/MuGanKaon-2019-04-2913_16_43.860800/",
###         'p+'  : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/MuGanProton-2019-04-3000_01_07.504122/",
###         'p~-' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/MuGanProton-2019-04-3000_01_07.504122/",
###         },
###                                    MuonLLGanInput = 'X_gen',
###                                    MuonLLGanOutput = 'output',
###                                    
###                                    isMuonMlp = {
###         'mu+' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/MuonIsMuon2019-05-03__11_11_19.978969/",
###         'mu-' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/MuonIsMuon2019-05-03__11_11_19.978969/",
###         'pi+' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/PionIsMuon2019-05-03__11_12_17.633997/",
###         'pi-' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/PionIsMuon2019-05-03__11_12_17.633997/",
###         'K+'  : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/KaonIsMuon2019-05-03__11_11_58.436823/",
###         'K-'  : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/KaonIsMuon2019-05-03__11_11_58.436823/",
###         'p+'  : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/ProtonIsMuon2019-05-03__11_09_52.500469/",
###         'p~-' : "/eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/ProtonIsMuon2019-05-03__11_09_52.500469/",
###         },
###                                    isMuonMlpInput = 'inputds',
###                                    isMuonMlpOutput = 'strided_slice',
                                   
                                   #OutputLevel = VERBOSE,
#                                   )
    
    LamarrRecStat = LamarrRecoSummary ('LamarrRecoSummary',
                                       nTracksHistogramFile = "root://eosuser.cern.ch//eos/lhcb/user/l/landerli/FastSimulationModels/v20190503/KS0_nTracks_Brunel.root",
                                       nTracksHistogramName = "dataHist",
                                       #OutputLevel = VERBOSE,
                                       )
#    LamarrPID.OutputLevel = DEBUG
    #make the default card
    #PGPrimaryVertex().OutputLevel=DEBUG
    PGPrimaryVertex().OutputVerticesName = '/Event/Rec/Vertex/Primary'
    #    PGPrimaryVertex().InputVerticesName = '/Event/MCFast/MCVertices'
    PGPrimaryVertex().InputVerticesName = '/Event/MC/Vertices'
#    LamarrPropagator().OutputLevel=DEBUG
    LamarrPropagator().ChargedParticlesStates = ['EndVelo','BegRich1','EndRich1','EndT','BegRich2']
    LamarrPropagator().FormulaZCenterMagnet = "5016.2 -1697.4*x+6.4e8*x*x"
    LamarrPropagator().TrackLUT = "$LBLAMARRROOT/LookUpTables/"
    LamarrPropagator().TrackCovarianceLUT = "$LBLAMARRROOT/LookUpTables/"
    LamarrPropagator().AcceptanceModelLib = "/pclhcb06/landerli/LamarrTrainingScripts/compiledC/acceptance_2016-MagUp.so"
    LamarrPropagator().EfficiencyModelLib = "/pclhcb06/landerli/LamarrTrainingScripts/compiledC/trkEfficiency_2016-MagUp.so"
#    LamarrPropagator().UseEfficiency = False
    #    LamarrPropagator().States = ['EndVelo']
    lamarrSeq = GaudiSequencer("LamarrSeq", MeasureTime = True)
    lamarrSeq.Members = [ SimInit("InitLamarr") ]
    lamarrSeq.Members += [ LamarrPropagator("LamarrPropagator") ]    
    #    lamarrSeq.Members += [ LamarrHist("LamarrHist") ]
    lamarrSeq.Members += [ LamarrRecStat ] 
    lamarrSeq.Members += [ LamarrPID ] 
#    lamarrSeq.Members += [ ChargedProtoParticleAddRichInfo ('ChargedProtoParticleAddRichInfo') ] 
#    lamarrSeq.Members += [ ChargedProtoParticleAddMuonInfo ('ChargedProtoParticleAddMuonInfo') ] 
#    lamarrSeq.Members += [ ChargedProtoCombineDLLsAlg ('ChargedProtoCombineDLLsAlg') ]
    lamarrSeq.Members += [ LamarrCaloProto("LamarrCaloProto") ]
#    lamarrSeq.Members += [ LamarrTuple("LamarrTuple") ]
    lamarrSeq.Members += [ BooleInit("BooleInit", ModifyOdin = True) ]
    lamarrSeq.Members += [ PGPrimaryVertex("PGPrimaryVertex")]
    lamarrSeq.Members += [ GaudiSequencer("LamarrMonitor") ]
    ApplicationMgr().TopAlg += [ lamarrSeq ]

appendPostConfigAction( lamarrForGauss )

