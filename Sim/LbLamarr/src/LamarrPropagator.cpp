/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Standard C
#include <cstdlib>

// Standard Template Libraries
#include <sstream>

// Gaudi
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/KeyedTraits.h"

// Event
#include "Event/HepMCEvent.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/StateParameters.h"
#include "Event/TrackEnums.h"
#include "Event/State.h"

// HepMC
#include "GenEvent/HepMCUtils.h"

// ROOT
#include "TDatabasePDG.h"

// local
#include "LamarrPropagator.h"

// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

//from ROOT
#include "TROOT.h"
#include "TFile.h"
#include "TMath.h"
#include "TAxis.h"
#include "TString.h"
#include "TFormula.h"
#include "TRandom3.h"
#include "TObjArray.h"
#include "TLorentzVector.h"
#include "TVector3.h"


//-----------------------------------------------------------------------------
// Implementation file for class : LamarrPropagator
//

// 2019 - : Benedetto G. Siddi
// 2021 - : Lucio Anderlini  (migrated to scikinC) 
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LamarrPropagator )
//=============================================================================
// Standard constructor, initializes variables

LamarrPropagator::LamarrPropagator( const std::string& name,ISvcLocator* pSvcLocator)
: GaudiAlgorithm ( name , pSvcLocator ){}

//=============================================================================
// Initialization
//=============================================================================
StatusCode LamarrPropagator::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_gaussDist.initialize( randSvc , Rndm::Gauss( 0. , 1. ) ) ;
  if(sc.isFailure())
  {
    error()<<"Cannot initialize flat random number generator"<<endmsg;
    return sc;    
  }
  sc = m_flatDist.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if(sc.isFailure())
  {
    error()<<"Cannot initialize flat random number generator"<<endmsg;
    return sc;
  }
  
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Loading look-up tables"<<endmsg;  


  //// The following block define a raw resolution model which is currently
  //// used for track states other than ClosestToBeam, less critical for physics
  std::unique_ptr<TFile> resFile =  std::make_unique<TFile>(m_ResolutionHisto.value().c_str(),"OPEN");
  debug()<<"Resolution Filename: "<<resFile->GetName()<<endmsg;
  for(auto& stateName : m_chargedStates.value())
  {
    std::stringstream ss;
    ss<<stateName;
    
    std::string resName = "sparseRes_"+ss.str();
    debug()<<"Getting "<<resName<<" resolution histo"<<endmsg;
    
    const auto h = std::unique_ptr<THnSparseD>((THnSparseD*)resFile->Get(resName.c_str()));
    if(h)
    {
      debug()<<"Loaded resolution histo: "<<h->GetName()<<endmsg;

      // Cache the projections 
      if ( msgLevel(MSG::DEBUG) ) debug()<< "Computing resolution lookup-tables" <<endmsg;  
      const auto& location = map_Locations[stateName]; 
      for (int iAxis = 0; iAxis < 6; ++iAxis) 
      {
        std::string hname = std::string("resolutions")+stateName+std::to_string(iAxis);      
        m_map_res[iAxis][location] = std::make_unique<TH3F>(hname.c_str(), "",
                                                            h->GetAxis(0)->GetNbins(), h->GetAxis(0)->GetXmin(), h->GetAxis(0)->GetXmax(), 
                                                            h->GetAxis(1)->GetNbins(), h->GetAxis(1)->GetXmin(), h->GetAxis(1)->GetXmax(), 
                                                            h->GetAxis(2)->GetNbins(), h->GetAxis(2)->GetXmin(), h->GetAxis(2)->GetXmax());



        auto& map = m_map_res[iAxis][location];
        map->SetDirectory(nullptr); 
        std::unique_ptr<TH3F> sum  (static_cast<TH3F*>(map->Clone("sum")));
        std::unique_ptr<TH3F> sum2 (static_cast<TH3F*>(map->Clone("sum2")));
        std::unique_ptr<TH3F> totW (static_cast<TH3F*>(map->Clone("totW")));
        int i[6];
        // Loop on the filled bins 
        for (int iBin = 0; iBin < h->GetNbins(); ++iBin)
        {
          const double w = h->GetBinContent(iBin, i); 
          const double x = h->GetAxis(iAxis)->GetBinCenter(i[iAxis]); 
          const int binId = sum->GetBin(i[0],i[1],i[2]); 

          sum->AddBinContent (binId, w*x ); 
          sum2->AddBinContent (binId, w*w*x*x); 
          totW->AddBinContent (binId, w ); 
        }

        for (int iBin = 1; iBin <= map->GetNcells(); ++iBin)
        {
          double w     = totW->GetBinContent(iBin);
          if (w == 0) continue; 
          double mean2 = sum2->GetBinContent(iBin) / w;
          double mean  = sum->GetBinContent(iBin) / w; 

          map -> SetBinContent(iBin, sqrt(mean2 - mean*mean) ); 
        }
      }
    }
  }
  resFile->Close();

  StatusCode retcode = release(randSvc);
  if (retcode != StatusCode::SUCCESS)
    return retcode;

  //////////////////////////////////////////////////////////////////////////////
  // Load External ML functions 
  if (m_useEfficiency)
  {
    m_acceptance_mlfun = Lamarr::load_mlfun<Lamarr::mlfun>(
      m_acceptance_model_lib, m_acceptance_model_entrypoint);
    
    m_efficiency_mlfun = Lamarr::load_mlfun<Lamarr::mlfun>(
      m_trkEfficiency_model_lib, m_trkEfficiency_model_entrypoint);

    if (!m_acceptance_mlfun || !m_efficiency_mlfun)
      return StatusCode::FAILURE;
  }

  //////////////////////////////////////////////////////////////////////////////
  if (m_useResolution)
  {
    // Full pipelines 
    m_resol [LHCb::Event::Enum::Track::Type::Long] = Lamarr::load_mlfun <Lamarr::ganfun>(
      m_resolution_model_lib, m_resolution_long_entrypoint
    );
    m_resol [LHCb::Event::Enum::Track::Type::Upstream] = Lamarr::load_mlfun <Lamarr::ganfun>(
      m_resolution_model_lib, m_resolution_upstream_entrypoint
    );
    m_resol [LHCb::Event::Enum::Track::Type::Downstream] = Lamarr::load_mlfun <Lamarr::ganfun>(
      m_resolution_model_lib, m_resolution_downstream_entrypoint
    );
  }

  //////////////////////////////////////////////////////////////////////////////
  if (m_useCovariance)
  {
    // Core of the ML algorithm
    m_covar [LHCb::Event::Enum::Track::Type::Long] = Lamarr::load_mlfun <Lamarr::ganfun>(
      m_covariance_model_lib, m_covariance_long_entrypoint
    );
    m_covar [LHCb::Event::Enum::Track::Type::Upstream] = Lamarr::load_mlfun <Lamarr::ganfun>(
      m_covariance_model_lib, m_covariance_upstream_entrypoint
    );
    m_covar [LHCb::Event::Enum::Track::Type::Downstream] = Lamarr::load_mlfun <Lamarr::ganfun>(
      m_covariance_model_lib, m_covariance_downstream_entrypoint
    );
  }

  //   targetVars:
  //     0- reco_x - x_ClosestToBeam
  //     1- reco_y - y_ClosestToBeam
  //     2- reco_tx - tx_ClosestToBeam
  //     3- reco_ty - ty_ClosestToBeam
  //     4- reco_p - p_ClosestToBeam
  //     5- likelihood
  //     6- chi2PerDoF
  //     7- nDoF
  //     8- ghostProb

  
  //m_output_file = TFile::Open (TString::Format("debug_output_%03d.root", int(rand()%1000)), "RECREATE");
  //m_output_tree = new TNtuple ("original_space", "original_space", "x:y:tx:ty:logp:dx:dy:dtx:dty:dp:likelihood:chi2PerDof:nDoF:ghostProb");
  //m_output_tree_Xprep = new TNtuple ("prepX", "prepX", "x:y:tx:ty:logp:tracktype");
  //m_output_tree_Yprep = new TNtuple ("prepY", "prepY", "dx:dy:dtx:dty:dp:likelihood:chi2PerDof:nDoF:ghostProb");
  //
  ////////////////////////////////////////////////////////////////////////////////
  // Profiler configuration
  m_profiler.enable (m_enable_profiler);

  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode LamarrPropagator::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // convert hep mc -> Delphes
  LHCb::HepMCEvents* generationEvents = 
    get<LHCb::HepMCEvents>(m_generationLocation);
  debug()<<"got generation Events TES locations"<<endmsg;

  pp2mcTable = new TablePP2MC(1000);
  put(pp2mcTable,m_cppRelationsLocation.value());
  //Get TES container
  m_particleContainer = getOrCreate<LHCb::MCParticles,LHCb::MCParticles>(m_generatedParticles);
  m_verticesContainer = getOrCreate<LHCb::MCVertices,LHCb::MCVertices>(m_generatedVertices);
  

  m_trackContainer = getOrCreate<LHCb::Tracks,LHCb::Tracks>(m_tracks);

  m_chargedProtoContainer = getOrCreate<LHCb::ProtoParticles,LHCb::ProtoParticles>( m_chargedProtoParticles );
  m_neutralProtoContainer = getOrCreate<LHCb::ProtoParticles,LHCb::ProtoParticles>( m_neutralProtoParticles );
  m_richPIDContainer = getOrCreate<LHCb::RichPIDs,LHCb::RichPIDs>( m_richPIDs );
  m_muonPIDContainer = getOrCreate<LHCb::MuonPIDs,LHCb::MuonPIDs>( m_muonPIDs );

  m_barcodes.clear();

  mcHeader = get<LHCb::MCHeader>(m_mcHeader);
  debug()<<"getOrCreate TES locations"<<endmsg;

  std::vector<int>generated_PV_barcodes;  
  //loop over all HepMC Events
  
    
  for(LHCb::HepMCEvents::const_iterator genEvent = generationEvents->begin(); 
      generationEvents->end() != genEvent; ++genEvent) 
  {    
    const int eventId = genEvent - generationEvents->begin();

    HepMC::GenEvent* ev = (*genEvent)->pGenEvt();
    //put pointer to PV
    //stolen from GenerationToSimulation
    HepMC::GenVertex* genPV = primaryVertex(ev);    
    if(nullptr==genPV){//need a check, otherwise we have a seg fault
      Warning("Failed to find a PV in the event!!!").ignore();
      return StatusCode::SUCCESS;
    }
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Found Primary Vertex"<<endmsg;

    // Create and add the primary MCVertex.
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Convert PV"<<genPV->barcode()<<endmsg;
    auto primaryVtx = getOrCreateMCVertex ( genPV, eventId ); 

    // Set LHCb Event properties of the new primary vertex
    primaryVtx->setType(LHCb::MCVertex::ppCollision);
    mcHeader->addToPrimaryVertices(primaryVtx.get());

    if ( msgLevel(MSG::DEBUG) ) debug()<<"got generated PV "<<genPV->barcode()<<endmsg;
    generated_PV_barcodes.push_back(genPV->barcode());

    HepMC::GenParticle* beam1 = ev->beam_particles().first;
    HepMC::GenParticle* beam2 = ev->beam_particles().second;

    if ( ((beam1) && (beam1->production_vertex() != 0)) || 
         ((beam2) && (beam2->production_vertex() != 0)) )
    {
      error() << "Unexpectedly, a beam particle has production vertex" << endmsg;
      return StatusCode::FAILURE;
    }

    StatusCode sc;
    if (beam1)
    {
      sc = processParticle (beam1,  primaryVtx, eventId);
      if (!sc) return sc;
    }

    if (beam2)
    {
      sc = processParticle (beam2,  primaryVtx, eventId); 
      if (!sc) return sc;
    }

    if (beam1 == nullptr && beam2 == nullptr) // Generator without beams (e.g. PGun)
      for (auto primaryParticle  = genPV->particles_begin ( HepMC::children );
                primaryParticle != genPV->particles_end   ( HepMC::children );
                ++ primaryParticle )
      {
        StatusCode sc = processParticle(*primaryParticle, primaryVtx, eventId);

        if (!sc) 
          return sc;
      }

    // primaryVtx is owned by TES 
    primaryVtx.release(); 
  }

  counter ("Unique barcodes") += m_barcodes.size();

  //// Some TES statistics 
  counter ("MCP2PP Relations") += pp2mcTable->relations().size();
  counter ( m_generatedParticles ) += m_particleContainer->size(); 
  counter ( m_generatedVertices  ) += m_verticesContainer->size(); 
  counter ( m_tracks ) += m_trackContainer->size(); 
  counter ( m_chargedProtoParticles ) += m_chargedProtoContainer->size(); 
  counter ( m_neutralProtoParticles) += m_neutralProtoContainer->size(); 
  counter ( m_richPIDs ) += m_richPIDContainer->size(); 
  counter ( m_muonPIDs ) += m_muonPIDContainer->size(); 

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode LamarrPropagator::finalize() {

  //m_output_file->cd();
  //m_output_tree->Write();
  //m_output_tree_Xprep->Write();
  //m_output_tree_Yprep->Write();
  //m_output_file->Close();

  if (m_enable_profiler)
  {
    std::ofstream profile_file;
    profile_file.open(name() + ".profile.json");
    profile_file << m_profiler.json_dump();
    profile_file.close();
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  return GaudiAlgorithm::finalize();  // must be called after all other actions
}


//=============================================================================
//  getOrCreateMCVertex 
//  -
//  Find the id of the prodVertex within the container or create it
//=============================================================================
std::unique_ptr<LHCb::MCVertex> LamarrPropagator::getOrCreateMCVertex ( 
    const HepMC::GenVertex *prodVertex,
    const int eventId
    )
{
  const long int key = hepmc_uid(prodVertex, eventId);
  if ( msgLevel(MSG::DEBUG) ) debug() << "Requested a vertex for key " << key <<endmsg;

  auto storedVtx = m_verticesContainer->object ( key );
  if (storedVtx != nullptr) return std::unique_ptr <LHCb::MCVertex>( storedVtx ); 

  if ( msgLevel(MSG::VERBOSE) ) 
    verbose() << "Vertex keyed " << key << " is not in TES, yet." <<endmsg;

  counter("MC vertices")++; 
  
  // Creates the new vertex
  auto newVtx = std::make_unique <LHCb::MCVertex> (); 

  // Copies properties from the HepMC::Vertex 
  newVtx->setPosition(Gaudi::XYZPoint(prodVertex->point3d()));
  newVtx->setTime(prodVertex->position().t());

  // Store the newly created vertex in TES 
  m_verticesContainer->insert(newVtx.release(), key);

  // Tries again to read from TES to ensure proper ownership 
  return getOrCreateMCVertex ( prodVertex, eventId ); 
}


//=============================================================================
//  processParticle
//=============================================================================
StatusCode LamarrPropagator::processParticle ( 
      HepMC::GenParticle*               genPart, 
      std::unique_ptr<LHCb::MCVertex>&  originVtx,
      const int eventId, 
      bool fromSignal
    )
{
  // Ensure each particle is processed at most once
  if (std::find(m_barcodes.begin(), m_barcodes.end(), hepmc_uid(genPart, eventId)) == m_barcodes.end())
    m_barcodes.push_back(hepmc_uid(genPart, eventId));
  else
    return StatusCode::SUCCESS;


  // Remove gluons and other irrelevant generator-only particles 
  const bool to_keep = keep(genPart);
  Gaudi::LorentzVector mom(genPart->momentum());
  LHCb::ParticleID pid(genPart->pdg_id());
  auto status = genPart->status();
  fromSignal |= (LHCb::HepMCEvent::SignalInLabFrame == status);

  HepMC::GenVertex* genEndVtx = genPart->end_vertex(); 

  const Long64_t key = hepmc_uid(genPart, eventId);
  if ( msgLevel(MSG::DEBUG) ) 
    debug() << "Converting particle " << genPart->barcode() 
            << " to key: " << key 
            << " (PID: " << genPart->pdg_id() << ", keep: " <<  to_keep << ")" <<endmsg;

  // Process the daughter particles iteratively  

  if (to_keep == false)
  {
    if (genEndVtx)
    {
      for (auto daughter  = genEndVtx->particles_begin ( HepMC::children );
                daughter != genEndVtx->particles_end   ( HepMC::children );
                ++ daughter )
        if (!processParticle ( *daughter, originVtx, eventId, fromSignal))
          error() << "Failing daughter processing" << endmsg; 
    }
    else 
      counter ( "Discarded intermediate state" ) ++; 
  }
  else  // Creates the LHCb::MCParticle 
  {
    auto mcPart = std::make_unique<LHCb::MCParticle>();  

    //// Copies properties from genPart to mcPart 
    // - Momentum 
    mcPart -> setMomentum(mom);  
    if ( msgLevel(MSG::VERBOSE) ) verbose() << " - Momentum: " << mom <<endmsg;
    
    // - PID 
    mcPart -> setParticleID(pid);
    if ( msgLevel(MSG::VERBOSE) ) verbose() << " - PID: " << pid.pid() <<endmsg;

    // - Production vertex 
    originVtx->addToProducts ( mcPart.get() ); 
    mcPart->setOriginVertex ( originVtx.get() ); 

    // - the fromSignal flag  (whether this particle is part of signal) 
    mcPart -> setFromSignal(fromSignal);

  //// The following behaviour is tuned according to particle's nature: 
  //      charged-stable, neutral-stable or decaying 
  //


  //////////////////////////////////////////////////////////////////////////////
  // DECAYING PARTICLE 
    if ( genEndVtx ) 
    {
      counter ( "Generated Decaying Particles" ) ++; 
      // Gets the decay vertex 
      if ( msgLevel(MSG::DEBUG) ) debug() << "Create decay vertex" << endmsg; 
      auto endVertex = getOrCreateMCVertex ( genEndVtx, eventId ) ; 

      // Check if the particle has oscillated.
      const HepMC::GenParticle* oscPart = hasOscillated(genPart);

      // Define and set the type of the MCVertex 
      if (oscPart) 
        endVertex->setType( LHCb::MCVertex::OscillatedAndDecay);
      else if (( 4 == pid.abspid()) || (5 == pid.abspid()))
        endVertex->setType( LHCb::MCVertex::StringFragmentation);
      else
        endVertex -> setType( LHCb::MCVertex::DecayVertex);

      // Add the decay vertex as end vertex of genPart 
      mcPart -> addToEndVertices(endVertex.get());

      for (auto daughter  = genEndVtx->particles_begin ( HepMC::children );
                daughter != genEndVtx->particles_end   ( HepMC::children );
                ++ daughter )
        if (!processParticle (*daughter, endVertex, eventId, fromSignal))
          error() << "Failing daughter processing" << endmsg; 
      
      // endVertex is owned by TES 
      endVertex.release(); 
    }

    //////////////////////////////////////////////////////////////////////////////
    // STABLE, CHARGED PARTICLE 
    else if ( isChargedAndStable ( mcPart->particleID() ) ) 
    {
      counter ( "Generated Stable&Charged Particles" ) ++; 
      m_profiler.start("PropagateAndConvert");
      PropagateAndConvert ( mcPart, key );
      m_profiler.stop();
    }
    //////////////////////////////////////////////////////////////////////////////
    // STABLE, NEUTRAL PARTICLE
    else if ( pid.threeCharge() == 0 )
      counter ( "Generated Stable&Neutrals Particles" ) ++; 
    //////////////////////////////////////////////////////////////////////////////
    // UNEXPECTED, IGNORED PARTICLE
    else 
    {
      counter ( "IgnoredStable" ) ++; 
      if ( msgLevel(MSG::VERBOSE) ) 
        verbose() << " Particle #" << mcPart->key() << " (PID: " << pid.pid() << ")"
                  << " is not propagated nor stored as LHCb::MCParticle" << endmsg; 
    }

    m_particleContainer->insert(mcPart.release(), key);
  }

  return StatusCode::SUCCESS; 
}
 
//=============================================================================
//   primaryVertex 
//
//   Finds the primary vertex in the event trying three different methods 
//=============================================================================
HepMC::GenVertex* LamarrPropagator::primaryVertex
(const HepMC::GenEvent* genEvent) const {
  // First method, get the beam particle and use the decay vertex if it exists.
  HepMC::GenParticle* P;
  HepMC::GenVertex*   V;

  if (genEvent->valid_beam_particles()) {
    P = genEvent->beam_particles().first;
    V = P->end_vertex();

    if (V != nullptr) 
      return V;
    else 
      error() << "The beam particles have no end vertex!" << endmsg;
  } 
  
  // Second method, use the signal vertex stored in HepMC.
  V = genEvent->signal_process_vertex();
  if (V != nullptr)
    return V;
  else
    error() << "Signal vertex not defined in HepMC" << endmsg;

  for (int tentative_barcode = 1; tentative_barcode < 1000; ++tentative_barcode)
  {
    P = genEvent->barcode_to_particle(tentative_barcode);
    if (P == nullptr) continue; 
    V = P->production_vertex();
    if (V != nullptr) 
    {
      warning() << "PV obtained from the particle with the lowest barcode: " << tentative_barcode << endmsg;
      return V;
    }
  }

  return nullptr;
}



//=============================================================================
// Main Algorithm to Propagate MCParticles and Convert them into reconstruted objects
//=============================================================================
void LamarrPropagator::PropagateAndConvert(const std::unique_ptr<LHCb::MCParticle>& particle, Long64_t key)
{
  // Creates the track object and it inialize it to Long
  auto track = std::make_unique<LHCb::Track>(key);      
  track->setType(LHCb::Event::Enum::Track::Type::Long);

  // If simulating the reconstruction procedure,
  //  - compute the acceptance
  //  - compute the efficiency
  //  - assign the type on the basis of the efficiency
  if (m_useEfficiency)
  {
    counter ("Propagated") ++;
    m_profiler.start("Acceptance");
    const double eta = particle->momentum().eta();

    bool accepted = (eta > 1) && (eta < 7) && (apply_acceptance(particle));
    m_profiler.stop();
    if (!accepted) return; 
    counter ("InAcceptance") ++;
  
    m_profiler.start("Efficiency");
    LHCb::Event::Enum::Track::Type track_type = apply_efficiency(particle);
    m_profiler.stop();
    if (track_type == LHCb::Event::Enum::Track::Type::Unknown) return;

    track->setType(track_type);
  }

  // Retrieve the states used in the ML model 
  m_profiler.start("Propagate - ClosestToBeam");
  auto closest_to_beam = MCPropagationToState(particle,
                                              map_ZStates[map_Locations["ClosestToBeam"]],
                                              map_Locations["ClosestToBeam"]);
  m_profiler.stop();
  m_profiler.start("Resolution");
  if (m_useResolution)
    apply_resolution (closest_to_beam, track, particle);
  m_profiler.stop();

  m_profiler.start("Covariance");
  if (m_useCovariance)
    apply_covariance (closest_to_beam, track, particle);
  m_profiler.stop();

  track->addToStates (*closest_to_beam.release());
    
  
  // Uses the standard techniques to process all the other states
  for(auto& stateName : m_chargedStates.value())
  {
    if (stateName == "ClosestToBeam")
      continue;
    
    m_profiler.start("Propagate - Other states");
    auto state = MCPropagationToState(particle,
                                      map_ZStates[map_Locations[stateName]],
                                      map_Locations[stateName]);
    m_profiler.stop();

    if (m_useResolution) 
      ResolutionAtZ(state);

    if (m_useCovariance)
      CovarianceAtZ(state);        

    track->addToStates(*state.release());
  }


  //Add Track additional information
  // AddTrackInfo(track);      
  track->setFitStatus(LHCb::Event::Enum::Track::FitStatus::Fitted);
  track->setHistory(LHCb::Event::Enum::Track::History::TrackIdealPR);
  //track->setType(LHCb::Track::Long);
  

  //Fill PID basic info
  auto richPID = std::make_unique<LHCb::RichPID>();
  auto muonPID = std::make_unique<LHCb::MuonPID>();
  if ( msgLevel(MSG::DEBUG) )  debug()<<"Initialized richPID and muonPID "<<endmsg;
  
  const std::vector< float > llValue{-1000.0,0.0,0.0,0.0,0.0,0.0,0.0};
  
  muonPID->setIDTrack(track.get());
  richPID->setTrack(track.get());
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Setting RichPID default"<<endmsg;
  
//  switch(abs(particle->particleID().pid()))
//  {
//  case(11):
//    richPID->setBestParticleID(Rich::Electron);
//    break;
//  case(13):
//    richPID->setBestParticleID(Rich::Muon);
//    break;
//  case(211):
//    richPID->setBestParticleID(Rich::Pion);
//    break;
//  case(321):
//    richPID->setBestParticleID(Rich::Kaon);
//    break;
//  case(2212):
//    richPID->setBestParticleID(Rich::Proton);
//    break;
//  default:
//    richPID->setBestParticleID(Rich::Unknown);
//  }

  //fill Protos
  auto proto = std::make_unique<LHCb::ProtoParticle>();
      
  proto->addInfo(LHCb::ProtoParticle::NoPID , 1);
  //proto->addInfo(LHCb::ProtoParticle::TrackChi2PerDof,1);      
  proto->setTrack(track.get());

  proto->setRichPID ( richPID.get() ); 
  proto->setMuonPID ( muonPID.get() ); 
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Adding to the containers key " << key <<endmsg;
  pp2mcTable->i_push(proto.get(),particle.get(), 1.);
  pp2mcTable->i_sort() ;

  debug()<<"Put pp2mcTable and release"<<endmsg;
  // debug()<<"Done pp2mcTable and release"<<endmsg;
  //Inserting to containers
  if ( msgLevel(MSG::VERBOSE) ) debug()<<" - Track "<<endmsg;
  m_trackContainer->insert(track.release(),key);
  if ( msgLevel(MSG::VERBOSE) ) debug()<<" - Rich "<<endmsg;
  m_richPIDContainer->insert(richPID.release(),key);    
  if ( msgLevel(MSG::VERBOSE) ) debug()<<" - Muon "<<endmsg;
  m_muonPIDContainer->insert(muonPID.release(),key);
  if ( msgLevel(MSG::VERBOSE) ) debug()<<" - Proto "<<endmsg;
  m_chargedProtoContainer->insert(proto.release(),key);
  counter ("Reconstructed Tracks")++; 
 
      
  if (msgLevel(MSG::DEBUG)) debug()<<"Added to the relative containers"<<endmsg;
}


//=============================================================================
std::unique_ptr<LHCb::State> LamarrPropagator::MCPropagationToState(
    const std::unique_ptr<LHCb::MCParticle>& mcPart,
    double zProp,LHCb::State::Location location
    )
{
  double x = mcPart->originVertex()->position().X();
  double y = mcPart->originVertex()->position().Y();
  double z = mcPart->originVertex()->position().Z();
  //double t = mcPart->originVertex()->time();
  
  double px = mcPart->momentum().Px();
  double py = mcPart->momentum().Py();
  double pz = mcPart->momentum().Pz();
  double p = mcPart->momentum().P();
  // double e = mcPart->momentum().E();
  
  double tx = px/pz;
  double ty = py/pz;
  double q = mcPart->particleID().threeCharge()/3.0;
  double qOverP = q/p;
  // double c_light = 2.99792458E11;  
  // double gammam = e / (c_light*c_light);

  double zCenter = computeZMagnetCenter(qOverP);
  double ptKick = 1.0e-9 < fabs(q) ? m_ptKick.value() : 0.;

  if(((q > 0) && (m_magnetPolarity.value() == "up")) || ((q < 0) && (m_magnetPolarity.value() == "down"))) ptKick = -ptKick;

  debug()<<"MCPROPAGATIONTOSTATE <== Propagating particle from z = "<<z<<" to zProp: "<<zProp<<"\t zCenter "<<zCenter<<" relative to qOverP of "<<qOverP<<endmsg;

  auto state = std::make_unique<LHCb::State>();    
  state->setState(x,y,z,tx,ty,qOverP);
  state->setLocation(location);
  
  if((zProp >= z && zProp<zCenter) || (zProp >= z && q==0))
  {
    //double tz = gammam / (pz) * (-z + zProp);
    double xk = x + tx* (zProp - z);
    double yk = y + ty * (zProp - z);
    state->setState(xk,yk,zProp,tx,ty,qOverP);
    return state;  
  }

  if(zProp >= z && zProp>=zCenter && q!=0)
  {    
    //double tz = gammam / (pz) * (-z + zCenter);
    double xk = x + tx * (zCenter - z);
    double yk = y + ty * (zCenter - z);
    px += ptKick;    
    double px2 = px*px;
    double py2 = py*py;
    double pz2 = TMath::Power(p,2) - py2 - px2;
    if(pz2>0){pz = TMath::Sqrt(pz2);}
    
    tx = px / pz;    
    ty = py / pz;
    //tz += gammam / (pz) * (zProp - zCenter);
    xk += tx * (zProp - zCenter);    
    yk += ty * (zProp - zCenter);
    state->setState(xk,yk,zProp,tx,ty,qOverP);
    return state;  
  }
  
  debug()<<"MCPROPAGATIONTOSTATE: Particle not propagated, return state at z = "<<z<<endmsg;
  if(z>StateParameters::ZEndVelo) state->setLocation(LHCb::State::Location::LocationUnknown);
  
  return state;  
}

//==============================================================================
double LamarrPropagator::computeZMagnetCenter(double qOverP)
{
  if(m_formulaCenterMagnet.value()=="") return m_valueCenterMagnet.value();
  auto zMagnetFormula = std::make_unique<TFormula>("zMagnetFormula",m_formulaCenterMagnet.value().c_str(),1);
  
  double zCenter = zMagnetFormula->Eval(qOverP);
  
  return zCenter;  
}


//==============================================================================
bool LamarrPropagator::apply_acceptance(const std::unique_ptr<LHCb::MCParticle>& particle)
{
  // computes the probability of the particle being in acceptance 
  float accProb [1];
  
  // Retrieve the states used in the ML model 
  auto ctb = MCPropagationToState(particle,
                                      map_ZStates[map_Locations["ClosestToBeam"]],
                                      map_Locations["ClosestToBeam"]);

  // From the documentation of the acceptance ML model one gets: 
  // discrVars:
  //   - x_ClosestToBeam
  //     - y_ClosestToBeam
  //     - tx_ClosestToBeam
  //     - ty_ClosestToBeam
  //     - p_ClosestToBeam
  //     - eta_ClosestToBeam

  float input[] = {
    static_cast<float> (ctb->x()),
    static_cast<float> (ctb->y()), 
    static_cast<float> (ctb->tx()),
    static_cast<float> (ctb->ty()), 
    static_cast<float> (ctb->p()),
    static_cast<float> (ctb->momentum().eta()),
  };
   
  // Executes the ml model
  m_acceptance_mlfun(accProb, input);

  // Converts the probability into a boolean value through random sampling
  return m_flatDist() < accProb[0]; 
}


 LHCb::Event::Enum::Track::Type LamarrPropagator::apply_efficiency (const std::unique_ptr<LHCb::MCParticle>& particle)
 {
  // computes the probability of the particle being in acceptance 
  float effProb [4];
  
  // Retrieve the states used in the ML model 
  auto ctb = MCPropagationToState(particle,
                                      map_ZStates[map_Locations["ClosestToBeam"]],
                                      map_Locations["ClosestToBeam"]);

  // From the documentation of the acceptance ML model one gets: 
  // discrVars:
  //   - x_ClosestToBeam
  //     - y_ClosestToBeam
  //     - tx_ClosestToBeam
  //     - ty_ClosestToBeam
  //     - p_ClosestToBeam
  //     - eta_ClosestToBeam
  
  float input[] = {
    static_cast<float> (ctb->x()),
    static_cast<float> (ctb->y()), 
    static_cast<float> (ctb->tx()),
    static_cast<float> (ctb->ty()), 
    static_cast<float> (ctb->p()),
    static_cast<float> (ctb->momentum().eta()),
  };
   
  // Executes the ml model
  m_efficiency_mlfun(effProb, input);
  for (int i = 1; i < 4; ++i)
    effProb[i] += effProb[i-1];

  // Converts the probability into a boolean value through random sampling
  double r = m_flatDist();

  if (r < effProb[0])
  {
    counter ("Unreconstructed") ++;
    return LHCb::Event::Enum::Track::Type::Unknown;
  }
  else if (r < effProb[1])
  {
    counter ("Long tracks") ++;
    return LHCb::Event::Enum::Track::Type::Long;
  }
  else if (r < effProb[2])
  {
    counter ("Upstream tracks") ++;
    return LHCb::Event::Enum::Track::Type::Upstream;
  }  
  else if (r < effProb[3])
  {
    counter ("Downstream tracks") ++;
    return LHCb::Event::Enum::Track::Type::Downstream;
  }

  error() 
    << "Unexpectedly non-normalized efficiency: " 
    << effProb[3] << " != 1" 
    << endmsg;
  
  return LHCb::Event::Enum::Track::Type::Unknown;

 }

 void LamarrPropagator::apply_resolution (
     std::unique_ptr<LHCb::State>& state,
     std::unique_ptr<LHCb::Track>& track,
     const std::unique_ptr<LHCb::MCParticle>& 
   )
 {
  const auto q = state->qOverP() * state->p();
  // resolutionGAN:
  //   discrVars:
  //     - x_ClosestToBeam
  //     - y_ClosestToBeam
  //     - tx_ClosestToBeam
  //     - ty_ClosestToBeam
  //     - log(p_ClosestToBeam)/log(10)

  //   targetVars:
  //     0- reco_x - x_ClosestToBeam
  //     1- reco_y - y_ClosestToBeam
  //     2- reco_tx - tx_ClosestToBeam
  //     3- reco_ty - ty_ClosestToBeam
  //     4- reco_p - p_ClosestToBeam
  //     5- likelihood
  //     6- chi2PerDoF
  //     7- nDoF
  //     8- ghostProb

  // const int nX = 5;
  const int nOut = 9;
  const int nRandomNodes = m_resolution_random_nodes;

  float input[] = {
    static_cast<float> (state->x()), 
    static_cast<float> (state->y()), 
    static_cast<float> (state->tx()), 
    static_cast<float> (state->ty()), 
    static_cast<float> (log10(state->p()))
    };
  float output[nOut];
  std::vector<float> random (nRandomNodes);
  for (int i = 0; i < nRandomNodes; ++i) random[i] = m_gaussDist();

  m_resol [track->type()]  (output, input, random.data());

  state->setX (state->x() + output[0]);
  state->setY (state->y() + output[1]);
  state->setTx (state->tx() + output[2]);
  state->setTy (state->ty() + output[3]);
  state->setQOverP( q / (state->p() + output[4]));

  track->setLikelihood (output[5]);
  track->setChi2AndDoF (output[6]*output[7], int(output[7]));
  track->setGhostProbability (output[8]);

}


void LamarrPropagator::apply_covariance (
     std::unique_ptr<LHCb::State>& state,
     std::unique_ptr<LHCb::Track>& track,
     const std::unique_ptr<LHCb::MCParticle>& 
   )
 {
  // const auto q = state->qOverP() * state->p();

  // discrVars:
  //   - x_ClosestToBeam
  //   - y_ClosestToBeam
  //   - tx_ClosestToBeam
  //   - ty_ClosestToBeam
  //   - log(p_ClosestToBeam)/log(10)
  //   - likelihood
  //   - nDoF

  // targetVars:
  //  0- log(cov_ClosestToBeam_0_0)
  //  1- cov_ClosestToBeam_0_1
  //  2- cov_ClosestToBeam_0_2
  //  3- cov_ClosestToBeam_0_3
  //  4- cov_ClosestToBeam_0_4
  //  5- log(cov_ClosestToBeam_1_1)
  //  6- cov_ClosestToBeam_1_2
  //  7- cov_ClosestToBeam_1_3
  //  8- cov_ClosestToBeam_1_4
  //  9- log(cov_ClosestToBeam_2_2)
  // 10- cov_ClosestToBeam_2_3
  // 11- cov_ClosestToBeam_2_4
  // 12- log(cov_ClosestToBeam_3_3)
  // 13- cov_ClosestToBeam_3_4
  // 14- log(cov_ClosestToBeam_4_4)

  // const int nX = 7;
  const int nOut = 15;
  const int nRandomNodes = m_covariance_random_nodes;

  float input[] = {
    static_cast<float> (state->x()),
    static_cast<float> (state->y()), 
    static_cast<float> (state->tx()), 
    static_cast<float> (state->ty()), 
    static_cast<float> (log10(state->p())), 
    static_cast<float> (track->likelihood()),
    static_cast<float> (track->nDoF())
    };

  float output[nOut];
  //float random[nRandomNodes];
  std::vector <float> random (nRandomNodes);
  for (int i = 0; i < nRandomNodes; ++i) random[i] = m_gaussDist();

  m_covar [track->type()]  (output, input, random.data());

  Gaudi::TrackSymMatrix cov;

  cov(0, 0) = exp(output[0]);
  cov(0, 1) = output[1];
  cov(0, 2) = output[2];
  cov(0, 3) = output[3];
  cov(0, 4) = output[4];
  cov(1, 1) = exp(output[5]);
  cov(1, 2) = output[6];
  cov(1, 3) = output[7];
  cov(1, 4) = output[8];
  cov(2, 2) = exp(output[9]);
  cov(2, 3) = output[10];
  cov(2, 4) = output[11];
  cov(3, 3) = exp(output[12]);
  cov(3, 4) = output[13];
  cov(4, 4) = exp(output[14]);

  // if ( msgLevel(MSG::VERBOSE) ) 
  // {
    std::stringstream s; 
    for (uint i = 0; i < 5; ++i, s << "\n" ) 
      for (uint j = 0; j < 5; ++j, s << " \t" ) 
        if (i == j)
          s << log(cov(i,j));
        else 
          s << cov (i, j) * 1e12; 

    verbose() << "Cov: \n" << s.str() << endmsg; 
  //}

  state->setCovariance( cov );
 }

void LamarrPropagator::ResolutionAtZ(std::unique_ptr<LHCb::State> &state)
{
  debug()<<"RESOLUTIONATZ: retrieving state info: "<<endmsg;
  
  double tx = state->tx();
  double ty = state->ty();
  double p = state->p();
  double qOverP = state->qOverP();
  double q = qOverP*p;
  auto location = state->location();

  if (location == LHCb::State::LocationUnknown || location == LHCb::State::ClosestToBeam) 
    location = LHCb::State::EndVelo;
  
  double txRMS = m_map_res[3][location]->GetBinContent(m_map_res[3][location]->FindBin(tx,ty,p));
  double tyRMS = m_map_res[4][location]->GetBinContent(m_map_res[4][location]->FindBin(tx,ty,p));
  double pRMS  = m_map_res[5][location]->GetBinContent(m_map_res[5][location]->FindBin(tx,ty,p));

  debug()<<"RESOLUTIONATZ: getting random number from RMS"<<endmsg;
  tx = m_gaussDist()*txRMS + tx;
  ty = m_gaussDist()*tyRMS + ty;  
  p  = m_gaussDist()*pRMS + p;
  
  debug()<<"RESOLUTIONATZ: setting state"<<endmsg;
  state->setTx(tx);
  state->setTy(ty);
  state->setQOverP(q/p);
  debug()<<"RESOLUTIONATZ: done!"<<endmsg;
}



void LamarrPropagator::CovarianceAtZ(std::unique_ptr<LHCb::State> &state)
{
  double oneOverP = 1./state->p();  
  if ( msgLevel(MSG::DEBUG) ) 
    debug() << "COVARIANCEATZ: ==> Adding Info at " <<state->location()<<" with 1/p: "<<oneOverP<< endmsg;  

  Gaudi::TrackSymMatrix cov;
  const auto rnd = m_flatDist();
  for (uint i = 0; i < 5; ++i ) 
    for (uint j = i; j < 5; ++j ) 
      if ( m_covsampler[state->location()][(i<<4)+j] )
      {
        cov(i,j) = m_trackCovarianceScale*m_covsampler[state->location()][(i<<4)+j] -> sample(oneOverP, rnd);
        cov(j,i) = cov(i,j) ;
      }

  if ( msgLevel(MSG::VERBOSE) ) 
  {
    std::stringstream s; 
    for (uint i = 0; i < 5; ++i, s << "\n" ) 
      for (uint j = 0; j < 5; ++j, s << " \t" ) 
        s << cov (i, j) * 1e12; 

    verbose() << "Cov: \n" << s.str(); 
  }

  state->setCovariance( cov );
}



//=============================================================================
// Check if a particle has oscillated.
//=============================================================================
const HepMC::GenParticle* LamarrPropagator::hasOscillated
(const HepMC::GenParticle* P) const 
{
  const HepMC::GenVertex* ev = P->end_vertex();
  if (!ev) return 0 ;
  if (1 != ev->particles_out_size()) return 0;
  const HepMC::GenParticle* D = *(ev->particles_out_const_begin());
  if (!D) return 0 ;
  if (-P->pdg_id() != D->pdg_id()) return 0;
  return D;  
}


//=============================================================================
// Decides if a particle should be kept in MCParticles.
//=============================================================================
bool LamarrPropagator::keep(const HepMC::GenParticle* particle) const{
  // Do not keep beam particles, whatever the pid
  if (!particle->production_vertex()) return false;

  LHCb::ParticleID pid(particle->pdg_id());
  switch (particle -> status()) {
  case LHCb::HepMCEvent::StableInProdGen: return true;
  case LHCb::HepMCEvent::DecayedByDecayGen: return true;
  case LHCb::HepMCEvent::DecayedByDecayGenAndProducedByProdGen: return true;
  case LHCb::HepMCEvent::SignalInLabFrame: return true;
  case LHCb::HepMCEvent::StableInDecayGen: return true;

  // For some processes the resonance has status 3.
  case LHCb::HepMCEvent::DocumentationParticle:
    if (24 == particle->parent_event()->signal_process_id()) {
      if (23 == pid.abspid()) return true;
      else if (25 == pid.abspid()) return true;
    } else if (26 == particle->parent_event()-> signal_process_id()) {
      if (24 == pid.abspid()) return true;
      else if (25 == pid.abspid()) return true;
    } else if (102 == particle->parent_event()->signal_process_id()) {
      if (25 == pid.abspid()) return true;
    } else if (6 == pid.abspid()) return true;
    return false;
  case LHCb::HepMCEvent::Unknown: return false;
  case LHCb::HepMCEvent::DecayedByProdGen: 
    if ( pid.isHadron() ) return true;
    if ( pid.isLepton() ) return true;
    if ( pid.isNucleus() ) return true;
    if ( pid.isDiQuark() ) return false;

    // Store particles of interest.
    switch (pid.abspid()) {
    case LHCb::ParticleID::down   : return false;
    case LHCb::ParticleID::up     : return false;
    case LHCb::ParticleID::strange: return false;
    case LHCb::ParticleID::charm  : return true;
    case LHCb::ParticleID::bottom : return true;
    case LHCb::ParticleID::top    : return false;
    case 21: return false;  // Gluon.
    case 22: return true;   // Photon.
    case 23: // Z0.
      if (24 == particle->parent_event()->signal_process_id()) return false;
      else return true;
    case 24: // W. 
      if (26 == particle->parent_event()->signal_process_id()) return false;
      else return true;
    case 25: // SM Higgs.
      if (24 == particle-> parent_event()-> signal_process_id() || 
	  26 == particle-> parent_event()-> signal_process_id() || 
	  102 == particle-> parent_event()-> signal_process_id()) return false;
      else return true;
    case 32: return true;  // Z'.
    case 33: return true;  // Z''.
    case 34: return true;  // W'.
    case 35: return true;  // CP-even heavy Higgs (H0/H2).
    case 36: return true;  // CP-odd Higgs (A0/H3).
    case 37: return true;  // Charged Higgs (H+).
    // See Table 6 of the Pythia 6 manual (arxiv.org/abs/hep-ph/0603175).
    case 90: return false; // System particle.
    case 91: return false; // Parton system from cluster fragmentation.
    case 92: return false; // Parton system from string fragmentation.
    case 93: return false; // Parton system from independent fragmentation.
    case 94: return false; // Time-like showering system.
    default: return true;
    }
    return true;
  default: return false;
  }
  return false;
}


//=============================================================================
// Decides if a particle should be propagated as a track 
//=============================================================================
bool LamarrPropagator::isChargedAndStable ( const LHCb::ParticleID &partId ) const
{
  const std::vector<int> stableId { 11, 13, 211, 321, 2212, 1000010020 }; 
  const auto testedId = partId.abspid(); 

  for (auto id : stableId)
    if (testedId == static_cast<unsigned int>(id))
      return true;

  return false; 
}
