###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test file for Delphes: reconstructing KS0 candidates
Author: Lucio Anderlini

Runs with:
lb-run \
  --platform=x86_64-centos7-gcc8-opt \
  Bender/latest \
  python BenderScript.py
"""
# ==============================================================================
from glob import glob
import os

import Bender.Main as bnd 
from IPython import embed

import Bender.Main as bnd 
from Bender import MainMC as bndmc

import contextlib
import sqlite3 as sql

from collections import deque


class GraphBuilder:
  def __init__ (self, output_filename):
    self._output_filename = output_filename
    self._db = self._connect(self.output_filename)


  @contextlib.contextmanager
  def get_cursor(self):
    cursor = self._db.cursor()
    yield cursor
    self._db.commit()


  @property
  def output_filename (self):
    return self._output_filename

  @staticmethod
  def _connect (output_filename):
    if os.path.exists(output_filename):
      os.remove(output_filename)

    conn = sql.connect(output_filename)
    conn.execute("""
      CREATE TABLE IF NOT EXISTS particles (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        event INTEGER,
        interaction INTEGER,
        barcode INTEGER,
        pdgid INTEGER,
        originvertex INTEGER,
        endvertex INTEGER,
        category INTEGER,
        status INTEGER
      );
      """)
    conn.commit()

    conn.execute("""
      CREATE TABLE IF NOT EXISTS vertices (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        event INTEGER,
        interaction INTEGER,
        barcode INTEGER,
        n_input INTEGER,
        n_output INTEGER,
        category INTEGER
      );
      """)

    return conn


  def add_particle (self, event, interaction, particle, category='HepMC'):

    with self.get_cursor() as c:
      if category == 'HepMC':
        ov = particle.production_vertex()
        ev = particle.end_vertex()
        c.execute("""
          INSERT INTO particles (event, interaction, barcode, pdgid, originvertex, endvertex, category, status) 
          VALUES (?, ?, ?, ?, ?, ?, ?, ?);
        """, 
          (event, interaction, particle.barcode(), particle.pdg_id(), ov.barcode() if ov else 0, ev.barcode() if ev else 0, category, particle.status())
        )
      elif category == 'MC':
        ov = particle.originVertex()
        ev = particle.decayVertex()
        c.execute("""
          INSERT INTO particles (event, interaction, barcode, pdgid, originvertex, endvertex, category, status) 
          VALUES (?, ?, ?, ?, ?, ?, ?, ?);
        """, 
          (event, interaction, particle.key(), particle.particleID().pid(), ov.key() if ov else 0, ev.key() if ev else 0, category, 889 if particle.fromSignal() else 0)
        )

  def add_vertex (self, event, interaction, vertex, category='HepMC'):
    if category == 'HepMC':
      with self.get_cursor() as c:
        c.execute("""
          INSERT OR IGNORE INTO vertices (event, interaction, barcode, n_input, n_output, category) 
          VALUES (?, ?, ?, ?, ?, ?);
        """, 
          (event, interaction, vertex.barcode(), vertex.particles_in_size(), vertex.particles_out_size(), category)
        )
    elif category == 'HepMC':
        c.execute("""
          INSERT OR IGNORE INTO vertices (event, interaction, barcode, n_input, n_output, category) 
          VALUES (?, ?, ?, ?, ?, ?);
        """, 
          (event, interaction, vertex.key(), 1, len(part.decayVertex().products()), category)
        )


  def main_loop (self, evtmax=10000):
    for iEvent in range(evtmax):
      bnd.run(1)
      events = bnd.get("/Event/Gen/HepMCEvents")
      if not events: continue 
      #embed()
      for iInteraction, interaction in enumerate(events):
        for particle in interaction.particles(bndmc.GALL):
          self.add_particle(iEvent, iInteraction, particle)
          if particle.end_vertex():
            self.add_vertex(iEvent, iInteraction, particle.end_vertex())


      parts = bnd.get("/Event/MC/Particles")
      for part in parts:
          self.add_particle(iEvent, 0, part, category='MC')
          if part.decayVertex():
            self.add_vertex(iEvent, 0, part.decayVertex().key(), category='MC')

      #embed()
    


def main():
  from argparse import ArgumentParser

  parser = ArgumentParser()
  parser.add_argument("inputfile", type=str, default=10)
  parser.add_argument("-n", "--num-events", type=int, default=10)
  args = parser.parse_args()

  bnd.setData  (args.inputfile, [], False)
  builder = GraphBuilder("output.db")
  builder.main_loop(args.num_events)


if __name__ == '__main__':
  main()








