###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##################################################
# simple pi0 particle maker using Delphes as input
# Adam Davis, last update 23/4/18
##################################################

#setup environment
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from PhysSelPython.Wrappers import Selection, SelectionSequence
from Configurables import DaVinci, DataOnDemandSvc,DecayTreeTuple,FilterDesktop, CombineParticles, NoPIDsParticleMaker
from PhysSelPython.Wrappers import Selection, DataOnDemand, SelectionSequence
from StandardParticles import StdAllNoPIDsPions
from CommonParticles.Utils import *



def MakeParticles(name,
                  particle):

    particleMaker =  NoPIDsParticleMaker( "PartMaker%s"%name ,  Particle = particle )
    particleMaker.Input = "/Event/Rec/ProtoP/Charged"
    particleMakerOutputLevel = INFO
    DataOnDemandSvc().AlgMap.update( {
        "/Event/Phys/" + particleMaker.name() + '/Particles' : particleMaker.getFullName(),
        "/Event/Phys/" + particleMaker.name() + '/Vertices'  : particleMaker.getFullName()
        } )
    
    AllParticles = Selection((name),
                             Algorithm = particleMaker,
                             InputDataSetter=None)
    return AllParticles
### filter on the IP of the velo tracks
#    return Selection("PPto%s"%name,
#                     Algorithm = FilterDesktop(name+"ForMeFilterTrack%s"%name,
#                                               Code="ALL" ),
#                     RequiredSelections = [AllParticles])

pions = MakeParticles("Pions","pion")
selseqPions=SelectionSequence("selseqPions",TopSelection=pions)
seqPions=selseqPions.sequence()


#get photons protoparticles, make them into particles.
#PhotonMaker takes directly from LHCb::ProtoParticleLocation::Neutrals
locations={}
#algorithm =  NoPIDsParticleMaker ( 'StdAllNoPIDsPions'  ,
#                                   DecayDescriptor = 'Pion' ,
#                                   Particle = 'pion' )
#selector = trackSelector ( algorithm )
#locations = updateDoD ( algorithm )

#pions = DataOnDemand(Location = 'Phys/StdAllNoPIDsPions')
#matchB02PiPi    = "(mcMatch('[B0 ==> pi+ pi-]CC'))"
#matchB02PiPi    = "(M<6.0*GeV) & (M>5.0*GeV)"
matchB02PiPi    = "ALL"


StdMCB02PiPi= CombineParticles ('StdMCB02PiPi')
StdMCB02PiPi.OutputLevel = DEBUG
StdMCB02PiPi.Inputs = [ 'Phys/Pions/Particles' ]
StdMCB02PiPi.DecayDescriptor = "[B0 -> pi+ pi-]cc"
StdMCB02PiPi.DaughtersCuts = {"pi+": "ALL"}
StdMCB02PiPi.MotherCut =  matchB02PiPi
StdMCB02PiPi.Preambulo = [
        "from LoKiPhysMC.decorators import *",
            "from PartProp.Nodes import CC" ]

#locations.update(updateDoD ( StdMCB02PiPi ))

#input pi0s are set, make a decay tree tuple
myb0Sel = Selection("myb0Sel",
                    Algorithm = StdMCB02PiPi,
                    RequiredSelections = [pions])
#                    RequiredSelections = [StdAllNoPIDsPions])


myb0SelSeq = SelectionSequence("myb0SelSeq",TopSelection = myb0Sel)
SeqBhh = myb0SelSeq.sequence()

#myb0Sel.OutputLevel = VERBOSE
tuple = DecayTreeTuple("B0tree")
#tuple.OutputLevel=DEBUG
tuple.Inputs = [myb0Sel.outputLocation()]
tuple.Decay = '[(B0 -> ^pi+ ^pi-)]CC'
#tuple.Decay = '[(KS0 -> ^pi+ ^pi-)]CC'
tuple.ToolList+= ["TupleToolKinematic",
                  "TupleToolGeometry",
                  "TupleToolEventInfo",
                  "TupleToolTrackInfo",
                  "TupleToolPrimaries",
                  "TupleToolEventInfo",
                  "TupleToolPropertime",
                  "LoKi::Hybrid::TupleTool/LoKiTupleBhh"
                  ]
from Configurables import CondDB
CondDB().IgnoreHeartBeat = True


from Configurables import DaVinci

DaVinci().PrintFreq = 10
DaVinci().TupleFile = "B0_DV_Tuples_from_Lamarr.root"

#DaVinci().UserAlgorithms=[StdMCB02PiPi,
#                          #fltr,
#                          myb0SelSeq]
DaVinci().UserAlgorithms=[seqPions, SeqBhh, tuple]
                         
                          
#DaVinci().appendToMainSequence([SeqBhh, tuple])
DaVinci().InputType = 'DST'
DaVinci().Lumi = False
DaVinci().Simulation = True
DaVinci().DataType = "2012"
DaVinci().EvtMax = -1
DaVinci().CondDBtag = "sim-20170721-2-vc-mu100"
DaVinci().DDDBtag = "dddb-20170721-3"
from GaudiConf import IOHelper
IOHelper().inputFiles(['../options/Gauss_11102013.xsim'],clear=True)
#IOHelper().inputFiles(['../options/Gauss-11102013-1000ev-20190716.gen'],clear=True)
#IOHelper().inputFiles(['/afs/cern.ch/work/b/bsiddi/lb-dev-Delphes-TensorFlow/Gauss-Check/Sim/LbDelphes/options/Gauss-11102013-50ev-20190716.gen'],clear=True)
#IOHelper().inputFiles(['/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00043311/0000/00043311_00000001_2.AllStreams.dst'],clear=True)

#IOHelper().inputFiles(['/eos/lhcb/grid/prod/lhcb/MC/2012/XDST/00073056/0000/00073056_00000002_4.xdst'],clear=True)
#IOHelper().inputFiles(['/eos/lhcb/grid/prod/lhcb/MC/2012/XDST/00073056/0000/00073056_00000002_4.xdst'],clear=True)
