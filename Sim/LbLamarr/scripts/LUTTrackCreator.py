#!/opt/local/bin/python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys, os

if (len(sys.argv)<2):
    print("Specify a file please")
    sys.exit()

import ROOT as R
import pandas
import numpy as np
import root_pandas as rp


def LUTCreatorList(histos, fout):
    nbins = histos[0].GetNbinsX()
    fOut = open(fout,"w")
    for ibin in range(1,nbins):
        xmin = histos[0].GetBinLowEdge(ibin)
        xmax = histos[0].GetBinLowEdge(ibin) + histos[0].GetBinWidth(ibin)
        entry = "{0}\t{1}".format(xmin,xmax)
        for i in histos:
            mean = i.GetBinContent(ibin)
            err  = i.GetBinError(ibin)
            entry += "\t{0}\t{1}".format(mean,err)            
        entry += "\n"
        fOut.write(entry)
    fOut.close()

extraInfoAll = [
    "ghostProb",
    "chi2PerDoF",
    "nDoF",
    "likelihood"
]

extraInfos = []    
if(len(sys.argv)>2):
    if(sys.argv[2] == "all"):
        extraInfos = extraInfoAll
    else:
        for i in range(2,len(sys.argv)):
            extraInfos.append(sys.argv[i])


printFreq = 1000
tr_recoed  = rp.read_root(sys.argv[1],"TrackEffRes/TrackTree_recoed", where="type==3")
       
nbinsP = 100

for extraInfo in extraInfos:
    info = R.TProfile("{0}".format(extraInfo),"{0}".format(extraInfo),nbinsP,0,0.001)
    info.Sumw2()
    print("Filling {0} extraInfo".format(extraInfo))
    for i in range(len(tr_recoed)):
        if(i%printFreq == 0):
            print("Event {0}".format(i))
        info.Fill(1./tr_recoed["p_EndVelo"][i],tr_recoed["{0}".format(extraInfo)][i],1)

    LUTCreatorList([info],"lutTrack_{0}.dat".format(extraInfo))
