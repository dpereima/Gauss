# Info For Alpha Testers

This provides a guide for how to minimally run Lamarr for a sample, then run DaVinci over the sample to produce the finalized ntuples. Should you find mistakes, please do ask for help and update this document!

## Production of Gauss Sample

We will rely mainly on the scripts documented in the `tests/qmtests/` directory and in `options/Lamarr-Job.py`. These provide the minimal configurations for samples to be produced.

### Productions with Pythia 8 as a generator
For samples which you'd like to use Pythia 8 as a generator, we will base the script on `tests/options/testGauss-Job-MinBias_MagUp.py` which provides the minimum configuration. This would lead to an options file which looks like:


```python
from Gaudi.Configuration import *
from Configurables import LbLamarr

eventType = ""#your event type, e.g. "30000000"

#only 2016 mag up is supported at this moment
importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")
importOptions("$DECFILESROOT/options/{eventType}.py".format(eventType=eventType))
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
#configuration of Lamarr
LbLamarr().EventType = eventType
LbLamarr().EvtMax    = 1000
LbLamarr().DataType  = '2016'
LbLamarr().Polarity  = 'MagUp'
#correct beam conditions
importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-mu100-2016-nu1.6.py")

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20210215-6"
LHCbApp().CondDBtag = "sim-20201113-6-vc-mu100-Sim10"
importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")

```

provided the script is saved as `alpha_test_example.py`, this script can then be run using the nightlies with the following command:

```
lb-run --nightly lhcb-gauss-lamarr Gauss/HEAD gaudirun.py alpha_test_example.py 
```

NB: if the nightly for the day has failed, this will not work. Have a peek at https://lhcb-nightlies.web.cern.ch/nightly/ for the status.

This should produce a file `Lamarr_{DataType}{Polarity}_{EventType}_{RunNumber}.xsim` which can be used in the next step


### Productions with ParticleGun as a generator
For samples for which ParticleGun is sufficient, you can use the file `options/Lamarr-Job.py` which contains the following:

```python
from Gaudi.Configuration import * 

from Configurables import LbLamarr 
from LbLamarr import GeneratorConfigWizard #do the heavy liting

eventType = '15874000' #as before

GeneratorConfigWizard.configureGenerator ( 'ParticleGun', eventType ) 

LbLamarr().EventType = eventType
LbLamarr().EvtMax    = 50
LbLamarr().DataType  = '2016'
LbLamarr().Polarity  = 'MagUp' 
LbLamarr().OutputLevel = VERBOSE 
```
This should also produce an output `.xsim` file which will have the info you need.

NB: If you have trouble with momentum ranges, or momentum histograms, you'll need to sort that out with the corresponding options

## Processing with DaVinci or Bender
Once you have a produced `.xsim` file, you can use either DaVinci or Bender to produce an analysis tuple which you can then use for the remaining step. For this, we need to let DaVinci know where the produced files are. At a bare minimum, you'll need to add:

```python
Protons = DataOnDemand ( "Phys/StdAllNoPIDsProtons/Particles")#, OutputLevel=VERBOSE ) 
Kaons = DataOnDemand ( "Phys/StdAllNoPIDsKaons/Particles" ) 
Pions = DataOnDemand ( "Phys/StdAllNoPIDsPions/Particles" ) 
Muons = DataOnDemand ( "Phys/StdAllNoPIDsMuons/Particles" ) 

```
to the DaVinci Script.  For photons or pi0s, you need to add

```python
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from PhysSelPython.Wrappers import Selection, SelectionSequence
from Configurables import DaVinci, DataOnDemandSvc,DecayTreeTuple,FilterDesktop, CombineParticles,PhotonMaker, PhotonMakerAlg#, Selection, SelectionSequence

from CommonParticles.Utils import *
#get photons protoparticles, make them into particles.
#PhotonMaker takes directly from LHCb::ProtoParticleLocation::Neutrals

algorithm =  PhotonMakerAlg ( 'StdLooseAllPhotons'         ,
                                DecayDescriptor = 'Gamma' )

# configure desktop&particle maker: 
algorithm.addTool ( PhotonMaker , name = 'PhotonMaker' )
photon = algorithm.PhotonMaker
photon.ConvertedPhotons   = True
photon.UnconvertedPhotons = True
#photon.ConfidenceLevelBase   = []
#photon.ConfidenceLevelSwitch = []
photon.PtCut              = 0 * MeV 

algorithm.InputPrimaryVertices = 'None'
## configure Data-On-Demand service 
locations = updateDoD ( algorithm )

## finally: define the symbol 
myStdLooseAllPhotons = Selection("myStdLooseAllPhotons",Algorithm = algorithm ,
                               InputDataSetter=None)

myStdLooseAllPhotonsSeq = SelectionSequence("myStdLooseAllPhotonsSeq",TopSelection = myStdLooseAllPhotons)
...

DaVinci().UserAlgorithms=[algorithm,myStdLooseAllPhotonsSeq]
```
