/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GenInit.h,v 1.3 2009-03-26 21:32:40 robbep Exp $
#ifndef GENINIT_H
#define GENINIT_H 1

// Include files
// from LHCbKernel
#include "Kernel/LbAppInit.h"
#include "Kernel/IEventCounter.h"
#include "GaudiKernel/ToolHandle.h"

// from Event
#include "Event/BeamParameters.h"
#include "Event/GenHeader.h"

class IGenericTool;

/** @class GenInit GenInit.h
 *
 *  First TopAlg for Generator phase of Gauss.
 *  Initializes random number and fills memory histogram.
 *  It also creates and fill the GenHeader.
 *
 *  @author Gloria CORTI
 *  @date   2006-01-16
 */
class GenInit : public LbAppInit {
public:
  using LbAppInit::LbAppInit;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:

  IGenericTool* m_memoryTool{nullptr};   ///< Pointer to (private) memory histogram tool
  IEventCounter* m_evtCounterTool{nullptr};
  Gaudi::Property<std::string> m_evtCounterToolName{this, "EvtCounter", ""};

  /// Event counter tool name for GaussMP
  /// Property to set the first event number, if event header is missing
  Gaudi::Property<long long int> m_firstEvent{this, "FirstEventNumber", 1};
  /// Property to set the run number, if event header is missing
  Gaudi::Property<unsigned int> m_runNumber{this, "RunNumber", 1};
  Gaudi::Property<std::string> m_mcHeader{this, "MCHeader",
                                          LHCb::GenHeaderLocation::Default};
  Gaudi::Property<std::string> m_beamParameters{
      this, "BeamParameters", LHCb::BeamParametersLocation::Default};
  Gaudi::Property<double> m_beamEnergy{this, "BeamEnergy",
                                       3.5 * Gaudi::Units::TeV};
  Gaudi::Property<double> m_bunchLengthRMS{this, "BunchLengthRMS", 1.};
  Gaudi::Property<double> m_normalizedEmittance{this, "NormalizedEmittance",
                                                1. * Gaudi::Units::mm};
  Gaudi::Property<double> m_totalCrossSection{this, "TotalCrossSection",
                                              100. * Gaudi::Units::microbarn};
  Gaudi::Property<double> m_horizontalCrossingAngle{
      this, "HorizontalCrossingAngle", 0.3 * Gaudi::Units::mrad};
  Gaudi::Property<double> m_verticalCrossingAngle{this, "VerticalCrossingAngle",
                                                  0.3 * Gaudi::Units::mrad};
  Gaudi::Property<double> m_horizontalBeamlineAngle{
      this, "HorizontalBeamlineAngle", 0.3 * Gaudi::Units::mrad};
  Gaudi::Property<double> m_verticalBeamlineAngle{this, "VerticalBeamlineAngle",
                                                  0.3 * Gaudi::Units::mrad};
  Gaudi::Property<double> m_betaStar{this, "BetaStar", 3. * Gaudi::Units::m};
  Gaudi::Property<double> m_bunchSpacing{this, "BunchSpacing",
                                         50. * Gaudi::Units::ns};
  Gaudi::Property<double> m_xLuminousRegion{this, "XLuminousRegion", 0.};
  Gaudi::Property<double> m_yLuminousRegion{this, "YLuminousRegion", 0.};
  Gaudi::Property<double> m_zLuminousRegion{this, "ZLuminousRegion", 0.};
  Gaudi::Property<double> m_luminosity{
      this, "Luminosity", 1.e32 / (Gaudi::Units::cm2 * Gaudi::Units::s)};

  Gaudi::Property<bool> m_createBeam{this, "CreateBeam", false};

  LHCb::BeamParameters m_beam ; ///< Local beam parameter object
};
#endif // GENINIT_H
