/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 

// from Gaudi
#include "GaudiKernel/IDetDataSvc.h"
#include "GaudiKernel/Time.h"
#include "GaudiAlg/IGenericTool.h"


// local
#include "SimInit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SimInit
//
// 2006-01-16 : Gloria CORTI
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SimInit )

//=============================================================================
// Initialization
//=============================================================================
StatusCode SimInit::initialize() {
  StatusCode sc = LbAppInit::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by LbAppInit

  if(msgLevel(MSG::DEBUG)) debug() << "==> Initialize" << endmsg;

  // Private tool to plot the memory usage
  std::string toolName = name()+"Memory";
  m_memoryTool = tool<IGenericTool>( "MemoryTool", toolName, this, true );

  // Get IDetDataSvc interface of DetDataSvc
  try {
    m_detDataSvc = svc<IDetDataSvc>("DetDataSvc/DetectorDataSvc", true );
  } catch (GaudiException&) {
    return StatusCode::FAILURE;
  }
  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode SimInit::execute() {

  if(msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;
  increaseEventCounter();

  // Memory size check
  checkMem();

  // Plot the memory usage
  if ( "" == rootInTES() ) m_memoryTool->execute();

  // Get the run and event number from the GenHeader if it exist (i.e.
  // Generator phase already run or events read from file)
  LHCb::GenHeader* evt = 
    get<LHCb::GenHeader>( m_genHeader.value() );

  // Initialize the random number
  std::vector<long int> seeds = getSeeds( evt->runNumber(), evt->evtNumber() );
  auto sc = this->initRndm( seeds );
  if ( sc.isFailure() ) return sc;  // error printed already by initRndm  
  this->printEventRun( evt->evtNumber(), evt->runNumber(), &seeds );

  // Create MCHeader and partially fill it - updated in Simulation phase
  LHCb::MCHeader* header = new LHCb::MCHeader();
  header->setApplicationName( this->appName() ); // update it later?
  header->setApplicationVersion( this->appVersion() );
  header->setRunNumber( evt->runNumber() );
  header->setEvtNumber( evt->evtNumber() );
  header->setEvtTime( m_detDataSvc->eventTime().ns() );
  header->setCondDBTags( this->condDBTags() );
  put( header, m_mcHeader.value() );

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode SimInit::finalize() {

  if(msgLevel(MSG::DEBUG)) debug() << "==> Finalize" << endmsg;

  return LbAppInit::finalize();  // must be called after all other actions
}

//=============================================================================
