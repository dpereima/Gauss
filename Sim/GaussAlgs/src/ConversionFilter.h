/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ConversionFilter.h,v 1.3 2008-05-07 09:54:20 gcorti Exp $
#ifndef CONVERSIONFILTER_H 
#define CONVERSIONFILTER_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class ConversionFilter ConversionFilter.h
 *  Filter on gamma conversions. Used after the simulation stage of Gauss to only write events that have a gamma conversion that fulfills certain criteria.
 * 
 *  Parameters:
 *  - Mother: Mother up the decay tree the gamma conversion has to originate from. Use DecayDescriptor symbols, like B+, D*(2007)0, etc. An empty string accepts all mothers.
 *  - MaxSearchDepth: Maximum "distance" in decay tree between gamma and mother.
 *  - MatchSearchDepth: Does the search depth need to match, or is it only a maximum?
 *  - MaxZ: Maximum z value the gamma conversion has to occur.
 *  - MinP: Minimum momentum of the products of the gamma conversion.
 *  - MinPT: Minimum transverse momentum of the products of the gamma conversion.
 *  - MinTheta: Minimum value of angle theta for conversion products
 *  - MaxTheta: Maximum value of angle theta for conversion products
 *
 *  @author Michel De Cian
 *  @date   2017-10-26
 */

class ConversionFilter : public GaudiAlgorithm {

 public: 
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  

 private:

  Gaudi::Property<std::string>   m_mother{this,"Mother","","Mother up the decay tree the gamma conversion has to originate from. Use DecayDescriptor symbols, like B+, D*(2007)0, etc. An empty string accepts all mothers."};
  Gaudi::Property<double>        m_maxZ{this,"MaxZ",500.0  ,"Maximum z value the gamma conversion has to occur."};
  Gaudi::Property<double>        m_minP{this,"MinP",1500.0  ,"Minimum momentum of the products of the gamma conversion."};
  Gaudi::Property<double>        m_minPT{this,"MinPT",50.0  ,"Minimum transverse momentum of the products of the gamma conversion."};
  Gaudi::Property<double>        m_minTheta{this,"MinTheta",0.010,"Minimum value of angle theta for conversion products"};
  Gaudi::Property<double>        m_maxTheta{this,"MaxTheta",0.400,"Maximum value of angle theta for conversion products"};
  Gaudi::Property<unsigned int>  m_maxSearchDepth{this,"MaxSearchDepth",20,"Maximum 'distance' in decay tree between gamma and mother."};
  Gaudi::Property<bool>          m_matchSearchDepth{this,"MatchSearchDepth",false,"Does the search depth need to match, or is it only a maximum?"};
  int m_motherID{-1};
    
};
#endif // MCTRUTHMONITOR_H
