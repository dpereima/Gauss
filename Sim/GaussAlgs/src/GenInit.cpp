/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 
#include <cmath>

// from Gaudi
#include "GaudiAlg/IGenericTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from GenEvent
#include "GenEvent/BeamForInitialization.h"

// local
#include "GenInit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GenInit
//
// 2006-01-16 : Gloria CORTI
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GenInit )


//=============================================================================
// Initialization
//=============================================================================
StatusCode GenInit::initialize() {
  StatusCode sc = LbAppInit::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by LbAppInit

  if(msgLevel(MSG::DEBUG)) debug() << "==> Initialize" << endmsg;

  // Private tool to plot the memory usage
  std::string toolName = name()+"Memory";
  m_memoryTool = tool<IGenericTool>( "MemoryTool", toolName, this, true );
  if (m_evtCounterToolName != "") {
    m_evtCounterTool = tool<IEventCounter>(m_evtCounterToolName, this);
  }

  // create beam parameter object
  m_beam.setEnergy( m_beamEnergy ) ;
  m_beam.setSigmaS( m_bunchLengthRMS ) ;
  m_beam.setEpsilonN( m_normalizedEmittance ) ;
  m_beam.setTotalXSec( m_totalCrossSection ) ;
  m_beam.setHorizontalCrossingAngle( m_horizontalCrossingAngle ) ;
  m_beam.setVerticalCrossingAngle( m_verticalCrossingAngle ) ;
  m_beam.setHorizontalBeamlineAngle( m_horizontalBeamlineAngle ) ;
  m_beam.setVerticalBeamlineAngle( m_verticalBeamlineAngle ) ;
  m_beam.setBetaStar( m_betaStar ) ;
  m_beam.setBunchSpacing( m_bunchSpacing ) ;
  m_beam.setBeamSpot( Gaudi::XYZPoint( m_xLuminousRegion ,
                                       m_yLuminousRegion ,
                                       m_zLuminousRegion ) ) ;
  m_beam.setLuminosity( m_luminosity ) ;
  
  if (m_createBeam) {
    info() << "Create BeamForInitialization" << endmsg;
    // print the values used:
    info() << "Beam Parameters :" << endmsg ;
    info() << "Energy = " << m_beam.energy() / Gaudi::Units::TeV << " TeV" << endmsg ;
    info() << "Bunch length RMS = " << m_beam.sigmaS() / Gaudi::Units::mm << " mm" << endmsg ;
    info() << "Normalized emittance = " << m_beam.epsilonN() / Gaudi::Units::mm << " mm" << endmsg ;
    info() << "Revolution frequency = " << m_beam.revolutionFrequency() / Gaudi::Units::kilohertz << " kHz" << endmsg ;
    info() << "Total cross-section assumed = " << m_beam.totalXSec() / Gaudi::Units::millibarn << " mb" << endmsg ;
    info() << "Horizontal crossing angle = " << m_beam.horizontalCrossingAngle() / Gaudi::Units::mrad << " mrad" << endmsg ;
    info() << "Vertical crossing angle = " << m_beam.verticalCrossingAngle() / Gaudi::Units::mrad << " mrad" << endmsg ;
    info() << "Horizontal beam line angle = " << m_beam.horizontalBeamlineAngle() / Gaudi::Units::mrad << " mrad" << endmsg ;
    info() << "Vertical beam line angle = " << m_beam.verticalBeamlineAngle() / Gaudi::Units::mrad << " mrad" << endmsg ;
    info() << "beta star = " << m_beam.betaStar() / Gaudi::Units::m << " m" << endmsg ;
    info() << "Bunch spacing = " << m_beam.bunchSpacing() / Gaudi::Units::ns << " ns" << endmsg ;
    info() << "Beam spot = " << m_beam.beamSpot() << " mm" << endmsg ;
    info() << "Luminosity = " << m_beam.luminosity() * (Gaudi::Units::cm2 * Gaudi::Units::s ) << " /cm2/s" << endmsg ;
    info() << "Nu = " << m_beam.nu() << endmsg;
    info() << "emittance = " << m_beam.emittance() << endmsg;
    info() << "Luminous region = ( " << m_beam.sigmaX() << ", "
           << m_beam.sigmaY() << ", " << m_beam.sigmaZ() << " ), mm " << endmsg;

    LHCb::BeamParameters * beamParameters = new LHCb::BeamParameters( m_beam );
    BeamForInitialization::getInitialBeamParameters() = beamParameters;

  }
  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode GenInit::execute() {

  if(msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;
  increaseEventCounter();

  // Memory size check
  checkMem();

  // Plot the memory usage
  if ( "" == rootInTES() ) m_memoryTool->execute();

  // Initialize the random number
  longlong eventNumber = 0;
  if(m_evtCounterTool){
  if(msgLevel(MSG::DEBUG)) debug() << "using IEventCounter tool for eventNumber" << endmsg;
    eventNumber = m_firstEvent - 1 + m_evtCounterTool->getEventCounter();
  } else {
  if(msgLevel(MSG::DEBUG)) debug() << "using internal counter for eventNumber" << endmsg;
    eventNumber = m_firstEvent - 1 + this->eventCounter();
  }
  std::vector<long int> seeds = getSeeds( m_runNumber, eventNumber );
  auto sc = this->initRndm( seeds );
  if ( sc.isFailure() ) return sc;  // error printed already by initRndm
  this->printEventRun( eventNumber, m_runNumber, &seeds);
  
  // Create GenHeader and partially fill it - updated during phase execution
  LHCb::GenHeader* header = new LHCb::GenHeader();
  header->setApplicationName( this->appName() );
  header->setApplicationVersion( this->appVersion() );
  header->setRunNumber( m_runNumber );
  header->setEvtNumber( eventNumber );
  header->setEvType( 0 ) ;
  put( header, m_mcHeader );    

  // Check if BeamParameters already exists (main event only!!), in which case link from header
  if ( m_createBeam ) {
    // Create BeamParameters object and fill it.
    if(msgLevel(MSG::DEBUG))
      debug() << "Creating BeamParameters in main event" << endmsg;
    LHCb::BeamParameters* beamParameters = new LHCb::BeamParameters( m_beam ) ;
    put( beamParameters , m_beamParameters ) ;
    
    // Link the beam parameters to the header
    header -> setBeamParameters( beamParameters ) ;
  }
  else {
    if(msgLevel(MSG::DEBUG))
      debug() << "Set beam parameters link to main event" << endmsg;
    LHCb::BeamParameters* beamParameters = 
      get<LHCb::BeamParameters>( m_beamParameters );
    header -> setBeamParameters ( beamParameters );
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode GenInit::finalize() {
  
  if (m_createBeam) {
    if(msgLevel(MSG::DEBUG)) 
      debug() << "Delete BeamForInitialization" << endmsg;
    delete ( BeamForInitialization::getInitialBeamParameters( ) ) ;
  }

  if(msgLevel(MSG::DEBUG)) debug() << "==> Finalize" << endmsg;

  return LbAppInit::finalize();  // must be called after all other actions
}

//=============================================================================
