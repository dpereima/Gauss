/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: EHCalSensDet.cpp,v 1.6 2008-07-11 10:47:44 robbep Exp $ 
// Include files 

// SRD & STD 
#include <algorithm>
#include <vector>

// CLHEP 
#include "CLHEP/Geometry/Point3D.h"
#include "CLHEP/Units/PhysicalConstants.h"

// GaudiKernel
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h" 
#include "GaudiKernel/IDataProviderSvc.h"

// GaussTools 
#include "GaussTools/GaussTrackInformation.h"

// Geant4 
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4SDManager.hh"
#include "G4EnergyLossTables.hh"

// GiGaCnv 
#include "GiGaCnv/GiGaVolumeUtils.h"

// CaloDet
#include "CaloDet/DeCalorimeter.h"

// local
#include "GaussCalo/CaloHit.h"
#include "GaussCalo/CaloSimHash.h"
#include "EHCalSensDet.h"

//
#include "AIDA/IHistogram1D.h"

//-----------------------------------------------------------------------------
//
//  Implementation of class EHCalSensDet
//
//  2001-01-23 : Vanya Belyaev, Patrick Robbe
//
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
EHCalSensDet::EHCalSensDet
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
  : G4VSensitiveDetector( name  )
  , CaloSensDet        ( type , name , parent )
  , m_slotWidth( 25 * CLHEP::ns )
{
  declareProperty( "SlotWidth" , m_slotWidth );
}

//=============================================================================
// The fractions of energy deposited in consequitive time-slots
// in the given Ecal/Hcal cell
//=============================================================================
StatusCode EHCalSensDet::timing 
( const double             time      , 
  const LHCb::Detector::Calo::CellID&        cell      ,
  CaloSubHit::Time&        slot    ,
  CaloSensDet::Fractions&  fractions ) const 
{
  // clear input data 
  slot = 0 ; fractions.clear();
  
  // evaluate the real delta time  
  const double deltaT = time - t0 ( cell ) ;
  
  // find the absolute time slot 
  slot  = (CaloSubHit::Time) floor( deltaT / slotWidth() ) ;
  
  // the time into the slot 
  const double dt  = deltaT - slot * slotWidth() ;
  
  for ( Histos::const_iterator ihist = histos().begin() ; 
        histos().end() != ihist ; ++ihist ) 
    {
      const AIDA::IHistogram1D* histo = *ihist ;
      const int           bin   = histo -> coordToIndex ( dt / CLHEP::ns ) ;
      const double        frac  = histo -> binHeight    ( bin     ) ;
      fractions.push_back( frac ) ;
    };
  
  if( fractions.empty() ) 
    { Warning("timing()::no time information is available!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
  
  return StatusCode::SUCCESS ;
}
