/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOSIM_CALOSUBHIT_H 
#define CALOSIM_CALOSUBHIT_H 1
// Kernel
#include "Detector/Calo/CaloCellID.h"
// GiGa 
#include <unordered_map>
#include "GiGa/GiGaUtil.h"
// Include files
#include "GaussTools/GaussHitBase.h"
#include "GaussCalo/CaloSim.h"
#include "GaussCalo/CaloSimHash.h"

/** @class CaloSubHit CaloSubHit.h
 * 
 *  Elementary "sub-hit" for calorimeter devices.
 *  The class represents an list of energy depositions 
 *  from given particle in the given calorimeter cell.
 *
 *  These objects are not expected to be stored directly 
 *  in Geant4 hits collections. 
 *  These objects are used to be collected in objects of type 
 *  <tt>CaloHit</tt> 
 *
 *  @see CaloHit
 *  @see CaloSubHit
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   2002-12-03
 */
class CaloSubHit : public GaussHitBase 
{
public:
  
  /// type for the hits "Energy" deposition 
  typedef double Energy                     ;  
  /// type for "time" index  
  typedef char   Time                       ;
  // actual type of 
  typedef std::unordered_map<Time,Energy> TheMap   ;
  // iterator type 
  typedef TheMap::const_iterator   iterator ;

public:
  
  /** Standard constructor
   *  @param cellID  cellID  of the detector cell  
   *  @param trackID trackID of the particle
   */
  CaloSubHit ( const LHCb::Detector::Calo::CellID& cellID  = LHCb::Detector::Calo::CellID() , 
               const int         trackID = 0            ) ;
  
  /** copy constructor 
   *  @param right object to be copied 
   */
  CaloSubHit ( const CaloSubHit& right                  ) ;
  
  /// clone method (virtual constructor) 
  virtual CaloSubHit* clone() const ;
  
  /// destructor 
  virtual ~CaloSubHit() ;
  
  void* operator new    ( size_t    ) ;
  void  operator delete ( void *hit ) ;
  
  /// access to cell ID for given hit  
  const LHCb::Detector::Calo::CellID& cellID() const          { return m_cellID        ; }  
  // set new cell ID for given hit 
  void setCellID ( const LHCb::Detector::Calo::CellID& cell ) {        m_cellID = cell ; }
  
  /** add energy deposition for given hit (safe method)
   *  Error flags:
   *   - inconsistent track ID :  300 
   *   - inconsistent cell  ID :  301
   * 
   *  @code 
   *
   *  CaloSubHit*        hit   = ... ;
   *  CaloSubHit::Time   time  = ... ;
   *  CaloSubHit::Energy e     = ... ;
   *  const int          track = ... ;
   *  const LHCb::Detector::Calo::CellID&  cell  = ... ;
   * 
   *  StatusCode sc  = hit->add( track , cell , time , energy ) ;
   *  if( sc.isFailure() ) { .... } 
   *
   *  @endcode 
   * 
   *  @param track  trackID for energy deposition
   *  @param cell   cellID  for the detector cell
   *  @param time   the time of the energy deposition 
   *  @param energy deposition itself 
   *  @return status code 
   */
  StatusCode add ( const int         track , 
                   const LHCb::Detector::Calo::CellID& cell    , 
                   const Time        time    , 
                   const Energy      energy  )
  {
    if (    trackID () != track   ) { return StatusCode( 300 ) ; }
    if ( !( cellID  () == cell  ) ) { return StatusCode( 301 ) ; }    
    return add( time , energy );
  };
  
  /** add energy deposition for given hit (fast method)
   * 
   *  @code 
   *
   *  CaloSubHit*        hit  = ... ;
   *  CaloSubHit::Time   time = ... ;
   *  CaloSubHit::Energy e    = ... ;
   * 
   *  hit->add( time , energy ) ;
   *
   *  @endcode 
   * 
   *  @param track  trackID for energy deposition
   *  @param cell   cellID  for the detector cell
   *  @param time   the time of the energy deposition 
   *  @param energy deposition itself 
   *  @return status code 
   */
  StatusCode add ( const Time   time   , 
                   const Energy energy )
  {
    m_map[ time ] += energy ;
    return StatusCode::SUCCESS ;
  };
  
  /** access for map iterator "begin" (const)
   *  
   *  @code 
   *  
   *  CaloSubHit* hit = ... ;
   *
   *  for( CaloSubHit::iterator entry = hit->begin() ; 
   *       hit->end() != entry ; ++entry ) 
   *    {
   *       CaloSubHit::Energy e = entry->first   ;
   *       CaloSubHit::Time   t = entry->second  ; 
   *    }
   *  
   *   @endcode 
   *   @return 'begin' iterator to the sequence of entries 
   */
  iterator begin   () const { return m_map.begin () ; }
  /** access for map iterator "end"   (const)
   *  
   *  @code 
   *  
   *  CaloSubHit* hit = ... ;
   *
   *  for( CaloSubHit::iterator entry = hit->begin() ; 
   *       hit->end() != entry ; ++entry ) 
   *    {
   *       CaloSubHit::Energy e = entry->first   ;
   *       CaloSubHit::Time   t = entry->second  ; 
   *    }
   *  
   *   @endcode 
   *   @return 'end' iterator to teh sequence of entries 
   */
  iterator end     () const { return m_map.end   () ; }
  
  /// number of entries/map size 
  size_t   size    () const { return m_map.size  () ; }
  size_t   entries () const { return       size  () ; }
  
  /** overall subhit evergy  (integrated over the time )
   * 
   *  @code 
   *  
   *  CaloSubHit* hit = ... ;
   *  CaloSubHit::Energy e = hit->energy() ;
   * 
   *  @endcode 
   *  @return total subhit energy (total energy deposition from given particle 
   *          in the given calorimeter cell)
   */
  Energy   energy  () const 
  {
    Energy e = 0 ;
    for( iterator entry = begin() ; end() != entry ; ++entry ) 
      { e += entry->second ; } 
    return e ;
  };
  
private:
  
  LHCb::Detector::Calo::CellID  m_cellID ;
  TheMap      m_map    ;
  
};
// ============================================================================

// ============================================================================
/** @fn  caloSubHit
 *  Fast cast of G4VHit interface to concrete Gauss implementation
 *  @param  g4   pointer to G4VHit interface 
 *  @return cast (dynamic or static) to CaloSubHit
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date    2002-12-07
 */
// ============================================================================
inline CaloSubHit* caloSubHit( G4VHit* g4 )
{
  GiGaUtil::FastCast<G4VHit,CaloSubHit> cast ;
  return cast( g4 );  
}
// ============================================================================

// ============================================================================
/** @fn  caloSubHit
 *  Fast cast of GaussHitBase interface to concrete Gauss implementation
 *  @param  g4   pointer to GaussHitBase interface 
 *  @return cast (dynamic or static) to CaloSubHit
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date    2002-12-07
 */
// ============================================================================
inline CaloSubHit* caloSubHit( GaussHitBase* g4 )
{
  GiGaUtil::FastCast<GaussHitBase,CaloSubHit> cast ;
  return cast( g4 );  
}
// ============================================================================

// ============================================================================
// The END 
// ============================================================================
#endif // CALOSIM_CALOSUBHIT_H
// ============================================================================
