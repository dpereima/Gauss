###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Sim/GaussCalo
-------------
#]=======================================================================]

gaudi_add_library(GaussCaloLib
    SOURCES
        src/lib/CaloHit.cpp
        src/lib/CaloSubHit.cpp
	src/lib/SmallCaloCell.cpp
    LINK
        PUBLIC
            LHCb::CaloDetLib
            LHCb::MCEvent
            LHCb::LHCbKernel
            Gauss::GaussToolsLib
)

gaudi_add_module(GaussCalo
    SOURCES
        src/CaloCollector.cpp
        src/CaloSensDet.cpp
        src/EHCalSensDet.cpp
        src/EcalSensDet.cpp
        src/GaussSensPlaneDet.cpp
        src/GaussSensPlaneHit.cpp
        src/GetCaloHitsAlg.cpp
        src/HcalSensDet.cpp
        src/SpdPrsSensDet.cpp
    LINK
        Gauss::GaussCaloLib
        Gauss::MCCollectorLib
)

gaudi_add_tests(QMTest)

gaudi_install(PYTHON)
