/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
// LHCb
#include "Event/HepMCEvent.h"

/** @class MaskParticles MaskParticles.h
 *
 *  Algorithm to prevent certain classes of generated particles from
 *  being transmitted to the simulation phase by setting their status
 *  to DocumentationParticle.
 *
 */

class MaskParticles : public GaudiAlgorithm {

 public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override;       ///< Algorithm execution

 private:
  /// TES location of input HepMC events.
  Gaudi::Property<std::string> m_inputLocation{this,"HepMCEventLocation",
    LHCb::HepMCEventLocation::Default,"TES location of input HepMC events."};
  /// PDG ID of particles to be masked.
  Gaudi::Property<int> m_pid{this,"ParticleID",
    2212,"PDG ID of particles to be masked."};
  /// Min. pseudorapidity required for a particle to be masked.
  Gaudi::Property<double> m_etaCut{this,"EtaCut",
    8.,"Min. pseudorapidity required for a particle to be masked."};
  /// Min. energy required for a particle to be masked.
  Gaudi::Property<double> m_energyCut{this,"EnergyCut",
    5. * Gaudi::Units::TeV,"Min. energy required for a particle to be masked."};

};
