/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Gaudi

// LHCb
#include "Kernel/ParticleProperty.h"

// Local
#include "MaskParticles.h"

DECLARE_COMPONENT( MaskParticles )

//=============================================================================
// Main execution
//=============================================================================
StatusCode MaskParticles::execute() {
  
  // Get the generated events.
  LHCb::HepMCEvents* events = getIfExists<LHCb::HepMCEvents>(m_inputLocation.value());
  if (!events) {
    return Error("No HepMCEvents in " + m_inputLocation.value());
  }

  // Loop over the events.
  LHCb::HepMCEvents::iterator it;
  for (it = events->begin(); it != events->end(); ++it) {
    HepMC::GenEvent* ev = (*it)->pGenEvt();
    // Loop over the particles.
    HepMC::GenEvent::particle_iterator itp; 
    for (itp = ev->particles_begin(); itp != ev->particles_end(); ++itp) {
      HepMC::GenParticle* particle = *itp;
      if (particle->status() != LHCb::HepMCEvent::StableInProdGen) continue;
      // Check the particle ID.
      LHCb::ParticleID pid(particle->pdg_id());
      if (m_pid.value() && pid.pid() != m_pid.value()) continue;
      // Check pseudorapidity and energy.
      const double eta = fabs(particle->momentum().pseudoRapidity());
      const double energy = particle->momentum().e();
      if (eta > m_etaCut.value() && energy > m_energyCut.value()) {
        // Set the particle status to DocumentationParticle.
        particle->set_status(LHCb::HepMCEvent::DocumentationParticle);
      }
    }
  }
  return StatusCode::SUCCESS;

}

