/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $ID:$ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.4  2002/05/07 12:21:32  ibelyaev
//  see $GIGAROOT/doc/release.notes  7 May 2002
//
// ============================================================================
#ifndef GIGA_IIDIGIGAGEOSRC_H 
#define GIGA_IIDIGIGAGEOSRC_H 
// ============================================================================
// GaudiKernel
#include "GaudiKernel/IInterface.h"

/** @file 
 *  Unique interface identifier for interafce IGiGaGeoSvc
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   8 May 2002 
 */

/** @var IID_IGiGaGeoSrc
 *  Unique interface identifier for interafce IGiGaGeoSvc
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   8 May 2002 
 */
static const InterfaceID IID_IGiGaGeoSrc ( "IIDIGiGaGeoSrc" , 4 , 0 );

// ============================================================================
// The End 
// ============================================================================
#endif ///< GIGA_IIDIGIGAGEOSRC_H
// ============================================================================
