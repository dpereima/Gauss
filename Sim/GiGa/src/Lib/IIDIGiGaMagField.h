/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IIDIGiGaMagField.h,v 1.1 2004-02-20 18:58:21 ibelyaev Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.9  2003/04/06 18:49:46  ibelyaev
//  see $GIGAROOT/doc/release.notes
//
// Revision 1.8  2002/05/07 12:21:32  ibelyaev
//  see $GIGAROOT/doc/release.notes  7 May 2002
//
// ============================================================================
#ifndef   GIGA_IIDIGIGAMagField_H
#define   GIGA_IIDIGIGAMagField_H 1 
// ============================================================================


/** @file 
 *  declaration of the unique interface identifier for class IGiGaMagField
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   7 May 2002  
 */
/** @var IID_IGiGaMagField
 *  declaration of the unique interface identifier for class IGiGaMagField
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   7 May 2002  
 */
static const InterfaceID IID_IGiGaMagField ( "IGiGaMagField" , 3 , 0 );

// ============================================================================
// The End 
// ============================================================================
#endif ///< GIGA_IIDIGIGAMagField_H
// ============================================================================
