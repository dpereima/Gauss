/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IIDIGiGaRunAction.h,v 1.1 2004-02-20 18:58:23 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.4  2002/05/07 12:21:32  ibelyaev
//  see $GIGAROOT/doc/release.notes  7 May 2002
//
// ============================================================================
#ifndef GIGA_IIDIGIGARUNACTION_H 
#define GIGA_IIDIGIGARUNACTION_H 1
// ============================================================================
/// GaudiKernel
#include "GaudiKernel/IInterface.h"

/** @file 
 *  declaration of unique interface identifier for interface IGiGaRunAction
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   26/07/2001
 */
/** @var IID_IGiGaRunAction
 *  declaration of unique interface identifier for interface IGiGaRunAction
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   26/07/2001
 */
static const InterfaceID IID_IGiGaRunAction( "IGiGarunAction" , 3 , 0 );

// ============================================================================
// The End 
// ============================================================================
#endif // GIGA_IIDIGIGARUNACTION_H
// ============================================================================
