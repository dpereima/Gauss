/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// from GiGa 
#include "GiGa/IGiGaSvc.h"
#include "GiGa/GiGaHitsByName.h"

// from GiGaCnv
#include "GiGaCnv/IGiGaKineCnvSvc.h" 
#include "GiGaCnv/IGiGaCnvSvcLocation.h"
#include "GiGaCnv/GiGaKineRefTable.h"

// Event 
#include "Event/MCCaloHit.h"

// local
#include "GaussCalo/CaloHit.h"
#include "GaussCalo/CaloSubHit.h"

/* 
 *  Implementation file for class GetFastCaloHitsAlg
 *
 *  author: Matteo Rama - INFN Pisa
 *          Giacomo Vitali - SNS Pisa
 *          3 Mar 2020
 */


class GetFastCaloHitsAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  GetFastCaloHitsAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
/// Name of the GiGa service
  Gaudi::Property<std::string> m_gigaSvcName{this, "GiGaService", "GiGa"};
  /// Name of the GiGaCnv service
  Gaudi::Property<std::string> m_kineSvcName{this, "KineCnvService", IGiGaCnvSvcLocation::Kine};
  /// Name of the output location of MCHits
  Gaudi::Property<std::string> m_hitsLocation{this, "MCHitsLocation", ""};
  /// Name of the G4 hits collection
  Gaudi::Property<std::string> m_colName{this, "CollectionName", ""};
  /// Name of the MCParticles location
  Gaudi::Property<std::string> m_mcParticles{this, "MCParticles", LHCb::MCParticleLocation::Default};

  /// Pointer to the GiGa service
  IGiGaSvc * m_gigaSvc ;
  /// Pointer to the GiGaKine service
  IGiGaKineCnvSvc * m_gigaKineCnvSvc ;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GetFastCaloHitsAlg )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
GetFastCaloHitsAlg::GetFastCaloHitsAlg( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator ) ,
    m_gigaSvc( nullptr ) , 
    m_gigaKineCnvSvc( nullptr ) {
  }

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode GetFastCaloHitsAlg::initialize() {
  // Initialize base class
  StatusCode sc = GaudiAlgorithm::initialize() ;
  if ( sc.isFailure() ) return sc ;

  debug() << "==> Initialise" << endmsg ;

  // Get GiGa Service 
  m_gigaSvc = svc< IGiGaSvc >( m_gigaSvcName ) ;

  // Check GiGa service exists
  if ( !m_gigaSvc ) 
    return Error( "execute() : IGiGaSvc* points to NULL" ) ;
  
  // get kineCnv service holding MCParticle/Geant4 table
  m_gigaKineCnvSvc = svc< IGiGaKineCnvSvc >( m_kineSvcName ) ;
  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode GetFastCaloHitsAlg::execute() {
  debug() << "==> Execute" << endmsg ;
  
  LHCb::MCCaloHits* hits = getOrCreate<LHCb::MCCaloHits,LHCb::MCCaloHits>( evtSvc() , m_hitsLocation );
  // Get the G4 hit collections corresponding to Calo
  GiGaHitsByName col( m_colName ) ;
  *m_gigaSvc >> col ;
  if ( !col.hits( ) ) 
    return Warning( "The hit collection='" + m_colName + "' is not found ! " ,
                    StatusCode::SUCCESS ) ;
  
  // Now cast to Calo hits collections
  const CaloSubHitsCollection * hitCollection = caloFastHits( col.hits( ) ) ;
  if ( !hitCollection ) return Error( "Wrong collection type" ) ;
  
  // Get the reference table between G4 tracks and MC particles
  if ( ! exist< LHCb::MCParticles >( m_mcParticles ) ) 
    return Error( "LHCb::MCParticles do not exist at'" +
                  m_mcParticles + "'" ) ;
  const GiGaKineRefTable & table = m_gigaKineCnvSvc -> table() ;

  const size_t numOfHits = hitCollection -> entries() ;
  if ( numOfHits > 0 ) {
    hits->reserve( numOfHits );
  }
  
  // transform CaloSubHits in MCHits:
  // Loop over all hits in collection
  for ( size_t iHit = 0 ; iHit < numOfHits ; ++iHit ) {
    // The calo sub hit in the calorimeter
    const CaloSubHit * subhit = (*hitCollection)[ iHit ] ;
    if ( !subhit ) continue ;
      
    // Pointer to the corresponding LHCb::MCParticle using trackID of the subhit
    const LHCb::MCParticle * mcp = table( subhit -> trackID() ).particle( ) ;
//    if ( !mcp ) 
//      warning() << "No pointer to LHCb::MCParticle for MCHit associated to G4 "
//                  << "trackID: " << subhit -> trackID() << endmsg ;
      
    // Loop over all energy/time deposits strored in the subhit
//    for ( CaloSubHit::iterator entry = subhit -> begin() ; 
//	  entry != subhit -> end() ; ++entry ) {
    for ( const auto entry : *subhit ) {
      // Create the new MCHit
      LHCb::MCCaloHit * mchit = new LHCb::MCCaloHit() ;
      // Fill it with:
      //   - Calorimeter CellID of the hit
      mchit -> setCellID( subhit -> cellID() ) ;
      //   - Time when the energy is deposited
      mchit -> setTime( entry.first ) ;
      //   - Active energy deposited
      mchit -> setActiveE( entry.second ) ;
      //   - Pointer to the LHCb::MCParticle giving the hit
      mchit -> setParticle( mcp ) ;
      // Now insert in output container
      hits -> add( mchit ) ;
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode GetFastCaloHitsAlg::finalize() {
  debug() << "==> Finalize" << endmsg ;

  release( m_gigaSvc ).ignore() ;
  release( m_gigaKineCnvSvc ).ignore() ;

  return GaudiAlgorithm::finalize( ) ;
}

//=============================================================================
