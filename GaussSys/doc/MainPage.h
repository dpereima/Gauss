/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** \mainpage notitle
 *  \anchor gaussdoxygenmain
 *
 * This reference manual documents all %LHCb and %Gaudi classes accessible from
 * the Gauss simulation program environment. More information on Gauss,
 * including a list of available releases, can be found in the 
 * <a href=" http://cern.ch/lhcb-release-area/DOC/gauss/">Gauss project Web pages</a>
 *
 * These pages have been generated directly from the code and reflect the 
 * exact state of the software for this version.
 *
 * <b>The HepMC doxygen pages are not included, follow the link below:</b>
 * \li: 
 * <a href="http://lcgapp.cern.ch/project/simu/HepMC/HepMC203/html/">HepMC 2.03.09</a>
 * 
 *
 */
